<?php /* Template Name: Page Easyshop */ get_header(); ?>

    
    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><?php the_title(); ?></h1>
        </div>
    </section>


    <?php if(function_exists('qtranxf_getLanguage')) { ?>
    <?php if (qtranxf_getLanguage()=='ca'): ?>
    <!-- ********** Català ********** -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/moscatell-demporda-espolla.jpg" class="media" alt="E-shop web responsive"/>
            
            <h2>E-shop per a Instagram i Facebook</h2>
            <h3 class="es-intro"><span>Sokvist</span>, Disseny & Marketing</h3>
            
            <p>Som súper fans de les compres per internet. Ja gairebé no trepitgem una botiga física des de fa anys, i sabem el que és:</p>
            
            <ul class="list img-list">
                <li class="half">
                    <div class="inner">
                        <div class="li-img tick">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Tenir una bona experiència de compra a internet i convertir-nos en clients-apòstols d'una marca.</p>
                        </div>
                    </div>
                </li>
                <li class="half">
                    <div class="inner">
                        <div class="li-img cross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Entrar a una botiga en línia i sortir sense comprar res i, el que és pitjor, sentint-nos frustrats.</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>En el 98% dels casos, els usuaris que ens hem frustrat durant un procés de compra en línia, fem accions més enllà de deixar de comprar:</p>
            
            <ul class="list img-list">
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">ens donem de baixa de la <em>newsletter</em> d'aquest negoci</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">bloquegem els anuncis d'aquesta marca</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">ho comentem amb amics i critiquem aquesta marca</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>A part de que el producte sigui interessant, el més important en una botiga online és oferir <strong>una bona experiència de compra</strong>, que el procés sigui fàcil, fluid, net i atent. El client ha d'acabar el procés fent clic al botó de pagar amb un somriure i una sensació positiva al cos.</p>
            
            <p>Si tens una estratègia de comunicaió en xarxes socials, un nínxol de mercat ja definit i un màxim de 15 productes, fes-te una <strong class="orange">e-Shop especial per Instagram i Facebook</strong>.</p>
            
            <p>El teu client està acostumat a la navegació <strong>ràpida, fresca, intuïtiva i interactiva</strong> de l'entorn de les xarxes socials. Per tant, el teu e-Shop ha d'estar a l'alçada de les seves expectatives.</p>
            
            <a class="showcase-img" href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/portfoli/easyshop-showcase-2.jpg" class="media" alt="E-shop web responsive"/></a>
            
            <h3>Assegura't una bona ràtio de conversió</h3>
            
            <p>Si fas un anunci a Facebook o Instagram per dur visitants a la botiga, la navegació ha de ser ràpida i eficient. Has seduït al teu visitant, l’has portat cap a casa teva, ara posa-l'hi molt fàcil per a <strong>COMPRAR</strong>.</p>
            
            <p>Aquest és l'objectiu amb el qual hem dissenyat aquesta botiga.</p>
            
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <h3 class="price">Una E-shop <a href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank">com aquesta</a> = <span>1.600€</span> + IVA</h3>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Característiques de l’e-shop</a></li>
                    <li><a href="#tab2">Necessitaràs</a></li>
                    <li><a href="#tab3">Extres</a></li>
                    <li><a href="#tab4">Termini d'entrega</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <ul>
                            <li>Disseny <em>responsive</em> (Multidispositius)</li>
                            <li>Estructura intuïtiva, neta i d'usabilitat òptima</li>
                            <li>Gestor de continguts fàcil perquè la puguis autogestionar</li>
                            <li>Plataformes de pagament</li>
                            <li>Botons per compartir les fitxes de producte en xarxes socials, WhatsApp o email</li>
                            <li>Pack SEO bàsic inclòs. La lliurem en les condicions indispensables perquè passi amb nota els tests de <strong>SEO: velocitat de càrrega i usabilitat</strong>
                                <ul>
                                    <li>Sitemap xml</li>
                                    <li>Meta-descripció a la “<em>home</em>”</li>
                                    <li>Semàntica i URL’s “<em>google-friendly</em>”</li>
                                    <li>Arxiu robots.txt</li>
                                    <li>Optimització de les imatges per a web</li>
                                    <li>Indexació en buscadors</li>
                                    <li>Alta a Google Analytics</li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div id="tab2" class="tab">
                        <ul>
                            <li>2-3 fotos en alta resolució per als fons de pantalla. Mínim 2000 px d'ample.</li>
                            <li>Fotos de producte professionals.</li>
                            <li>Textos de màxim 2 paràgrafs per a la secció Qui som.</li>
                            <li>Especificar les condicions de venda, polítiques d'enviaments i devolucions i tenir ja contractat un TPV o un compte de Paypal.</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <a class="tab3-a" href="mailto:judit@sokvist.com?subject=Extres%20e-Shop%20Instagram">consulta preu</a>
                        <ul>
                            <li>Galeria d'imatges. Quadrícula de miniatures o slider.</li>
                            <li>Més d’un idioma. </li>
                            <li>Si tens planificada una estratègia de continguts per atreure clients i fer marca, necessitaràs un bloc.</li>
                            <li>Aplicacions externes, per exemple: formulari de subscripció a la llista de Mailchimp, <em>widget</em> d'opinions de Facebook, Tripadvisor, etc..</li>
                            <li>Wishlist, ratings, elecció de talles i colors.</li>
                            <li>Hosting i registre de domini. Estem molt satisfets amb el nostre proveïdor i et podem proporcionar un bon servei d'allotjament i ciberseguretat. <a href="mailto:judit@sokvist.com">Demana’l</a>.</li>
                            <li>Manteniment web (antispam, protecció, actualització de plugins).</li>
                        </ul>
                    </div>
                    
                    <div id="tab4" class="tab">
                        <p><strong>15 dies</strong>. Te la lliurem "clau en mà", totalment optimitzada i funcionant en 15 dies després de rebre tot el contingut per part teva.</p>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            <h2>Vols vendre a les xarxes socials? Aquesta és la teva e-shop</h2>

            <a href="mailto:judit@sokvist.com?subject=Quiero contratar una e-shop&body=Contactadme por favor. Mi telefono es el ........." title="Envia'ns un email" class="trigger">Contracta-la</a>
            
            <p><strong>¿Dubtes? Te’ls aclarim:</strong></p>
            <p>Mòbil: <a href="tel:0034620273955">620 273 955</a><br>
            Fixe: <a href="tel:0034972793942">972 793 942</a><br>
            Email: <a href="mailto:judit@sokvist.com">judit@sokvist.com</a><br>
            Skype: <a href="skype:judit.figueiras?call">judit.figueiras</a></p>
            
        </div>
    </section><!-- end main -->


    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='es'): ?>
    <!-- ********** Español ********** -->

    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/moscatell-demporda-espolla.jpg" class="media" alt="E-shop web responsive"/>
            
            <h2>E-shop para Instagram y Facebook</h2>
            <h3 class="es-intro"><span>Sokvist</span>, Diseño & Marketing</h3>
            
            <p>Somos súper fans de las compras por internet. Ya casi no pisamos una tienda física desde hace años y sabemos lo que es:</p>
            
            <ul class="list img-list">
                <li class="half">
                    <div class="inner">
                        <div class="li-img tick">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Tener una buena experiencia de compra en internet y convertirnos en clientes-apóstoles de una marca.</p>
                        </div>
                    </div>
                </li>
                <li class="half">
                    <div class="inner">
                        <div class="li-img cross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Entrar en una tienda online y salir sin comprar nada y, lo que es peor, sintiéndonos frustrados.</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>En el 98% de los casos, los usuarios que nos hemos frustrado durante un proceso de compra online, hacemos acciones más allá de dejar de comprar:</p>
            
            <ul class="list img-list">
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">nos damos de baja de la <em>newsletter</em> de ese negocio</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">bloqueamos los anuncios de esa marca</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">lo comentamos con amigos y criticamos esa marca</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>Aparte de que el producto sea interesante o no, lo más importante en una tienda online es ofrecer <strong>una buena experiencia de compra</strong>, que el proceso sea fácil, fluido, limpio y atento. El cliente debe terminar el proceso haciendo <i>click</i> en el botón de pagar con una sonrisa y una sensación positiva en el cuerpo.</p>
            
            <p>Si tienes una estrategia de comunicación en redes sociales, un nicho de mercado ya definido y un máximo de 15 productos, <strong class="orange">hazte una e-Shop especial para Instagram y Facebook</strong>.</p>
            
            <p>Tu cliente está acostumbrado a la navegación <strong>rápida, fresca, intuitiva e interactiva</strong> del entorno de redes sociales. Por tanto, tu e-Shop debe estar a la altura de sus expectativas.</p>
            
            <a class="showcase-img" href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/portfoli/easyshop-showcase-2.jpg" class="media" alt="E-shop web responsive"/></a>
            
            <h3>Asegúrate un buen ratio de conversión</h3>
            
            <p>Si haces un anuncio en Facebook o Instagram para llevar visitantes a tu tienda, la navegación debe ser rápida y eficiente. Has seducido a tu visitante, lo has llevado hasta tu tienda, ahora pónselo muy fácil para <strong>COMPRAR</strong>.</p>
            
            <p>Ese es el objetivo con el que hemos diseñado esta tienda.</p>
            
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <h3 class="price">Una E-shop <a href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank">como esta</a> = <span>1.600€</span> + IVA</h3>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Características de la E-shop</a></li>
                    <li><a href="#tab2">Necesitarás</a></li>
                    <li><a href="#tab3">Extras</a></li>
                    <li><a href="#tab4">Plazo de entrega</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <ul>
                            <li>Diseño <em>responsive</em> (multidispositivos)</li>
                            <li>Estructura intuïtiva, limpia y de usabilidad óptima</li>
                            <li>Gestor de contenidos fácil para que la puedas autogestionar</li>
                            <li>Plataforma de pago</li>
                            <li>Botones de compartición de las fichas de producto en redes sociales, WhatsApp o email</li>
                            <li>Pack SEO básico incluido. La entregamos en las condiciones indispensables para que pase con nota los test de <strong>SEO: velocidad de carga y usabilidad</strong>.
                                <ul>
                                    <li>Sitemap xml</li>
                                    <li>Meta-descripción en la “<em>home</em>”</li>
                                    <li>Semántica y URL’s “<em>google-friendly</em>”</li>
                                    <li>Archivo robots.txt</li>
                                    <li>Optimización de las imágenes para web</li>
                                    <li>Indexación en buscadores</li>
                                    <li>Alta en Google Analytics</li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div id="tab2" class="tab">
                        <ul>
                            <li>2-3 fotos en alta resolución para los fondos de pantalla. Mínimo 2000 px de ancho.</li>
                            <li>Fotos de producto profesionales.</li>
                            <li>Textos de máximo 2 párrafos para la sección Quiénes somos</li>
                            <li>Especificar las condiciones de venta, políticas de envíos y devoluciones y tener ya contratado un TPV o una cuenta de Paypal.</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <a class="tab3-a" href="mailto:judit@sokvist.com?subject=Extras%20e-Shop%20Instagram">consulta precio</a>
                        <ul>
                            <li>Galería de imágenes. Cuadrícula de miniaturas o slider</li>
                            <li>Más de un idioma. </li>
                            <li>Si tienes planificada una estrategia de contenidos para atraer clientes y hacer marca, necesitarás un blog.</li>
                            <li>Aplicaciones externas, por ejemplo: cajita de suscripción a lista de Mailchimp, widget de opiniones de Facebook, Tripadvisor, etc.</li>
                            <li>Wishlist, ratings, elección de tallas y colores.</li>
                            <li>Hosting y registro de dominio. Estamos muy satisfechos con nuestro proveedor y te podemos proporcionar un buen servicio de alojamiento y ciberseguridad. <a href="mailto:judit@sokvist.com">Pídelo</a>.</li>
                            <li>Mantenimiento web (antispam, protección, actualización de plugins).</li>
                        </ul>
                    </div>
                    
                    <div id="tab4" class="tab">
                        <p><strong>15 días</strong>. Te la entregamos “llave en mano”, totalmente optimizada y funcionando en 15 días después de recibir todo el contenido por tu parte.</p>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            <h2>¿Quieres vender en redes sociales? Ésta es tu e-shop</h2>

            <a href="mailto:judit@sokvist.com?subject=Quiero contratar una e-shop&body=Contactadme por favor. Mi telefono es el ........." title="Envia'ns un email" class="trigger">Contrátala</a>
            
            <p><strong>¿Dudas? Te las aclaramos:</strong></p>
            <p>Movil: <a href="tel:0034620273955">620 273 955</a><br>
            Fijo: <a href="tel:0034972793942">972 793 942</a><br>
            Email: <a href="mailto:judit@sokvist.com">judit@sokvist.com</a><br>
            Skype: <a href="skype:judit.figueiras?call">judit.figueiras</a></p>
            
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='en'): ?>
    <!-- ********** English ********** -->

    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/moscatell-demporda-espolla.jpg" class="media" alt="E-shop web responsive"/>
            
            <h2>E-shop for Instagram and Facebook</h2>
            <h3 class="es-intro"><span>Sokvist</span>, Design & Marketing</h3>
            
            <p>We are super fans of online shopping. We have hardly stepped on a physical store for years, and we know what it is:</p>
            
            <ul class="list img-list">
                <li class="half">
                    <div class="inner">
                        <div class="li-img tick">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Having a good shopping experience on the internet and become a brand's client-apostles.</p>
                        </div>
                    </div>
                </li>
                <li class="half">
                    <div class="inner">
                        <div class="li-img cross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Enter an online store and leave without buying anything and, what is worse, feeling frustrated.</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>In 98% of the cases, we users that have been frustrated during an online shopping process, we do actions beyond stop buying:</p>
            
            <ul class="list img-list">
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">we unsubscribe from the newsletter of that business</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">we block ads of that brand</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">we discuss it with friends and criticize that brand</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>Apart from whether the product is interesting or not, the most important thing in an online store is to offer <strong>a good shopping experience</strong> and that the process is easy, smooth, clean and attentive. The client must finish the process by clicking on the pay button with a smile and a positive feeling.</p>
            
            <p>If you have a communication strategy in social media, a niche market already defined and a maximum of 15 products, get a special <strong class="orange">e-Shop for Instagram and Facebook</strong>.</p>
            
            <p>Your client is used to <strong>fast, fresh, intuitive and interactive browsing</strong> of the social media environment. Therefore, your e-shop must rise to their expectations.</p>
            
            <a class="showcase-img" href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/portfoli/easyshop-showcase-2.jpg" class="media" alt="E-shop web responsive"/></a>
            
            <h3>Make sure you have a good conversion rate</h3>
            
            <p>If you publish an ad on Facebook or Instagram to bring visitors to your store, navigation should be fast and efficient. You have seduced your visitor; you have taken him/her to your shop: now make it very easy for him/her to <strong>BUY</strong>.</p>
            
            <p>That's the goal for which we have designed this store.</p>
            
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <h3 class="price">An E-shop <a href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank">like this</a> = <span>€ 1,600</span> + VAT</h3>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">E-shop features</a></li>
                    <li><a href="#tab2">What you need</a></li>
                    <li><a href="#tab3">Additional features</a></li>
                    <li><a href="#tab4">Delivery time</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <ul>
                            <li>Responsive design</li>
                            <li>Intuitive, clean and usable structure</li>
                            <li>Easy to use content manager so you can self-manage it</li>
                            <li>Payment platform</li>
                            <li>Product listing sharing buttons on social media, WhatsApp or email</li>
                            <li>Basic SEO pack included. We deliver the site ready to pass the <strong>SEO test: loading speed and usability</strong>.
                                <ul>
                                    <li>Xml sitemap</li>
                                    <li>Meta-description on home page</li>
                                    <li>Semantics and URLs google-friendly</li>
                                    <li>Robots.txt file</li>
                                    <li>Web images optimization</li>
                                    <li>Search engine indexing</li>
                                    <li>Google Analytics registration</li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div id="tab2" class="tab">
                        <ul>
                            <li>2-3 high resolution images to be used as a background. Minimum 2000 pixels wide.</li>
                            <li>Professional product photos.</li>
                            <li>Copywriting of 2 paragraphs maximum for the About Us section.</li>
                            <li>Specify terms of sale, shipment and return policies, and login data to POS or PayPal account.</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <a class="tab3-a" href="mailto:judit@sokvist.com?subject=Extras%20e-Shop%20Instagram">price inquiry</a>
                        <ul>
                            <li>Image gallery. Thumbnails grid or slider.</li>
                            <li>Languages.</li>
                            <li>Blog. If you have a content strategy planned to attract customers and create brand image, you’ll need a blog.</li>
                            <li>External apps, for example: Mailchimp subscription form, Facebook reviews widget, Tripadvidor, etc...</li>
                            <li>Wishlist, ratings, size and color picker.</li>
                            <li>Hosting and domain registration. We can provide you with a good and secure hosting service.  <a href="mailto:judit@sokvist.com">Ask for it</a>.</li>
                            <li>Web maintenance (anti spam, protection, updates).</li>
                        </ul>
                    </div>
                    
                    <div id="tab4" class="tab">
                        <p><strong>15 days</strong>. We deliver it "turnkey", fully optimized and running in 15 days after receiving all the content.</p>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            <h2>Want to sell on social media? This is your e-shop</h2>

            <a href="mailto:judit@sokvist.com?subject=Quiero contratar una e-shop&body=Contactadme por favor. Mi telefono es el ........." title="Envia'ns un email" class="trigger">Order now</a>
            
            <p><strong>Questions? May we help you?:</strong></p>
            <p>Mobile: <a href="tel:0034620273955">620 273 955</a><br>
            Phone: <a href="tel:0034972793942">972 793 942</a><br>
            Email: <a href="mailto:judit@sokvist.com">judit@sokvist.com</a><br>
            Skype: <a href="skype:judit.figueiras?call">judit.figueiras</a></p>
            
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php } ?>


<?php get_footer(); ?>
