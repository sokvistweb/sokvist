        
        <footer class="site-footer text-light">
            <div class="container">
                <div class="site-footer-inner">
                    <div class="brand footer-brand">
                        <a href="https://sokvist.com/" title="Sokvist website">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 200 200" fill="#f1d866"><path d="M176.7 118.3c-3.8-18.4-9.4-33.5-22.4-47.4-11.2-11.9-27.7-18-43.3-21.3-14.1-3-28.2-3.1-41.7 2.3-15.5 6.2-25.8 20.2-35 33.5-19.6 28.1-20.5 69.9 6.7 93.8 12.2 10.7 27 20 43.7 20 13.3 0 27.4.5 40.6-2.2 35-7.5 58.6-43.7 51.4-78.7zm-70.2 36.3l.2.5c-1.3-1-5 .1-6.9.2-4.4.2-9.4.8-13.6-.5-19.2-6-24.1-27.6-13-43.3 6.3-8.9 14-14.3 25.3-12.7 10 1.4 19.6 6.2 24.3 15.5 7.5 15.4 2.2 36.3-16.3 40.3zM77.7 28.8c.3 2.3-1.4 3.8-4.6 5.3s-6.5 1.5-7 0c-4-13-8.3-18.2-8.3-18.2-1.1-2.7 1.4-3.8 4.6-5.3s7.5-1.3 8.2.1c5.2 11.4 7.1 18.1 7.1 18.1M48.9 44.1c1.2 1.2.2 4.2-2.3 6.7s-5.7 3.6-6.7 2.3c-7.3-9.6-8.7-9.7-15.2-15.5-2.5-2-.9-5.7 1.7-8.2 2.5-2.5 8.1-3.9 9-2.4 5.9 10.2 13.5 17.1 13.5 17.1M151 47.5c-1 1.5-4.2 1-7.1-1.1-2.8-2.1-4.3-4.9-3.3-6.4 0 0 4.8-10.2 12.3-19.1 1.1-1.4 4.3-1.4 7.2.6 2.8 2.1 6 6.9 4.6 8.1-8 7.3-13.7 17.9-13.7 17.9zm-35-19.6c.1 1.8-3.1 3.1-6.6 2.8-3.5-.2-5.9-1.9-6.2-3.7-.9-6.9-1.8-8.3.1-23.5.3-2.3 4.5-3.4 8-3.2s6.8 2.6 6.8 4.4c-.1 10.7-2.4 14.4-2.1 23.2z"/></svg>
                        </a>
                    </div>
                    
                    <ul class="footer-links list-reset">
                        <li>
                            <a href="/avis-legal" title="Consulta els nostres termes legals">Avís Legal</a>
                        </li>
                        <li>
                            <a href="/politica-de-cookies" title="Consulta la nostra política de cookies">Política de cookies</a>
                        </li>
                        <li>
                            <a href="/politica-de-privacitat" title="Consulta els nostres termes legals">Política de privacitat</a>
                        </li>
                    </ul>
                    
                    <ul class="footer-social-links list-reset">
                        <li></li>
                    </ul>
                    
                    <div class="footer-copyright">&copy; 2019 <a href="https://sokvist.com/" title="Sokvist Wesite">Sokvist, web & Marketing</a></div>
                </div>
            </div>
        </footer>
    </div>

    <!--<script src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/js/vendor/jquery-3.3.1.min.js"></script>-->
    
    
    <script src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/js/min/plugins.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/js/min/main.min.js"></script>
    <!--<script src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/js/scripts.js"></script>-->
    
    <?php wp_footer(); ?>
    
    
</body>
</html>
