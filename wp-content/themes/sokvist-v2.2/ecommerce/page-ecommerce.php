<?php /* Template Name: Page E-commerce */ get_header('ecommerce'); ?>


<main>
    <section class="hero text-center">
        <div class="container-sm">
            <div class="hero-inner">
                <h1 class="hero-title h2-mobile mt-0 is-revealing">Serveis per a la digitalització de comerços</h1>
                <h2 class="hero-tagline is-revealing">Fem ecommerce i consultoria digital per a botigues a peu de carrer</h2>

                <p class="cta-intro is-revealing">Si vols que analitzem el teu projecte</p>

                <div class="hero-button newsletter-form field-grouped field-alone is-revealing">
                    <button class="button button-primary button-block button-shadow js-tingle-modal-1">Contacta'ns</button>
                </div>

                <p class="cta-intro is-revealing">Continua llegint i descobreix com digitalitzar el teu negoci, i descarrega't la guia <a href="#" class="js-scroll-into-hello">Com etiquetar productes al teu Instagram</a>.</p>


                <div class="hero-browser">
                    <div class="hero-browser-inner is-revealing">
                        <img src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/images/desktop-browsers.jpg" alt="Sokvist - E-commerce en un portàtil" width="800" height="513">
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="features section text-center">
        <div class="container">
            <div class="features-inner section-inner has-bottom-divider">
                <h2 class="features-title is-revealing">Complementa les vendes amb una botiga online</h2>
                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52.8 66.8C64.1 66.8 70 38.2 70 27c0-11.3-8.5-15.7-19.8-15.7S11.8 34.2 11.8 45.5s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M55.6 40.9c.1-.6.1-1.3.1-1.9 0-.7-.1-1.3-.1-1.9l4.2-3.2c.4-.3.5-.8.2-1.3L56 26c-.2-.4-.8-.6-1.2-.4l-4.9 1.9c-1-.8-2.2-1.5-3.4-1.9l-.7-5.1c-.1-.4-.5-.8-1-.8h-7.9c-.5 0-.9.4-1 .8l-.8 5.1c-1.2.5-2.3 1.2-3.4 1.9l-4.9-1.9c-.5-.2-1 0-1.2.4l-4 6.7c-.2.4-.1 1 .2 1.3l4.3 3.2c0 .7-.1 1.3-.1 1.9s.1 1.3.1 1.9l-4.2 3.2c-.4.3-.5.8-.2 1.3l4 6.7c.2.4.8.6 1.2.4l4.9-1.9c1 .8 2.2 1.5 3.4 1.9l.8 5.1c.1.5.5.8 1 .8h7.9c.5 0 .9-.4 1-.8l.8-5.1c1.2-.5 2.3-1.2 3.4-1.9l4.9 1.9c.5.2 1 0 1.2-.4l4-6.7c.2-.4.1-1-.2-1.3l-4.4-3.3zm-14.8 4.9c-3.9 0-6.9-3-6.9-6.8s3.1-6.8 6.9-6.8 6.9 3 6.9 6.8-3 6.8-6.9 6.8z" fill="#c7b04e"/></svg>
                            </div>
                            <h3 class="feature-title">Amb un gestor senzill i<br> auto administrable</h3>
                            <p class="text-xs">Podràs pujar els teus productes a la web fàcilment, també des del mòbil, i sincronitzar-los amb el teu catàleg de Facebook.</p>
                        </div>
                    </div>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M53.1 67.5c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S12.1 34.9 12.1 46.2s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M41.3 37c-2.3 0-4.2 1.8-4.2 4.1s1.8 4.1 4.2 4.1 4.2-1.8 4.2-4.1-2-4.1-4.2-4.1zm12.4 4.1c0-6.8-5.7-12.3-12.4-12.3s-12.4 5.5-12.4 12.3c0 4.5 2.5 8.5 6.3 10.7l2.1-3.6c-2.5-1.5-4.2-4.1-4.2-7.1 0-4.5 3.7-8.2 8.4-8.2 4.5 0 8.4 3.7 8.4 8.2 0 3.1-1.7 5.7-4.2 7.1l2.1 3.6c3.5-2.1 5.9-6.2 5.9-10.7zM41.3 20.6c-11.4 0-20.8 9.2-20.8 20.5 0 7.6 4.2 14.1 10.3 17.8l2.1-3.6c-5-2.8-8.4-8.1-8.4-14.1 0-9.1 7.5-16.5 16.6-16.5s16.6 7.4 16.6 16.5c0 6-3.3 11.4-8.4 14.1l2.1 3.6c6.3-3.6 10.3-10.2 10.3-17.8.4-11.3-9-20.5-20.4-20.5z" fill="#c7b04e"/></svg>
                            </div>
                            <h3 class="feature-title">Amb Punts de Venda<br> Interactius </h3>
                            <p class="text-xs">T'integrem l'inventari amb PUNTS DE VENDA INTERACTIUS perquè en un sol lloc puguis tenir control del que vens online, a les botigues físiques, o a les parades al carrer.</p>
                        </div>
                    </div>
                </div>
                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80" xml:space="preserve"><path d="M54.8 67.3C66.1 67.3 72 38.7 72 27.5c0-11.3-8.5-15.7-19.8-15.7S13.8 34.7 13.8 46s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M27.3 34.9v-2.8c0-1.3.3-1.9 2.2-1.9H32v-4.7h-4.1c-5 0-6.7 2.3-6.7 6.3v3.1h-3.3v4.7h3.3v14.1h6.1V39.6h4.1l.6-4.7h-4.7z" fill="#c7b04e"/><circle cx="53.8" cy="39.5" r="5.9" fill="#fadf63"/><path d="M48.1 33.7c1.6-1.6 3.6-2.4 5.8-2.4 2.2 0 4.3.9 5.8 2.4 1 1 1.7 2.2 2.1 3.5H68v-8.3c0-1.9-1.5-3.5-3.5-3.5H43.3c-1.9 0-3.6 1.5-3.6 3.5v8.3H46c.4-1.3 1.1-2.5 2.1-3.5zm17.5-2.1c0 .5-.4.9-.9.9h-2.8c-.5 0-.9-.4-.9-.9v-2.8c0-.5.4-.9.9-.9h2.8c.5 0 .9.4.9.9v2.8zM59.7 45.4c-1.6 1.6-3.6 2.4-5.8 2.4-2.2 0-4.3-.8-5.8-2.4-1.6-1.6-2.4-3.7-2.4-5.8h-5.9v10.5c0 1.9 1.7 3.6 3.6 3.6h21.2c1.9 0 3.5-1.7 3.5-3.6V39.6h-5.8c-.2 2.1-1.1 4.2-2.6 5.8z" fill="#fadf63"/></svg>
                            </div>
                            <h3 class="feature-title">Sincronitzada amb Facebook i Instagram</h3>
                            <p class="text-xs">Perquè puguis etiquetar els teus productes als feeds d'Instagram i Facebook, fer-los visibles i fer-los arribar al teu públic.</p>
                        </div>
                    </div>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80" xml:space="preserve"><path d="M51.9 66.8c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S10.9 34.2 10.9 45.5s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M57.9 24.4H22.2c-1.4 0-2.5 1.1-2.5 2.5v24c0 1.4 1.1 2.5 2.5 2.5h35.7c1.4 0 2.5-1.2 2.5-2.6V27c0-1.4-1.1-2.6-2.5-2.6zm-34.2 2.9h32.6c.6 0 1.1.5 1.2 1.1v1.8h-35v-1.8c.1-.6.6-1.1 1.2-1.1zm32.6 23.4H23.7c-.6 0-1.1-.4-1.2-1.1V39h35v10.6c-.1.6-.6 1.1-1.2 1.1z" fill="#c7b04e"/><path d="M25.4 43.4h17.5v1.5H25.4zM25.4 46.3h8.7v1.5h-8.7zM48.7 43.4h5.8v4.4h-5.8z" fill="#c7b04e"/></svg>
                            </div>
                            <h3 class="feature-title">Amb tots els mètodes de pagament</h3>
                            <p class="text-xs">Els teus clients podran pagar amb els mètodes habituals: VISA, Paypal, Stripe, Mastercard, etc.</p>
                        </div>
                    </div>
                </div>
                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M51.3 67.7c11.3 0 17.2-28.7 17.2-40S59.9 12 48.6 12 10.2 35.1 10.2 46.4 40 67.7 51.3 67.7z" fill="#fdf5d1"/><path d="M53.8 45.8H61V22H37.2v7.1m8.3 28.7h3.6v-3.6m-23.8 0v3.6h3.6m-.1-23.9h-3.6v3.6" fill="none" stroke="#fadf63" stroke-width="4" stroke-linecap="square"/><path d="M46.7 33.9h2.4v2.4" fill="none" stroke="#fadf63" stroke-width="3" stroke-linecap="square"/></svg>
                            </div>
                            <h3 class="feature-title">Amb eines<br> per a l'atenció<br> al client digital</h3>
                            <p class="text-xs">T'integrem eines de missatgeria instantània perquè puguis donar una atenció impecable al teu client.</p>
                        </div>
                    </div>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52 67.2c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S11 34.6 11 45.9s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M23.3 31.1h8.4v-8.4h-8.4v8.4zm12.6 25.2h8.4v-8.4h-8.4v8.4zm-12.6 0h8.4v-8.4h-8.4v8.4zm0-12.6h8.4v-8.4h-8.4v8.4zm12.6 0h8.4v-8.4h-8.4v8.4zm12.6-21.1V31h8.4v-8.4h-8.4zm-12.6 8.5h8.4v-8.4h-8.4v8.4zm12.6 12.6h8.4v-8.4h-8.4v8.4zm0 12.6h8.4v-8.4h-8.4v8.4z" fill="#c7b04e"/></svg>
                            </div>
                            <h3 class="feature-title">+ Apps específiques per al sector comerç </h3>
                            <p class="text-xs">Hi ha una gran oferta d'aplicacions digitals pensades per a facilitar gestions relacionades amb la venda online: emprovadors digitals, wishlists, ubicació de punts de recollida, gestió de ressenyes. La llista és llarga, t'ajudarem a trobar la que s'ajusta a allò que necessites.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="features hero-mobile section text-center">
        <div class="container">
            <div class="features-inner section-inner has-bottom-divider">

                <h2 class="features-title is-revealing">Com serà la teva e-commerce?</h2>

                <div class="hero-browser">
                    <div class="hero-browser-inner is-revealing">
                        <img src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/images/mobile-browsers.jpg" alt="Sokvist - E-commerce en un mòbil" width="400" height="684">
                    </div>
                </div>

                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52 67.1c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S11 34.5 11 45.8s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M46.1 21.4h12v12m-14 2l12.2-12.2M34.1 57.4h-12v-12m14-2L23.9 55.6" fill="none" stroke="#c7b04e" stroke-width="4" stroke-linecap="square"/></svg>
                            </div>
                            <h3 class="feature-title">A la teva mida</h3>
                            <p class="text-xs">Ni sobredimensionada, ni limitada, exactament l’eina que respon a les necessitats d'avui, i preparada per a créixer.</p>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52.4 65.6c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S11.4 33 11.4 44.3s29.7 21.3 41 21.3z" fill="#fdf5d1"/><g fill="none" stroke="#c7b04e" stroke-width="4"><circle cx="38.5" cy="35.9" r="16"/><path d="M58.5 55.9l-8.7-8.7" stroke-linecap="square"/></g></svg>
                            </div>
                            <h3 class="feature-title"><abbr title="Search Engine Optimization">SEO</abbr> <i lang="en">friendly</i></h3>
                            <p class="text-xs">Amb tots els requeriments tècnics imprescindibles per a posicionar-se orgànicament.</p>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52.6 67.2c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S11.6 34.6 11.6 45.9s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M30.7 19.4h20c2.2 0 4 1.8 4 4v32c0 2.2-1.8 4-4 4h-20c-2.2 0-4-1.8-4-4v-32c0-2.2 1.8-4 4-4z" fill="none" stroke="#c7b04e" stroke-width="4" stroke-linecap="square"/><path fill="none" stroke="#000" stroke-width="4" stroke-linecap="square" d="M40.7 51.4"/><path fill="#c7b04e" d="M38.7 48.4h4v4h-4z"/></svg>
                            </div>
                            <h3 class="feature-title"><i lang="en">Mobile first</i></h3>
                            <p class="text-xs">Dissenyada des de la perspectiva de la usabilitat mòbil perquè els clients que ho fan tot amb el mòbil gaudeixin comprant a la teva botiga.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="features section who-you-are text-center">
        <div class="container">
            <div class="features-inner section-inner has-bottom-divider">
                <h2 class="features-title is-revealing">Qui ets tu:</h2>

                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm">T’hi mires molt amb el servei. Tens cura de que tingui una bona experiència quan entra al teu local. Amb la ecommerce, també.</p>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm">Ets una empresa petita. Tens poc personal. Vols posar-te al dia amb una inversió assumible i a la mida dels teus balanços.</p>
                        </div>
                    </div>
                </div>

                <div class="features-wrap">
                    <div class="feature feature-reverse is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm"><strong>Busques un partner que t'entengui i t'ajudi en aquest repte</strong>. Sense permanències ni quotes mensuals, però amatent a les teves inquietuds i necessitats.</p>
                        </div>
                    </div>

                    <div class="feature feature-reverse is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm">T’atabales amb el <abbr title="Return On Investment">ROI</abbr>, Ads, <abbr title="Key Performance Indicator">KPIs</abbr>, <i lang="en">feeds</i>, <i lang="en">leads</i>, SEO, però vols que la teva botiga sigui visible i complementar les vendes offline amb les online.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="who-we-are text-center">
        <div class="container-sm">
            <div class="hero-inner has-bottom-divider">
                <h2 class="features-title is-revealing">Qui som nosaltres</h2>
                <p class="cta-intro who-intro is-revealing">Coneixem bé les empreses petites i hi treballem a gust, perquè això ens permet donar una atenció molt personalitzada. Som un equip format per consultors que et podem ajudar en cada punt del procés de digitalització: gestió de marca i reputació online, desenvolupament i programació de webs/apps, fotografia de producte per a ecommerce, marketing digital i SEO, dinamització comercial, atenció al client online.</p>
            </div>
        </div>
    </section>
    
    <section class="hero what-we-do text-center">
        <div class="container-sm">
            <div class="hero-inner">
                <h3 class="hero-tagline is-revealing">Suport en la promoció, comunicació i èxit de vendes!</h3>
                <p class="cta-intro is-revealing">Un cop tinguis la botiga online a punt, t'ajudem a planificar les accions de promoció i comunicació digital. T'ajudem a fer un Pla de Marketing Digital i a treure el màxim profit de les campanyes d'Instagram, enviament de newsletters, fidelització del client online, i a combinar la dinamització comercial online i offline.</p>
                
                <h3 class="hero-tagline is-revealing">Tallers de tècniques de venda online i offline</h3>
                <p class="cta-intro is-revealing">Oferim tallers que t'ajudaran a treure el màxim profit de les eines digitals. Podem fer-los personalitzats per al teu negoci exclusivament, o per a col·lectius com associacions de botiguers, ajuntaments o centres de promoció econòmica:</p>

                <div class="features-wrap features-donate">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 400 400" xml:space="preserve"><circle cx="207.2" cy="191" r="15.6" fill="#fadf63"/><circle cx="160.4" cy="191" r="15.6" fill="#fadf63"/><path d="M252.6 13.1C184.4 13.1 21 167 21 243s179.1 143.2 247.3 143.2S372 193.9 372 118.6c0-75.9-51.3-105.5-119.4-105.5zM308.4 222c0 19.3-16.6 34.8-37.1 34.8h-1.8V288s-38.4-26.1-41.7-28.6c-3.4-2.5-3.4-2.5-10.4-2.5H143c-20.5 0-37.1-15.5-37.1-34.8v-61.7c0-19.3 16.6-34.9 37.1-34.9h128.2c20.5 0 37.1 15.6 37.1 34.9V222z" fill="#fadf63"/><circle cx="253.9" cy="191" r="15.6" fill="#fadf63"/></svg>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner feature-donate">
                            <ul>
                                <li><strong>Formació per a gestionar la ecommerce de forma autònoma</strong> - Taller personalitzat d'1 hora aprox (online).</li>
                                <li><strong>Eines digitals per a promocionar els teus productes</strong> - Workshop de 2 a 4 hores, presencial o online.</li>
                                <li><strong>Converteix el teu local en un punt de venda consultiva, i aconsegueix la venda (online o ofine)</strong> - Reunió personalitzada presencial o telemàtica, o workshop per a col•lectius. Presencial o telemàtic. Aprox 2 hores.</li>
                                <li><strong>La botiga física com a lloc d'experiències: el marketing d'esdeveniments i com enllaçar-lo amb la venda online</strong> - Workshop d'aprox. 2 hores presencial o telemàtic.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


     <section class="features section contactform">
        <div class="container">
            <div class="features-inner section-inner has-bottom-divider">

                <div class="contact-form newsletter-header text-center is-revealing">

                    <div class="features-wrap features-donate">
                        <div class="feature is-revealing">
                            <div class="feature-inner feature-donate">
                                <h2 class="section-title">Quedem, <span>no?</span></h2>
                                <p class="section-p">... o també pots estudiar-t’ho encara millor amb <a href="#" class="js-scroll-into-hello2">aquesta guia</a>.</p>
                            </div>
                        </div>

                        <div class="feature is-revealing">
                            <div class="feature-inner feature-donate">

                                <div class="tingle-demo tingle-demo-tiny tingle">
                                
                                    <?php echo do_shortcode( '[contact-form-7 id="755" title="Contact form 1"]' ); ?>
                                
                                </div>

                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </section>


    <section class="newsletter section">
        <div class="container-sm">
            <div class="newsletter-inner section-inner has-bottom-divider">

                <div class="newsletter-header text-center is-revealing">

                    <h2>La teva clientela és a Instagram?</h2>

                    <img src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/images/guia-pdf.jpg" alt="Sokvist - Vista prèvia del document pdf" width="260" height="195">

                    <p class="section-paragraph">Necessites una <strong>Instashop</strong>. Descarrega’t la guia on t’ensenyem com fer-ho.</p>
                </div>

                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=324f6df81d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div class="footer-form newsletter-form field field-grouped is-revealing">
                        <div class="control control-expanded">
                            <input type="email" value="" name="EMAIL" class="input email" id="mce-EMAIL" placeholder="El teu email" required>
                        </div>

                        <div class="control">
                            <input type="submit" value="Descarrega't la guia" name="subscribe" id="mc-embedded-subscribe" class="button button-primary button-block button-shadow">
                        </div>
                    </div>
                </form>

                <p class="text-center text-small is-revealing"><small>Tenim cura de les teves dades, tal com s’explica en la nostra <a href="/politica-de-privacitat" title="Consulta els nostres termes legals">Política de privacitat</a>.</small></p>
            </div>
        </div>
    </section>

</main>



<?php get_footer('ecommerce'); ?>