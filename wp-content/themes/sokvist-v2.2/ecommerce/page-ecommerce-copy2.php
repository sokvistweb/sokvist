<?php /* Template Name: Page E-commerce */ get_header('ecommerce'); ?>


<main>
    <section class="hero text-center">
        <div class="container-sm">
            <div class="hero-inner">
                <h1 class="hero-title h2-mobile mt-0 is-revealing">Fem e-commerce per a projectes sensibles</h1>
                <h2 class="hero-tagline is-revealing">Dades i disseny perquè marques conscients arribin als seus clients</h2>

                <p class="cta-intro is-revealing">Si ja tens clar què t’oferim</p>

                <div class="hero-button newsletter-form field-grouped field-alone is-revealing">
                    <button class="button button-primary button-block button-shadow js-tingle-modal-1">Contacta'ns</button>
                </div>

                <p class="cta-intro is-revealing">També pots continuar llegint la nostra proposta, o <a href="#" class="js-scroll-into-hello">descarregar-te la guia</a> que t’hem preparat.</p>


                <div class="hero-browser">
                    <div class="hero-browser-inner is-revealing">
                        <img src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/images/desktop-browsers.jpg" alt="Sokvist - E-commerce en un portàtil" width="800" height="513">
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="features section text-center">
        <div class="container">
            <div class="features-inner section-inner has-bottom-divider">
                <h2 class="features-title is-revealing">Què et proposem?</h2>
                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52.4 67c11.1 0 16.9-28 16.9-39 0-11.1-8.4-15.4-19.5-15.4S12.3 35.2 12.3 46.2C12.3 57.3 41.4 67 52.4 67z" fill="#fdf5d1"/><path d="M49.8 28.5v24.6m-8.7-14.5v14.5m-8.7-24.6v24.6m-8.7-14.5v14.5m34.8-34.8v34.8" fill="none" stroke="#786a2f" stroke-width="5" stroke-linecap="square"/></svg>
                            </div>
                            <h3 class="feature-title">Anàlisi de dades i<br> presa de decisions</h3>
                            <p class="text-xs">Analitzem les dades recollides per google de la teva web, i avaluem quins punts cal millorar, quins no són tan rellevants com pensaves, i què caldrà potenciar a la nova ecommerce.</p>
                        </div>
                    </div>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M51.6 66.5c11.2 0 17.1-28.4 17.1-39.6S60.2 11.3 49 11.3s-38 22.8-38 34c-.1 11.3 29.3 21.2 40.6 21.2z" fill="#fdf5d1"/><path d="M26.3 50.2l29-29" fill="none" stroke="#c7b04e" stroke-width="5" stroke-linecap="square"/><path d="M26.3 38.9l12.4-12.4m-14.4 4.1l4.1-4.1m1.1 32l21.8-21.8M51 48.4l9.7-9.7" fill="none" stroke="#fadf63" stroke-width="5" stroke-linecap="square"/></svg>
                            </div>
                            <h3 class="feature-title">Creativitat<br> i dades</h3>
                            <p class="text-xs">Podem crear el contingut fotogràfic, videogràfic o de <i lang="en">copywriting</i> d’acord amb les conclusions de l’anàlisi de dades. Perquè les dades sí que són essencials per a augmentar les vendes.</p>
                        </div>
                    </div>
                </div>
                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M51.3 67.7c11.3 0 17.2-28.7 17.2-40S59.9 12 48.6 12 10.2 35.1 10.2 46.4 40 67.7 51.3 67.7z" fill="#fdf5d1"/><path d="M53.8 45.8H61V22H37.2v7.1m8.3 28.7h3.6v-3.6m-23.8 0v3.6h3.6m-.1-23.9h-3.6v3.6" fill="none" stroke="#fadf63" stroke-width="4" stroke-linecap="square"/><path d="M46.7 33.9h2.4v2.4" fill="none" stroke="#fadf63" stroke-width="3" stroke-linecap="square"/></svg>
                            </div>
                            <h3 class="feature-title">Programació i sensibilitat</h3>
                            <p class="text-xs">Dissenyem i desenvolupem la teva e-commerce perquè la puguis rendibilitzar al màxim en campanyes <abbr title="Search Engine Marketing">SEM</abbr>, i els teus clients puguin fluir durant el procés de compra amb el mòbil, sense distraccions, sense impediments, amb la bona atenció i l’excel·lència que tu ofereixes.</p>
                        </div>
                    </div>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M53.1 67.5c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S12.1 34.9 12.1 46.2s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M41.3 37c-2.3 0-4.2 1.8-4.2 4.1s1.8 4.1 4.2 4.1 4.2-1.8 4.2-4.1-2-4.1-4.2-4.1zm12.4 4.1c0-6.8-5.7-12.3-12.4-12.3s-12.4 5.5-12.4 12.3c0 4.5 2.5 8.5 6.3 10.7l2.1-3.6c-2.5-1.5-4.2-4.1-4.2-7.1 0-4.5 3.7-8.2 8.4-8.2 4.5 0 8.4 3.7 8.4 8.2 0 3.1-1.7 5.7-4.2 7.1l2.1 3.6c3.5-2.1 5.9-6.2 5.9-10.7zM41.3 20.6c-11.4 0-20.8 9.2-20.8 20.5 0 7.6 4.2 14.1 10.3 17.8l2.1-3.6c-5-2.8-8.4-8.1-8.4-14.1 0-9.1 7.5-16.5 16.6-16.5s16.6 7.4 16.6 16.5c0 6-3.3 11.4-8.4 14.1l2.1 3.6c6.3-3.6 10.3-10.2 10.3-17.8.4-11.3-9-20.5-20.4-20.5z" fill="#c7b04e"/></svg>
                            </div>
                            <h3 class="feature-title">Promoció, comunicació i èxit</h3>
                            <p class="text-xs">T’ajudem a planificar un pla d’accions i de comunicació que posicioni la nova e-commerce al mercat que vols arribar. T’acompanyem també en el disseny d’altres suports offline: cartelleria, retolació, papereria. Tot coherent i alineat.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="features hero-mobile section text-center">
        <div class="container">
            <div class="features-inner section-inner has-bottom-divider">

                <h2 class="features-title is-revealing">Com serà la teva e-commerce?</h2>

                <div class="hero-browser">
                    <div class="hero-browser-inner is-revealing">
                        <img src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/images/mobile-browsers.jpg" alt="Sokvist - E-commerce en un mòbil" width="400" height="684">
                    </div>
                </div>

                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52 67.1c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S11 34.5 11 45.8s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M46.1 21.4h12v12m-14 2l12.2-12.2M34.1 57.4h-12v-12m14-2L23.9 55.6" fill="none" stroke="#c7b04e" stroke-width="4" stroke-linecap="square"/></svg>
                            </div>
                            <h3 class="feature-title">A la teva mida</h3>
                            <p class="text-xs">Ni sobredimensionada, ni limitada, exactament l’eina que respon a les necessitats que tens ara... i una miqueta més (perquè saps que creixerà).</p>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52.4 65.6c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S11.4 33 11.4 44.3s29.7 21.3 41 21.3z" fill="#fdf5d1"/><g fill="none" stroke="#c7b04e" stroke-width="4"><circle cx="38.5" cy="35.9" r="16"/><path d="M58.5 55.9l-8.7-8.7" stroke-linecap="square"/></g></svg>
                            </div>
                            <h3 class="feature-title">SEO <i lang="en">friendly</i></h3>
                            <p class="text-xs">Creada i codificada des de zero, sense plantilles prefabricades, sense plugins innecessaris que perjudiquin el <abbr title="Search Engine Optimization">SEO</abbr>. Amb totes les característiques imprescindibles per posicionar-se orgànicament.</p>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52.6 67.2c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S11.6 34.6 11.6 45.9s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M30.7 19.4h20c2.2 0 4 1.8 4 4v32c0 2.2-1.8 4-4 4h-20c-2.2 0-4-1.8-4-4v-32c0-2.2 1.8-4 4-4z" fill="none" stroke="#c7b04e" stroke-width="4" stroke-linecap="square"/><path fill="none" stroke="#000" stroke-width="4" stroke-linecap="square" d="M40.7 51.4"/><path fill="#c7b04e" d="M38.7 48.4h4v4h-4z"/></svg>
                            </div>
                            <h3 class="feature-title"><i lang="en">Mobile first</i></h3>
                            <p class="text-xs">Dissenyada des de la perspectiva de la usabilitat mòbil perquè els clients que ho fan tot amb el mòbil gaudeixin comprant a la teva botiga.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="features section who-you-are text-center">
        <div class="container">
            <div class="features-inner section-inner has-bottom-divider">
                <h2 class="features-title is-revealing">Qui ets tu:</h2>

                <div class="features-wrap">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm">T’hi mires molt amb el servei. Cuides el client. <strong>Tens cura de que tingui una bona experiència. Amb la e-commerce, també</strong>.</p>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm">T’agradaria trobar un partner que t’entengui i que t’ajudi a posar <strong>la teva marca al món digital, sense perdre ni un gram dels valors que tens i que vols transmetre</strong>.</p>
                        </div>
                    </div>
                </div>

                <div class="features-wrap">
                    <div class="feature feature-reverse is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm">Un negoci amb ànim <strong>d’aportar alguna cosa positiva al teu entorn.</strong></p>
                        </div>
                    </div>

                    <div class="feature feature-reverse is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm">T’atabales amb el <abbr title="Return On Investment">ROI</abbr>, Ads, <abbr title="Key Performance Indicator">KPIs</abbr>, <i lang="en">feeds</i>, <i lang="en">leads</i>, SEO, però saps que necessites tenir en compte aquestes variables. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="hero who-we-are text-center">
        <div class="container-sm">
            <div class="hero-inner">
                <h2 class="features-title is-revealing">Qui som nosaltres</h2>
                <p class="cta-intro who-intro is-revealing">Sabem bastant bé què et passa pel cap. Som un equip format per gent creativa i sensible com tu, i gent analítica i amant de la tecnologia com jo. Hem trobat un equilibri entre <strong>sensibilitat i tecnologia</strong>.</p>

                <h3 class="hero-tagline is-revealing">Sempre és un bon moment per a donar suport</h3>
                <p class="cta-intro is-revealing">Donem per fet que el teu projecte és honest, ètic, sostenible i/o socialment responsable. I creiem que el nou e-commerce pot ser la ocasió perfecta per a contribuir amb alguna causa del teu entorn. Perquè no? Et proposem que el 2% de l’import total del pressupost sigui donat a l’associació/entitat/<abbr title="Organització No Governamental">ONG</abbr>/causa a qui tu vulguis donar suport.</p>

                <div class="features-wrap features-donate">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 400 400" fill="#fadf63"><path d="M276 46.9c48.3 0 90.3 34.6 98.3 81.8 5.7 33.7-3.9 63.6-27.4 87.8-42.5 43.7-86.2 86.3-129.5 129.3-8.8 8.7-21 8.5-29.4-.1-8.2-8.4-7.9-20.7.8-29.4l85.7-86.1c2.3-2.3 4.4-5.1 5.7-8 .7-1.6.3-4.6-.9-5.8-1.1-1.2-4.1-1.6-5.8-1.1-2.4.8-4.7 2.5-6.6 4.3l-87.2 88.9c-7.9 8.1-20.2 8.5-28.6.5-8.7-8.3-9.1-20.8-.9-29l63.4-62.5c6.9-6.8 13.5-13.8 20.1-20.9 4.5-4.8 5.4-9.7 2.6-12.4-2.8-2.6-7.7-1.3-12.3 3.3l-82.4 82.4c-6.6 6.6-14.5 8.3-22.4 4.9-8.4-3.6-13.9-11.5-12.8-20.5.6-4.5 2.6-9.6 5.7-12.8l75-74.9c1.9-1.9 3.4-2.7 5.9-.6 20.7 16.9 50.5 9.1 60.3-16 5.7-14.7 3.3-28.2-7.9-39.6l-31.8-31.9c-4.5-4.5-4.4-6.4.7-10.4 18.2-14 38.7-21.2 61.7-21.2zM58.5 218.1c-20.5-21.6-32-46.5-30.4-76 2.1-40.5 22.4-70 58.9-86.4 36.2-16.3 71.3-11.2 102.9 13.2 7.6 5.9 13.9 13.4 20.7 20.2l27.6 27.6c11.5 11.7 11.1 28.5-.7 40.1-11.7 11.6-28.2 11.9-39.7.5l-68.9-68.9c-2.3-2.1-4.9-2.3-7.1-.1-2.3 2.3-1.8 4.9.4 7.1l19.2 19c4.2 4.2 4.2 5.1 0 9.3l-65.7 65.8c-8.2 8.1-16.6 15.8-17.2 28.6zm34.1 21.1c-11.7-.5-18.2-4.7-22-12.8-3.5-7.5-2-15.9 4.1-22.1l73.7-73.9c3.2-3.2 5.6-3.3 8.8 0 6.6 6.8 13.3 13.5 20.2 20.1 3.4 3.3 3.2 6.1 0 9.3l-74 73.6c-3.4 3.2-8.6 4.6-10.8 5.8z"/></svg>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner feature-donate">
                            <p>Com funciona:</p>
                            <ol>
                                <li>Acordem el pressupost del projecte que iniciarem junts.</li>
                                <li>Calculem el 2%.</li>
                                <li>Ens dius quina causa vols recolzar. En cas de que et calgui inspiració, et donem una llista de causes que necessiten suport econòmic.</li>
                                <li>Al final del projecte, transferim els diners de la donació en nom teu.</li>
                                <li>Rebràs el cerficat de donació que faci l’entitat beneficiària.</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


     <section class="features section contactform">
        <div class="container">
            <div class="features-inner section-inner has-bottom-divider">

                <div class="contact-form newsletter-header text-center is-revealing">

                    <div class="features-wrap features-donate">
                        <div class="feature is-revealing">
                            <div class="feature-inner feature-donate">
                                <h2 class="section-title">Quedem, <span>no?</span></h2>
                                <p class="section-p">... o també pots estudiar-t’ho encara millor amb <a href="#" class="js-scroll-into-hello2">aquesta guia</a>.</p>
                            </div>
                        </div>

                        <div class="feature is-revealing">
                            <div class="feature-inner feature-donate">

                                <div class="tingle-demo tingle-demo-tiny tingle">
                                
                                    <?php echo do_shortcode( '[contact-form-7 id="755" title="Contact form 1"]' ); ?>
                                
                                </div>

                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </section>


    <section class="newsletter section">
        <div class="container-sm">
            <div class="newsletter-inner section-inner has-bottom-divider">

                <div class="newsletter-header text-center is-revealing">

                    <h2>Vols uns quants consells?</h2>

                    <img src="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/images/front-page-pdf.png" alt="Sokvist - Vista prèvia del document pdf" width="200" height="240">

                    <p class="section-paragraph">Si vols aprofundir, hem preparat aquesta guia on t’expliquem detalladament el que sabem que és imprescindible en una botiga online eficaç.</p>
                </div>

                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=324f6df81d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div class="footer-form newsletter-form field field-grouped is-revealing">
                        <div class="control control-expanded">
                            <input type="email" value="" name="EMAIL" class="input email" id="mce-EMAIL" placeholder="El teu email" required>
                        </div>

                        <div class="control">
                            <input type="submit" value="Descarrega't la guia" name="subscribe" id="mc-embedded-subscribe" class="button button-primary button-block button-shadow">
                        </div>
                    </div>
                </form>

                <p class="text-center text-small is-revealing"><small>Tenim cura de les teves dades, tal com s’explica en la nostra <a href="/politica-de-privacitat" title="Consulta els nostres termes legals">Política de privacitat</a>.</small></p>
            </div>
        </div>
    </section>

</main>



<?php get_footer('ecommerce'); ?>