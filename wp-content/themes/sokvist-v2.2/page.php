<?php get_header(); ?>

    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><span><?php the_title(); ?></span></h1>
        </div>
    </section>

    <!-- section -->
    <section class="main clearfix" role="main">
        <div class="page-container">

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <?php the_content(); ?>

                <?php comments_template( '', true ); // Remove if you don't want comments ?>

                <br class="clear">

                <?php edit_post_link(); ?>

            </article>
            <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

            </article>
            <!-- /article -->

        <?php endif; ?>

        </div>
    </section>
    <!-- /section -->

<?php get_footer(); ?>
