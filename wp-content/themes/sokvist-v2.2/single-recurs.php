<?php get_header(); ?>

	<section class="parallax-section blog">
        <div id="intro" class="intro">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <span class="blog-title">Recursos</span>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <span class="blog-title">Recursos</span>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <span class="blog-title">Resources</span>
            <?php endif; ?>
            <?php } ?>
        </div>
    </section>
    
    
    <div class="content-body">
        <div class="container">
            <div class="row-main">
                <main class="col-main">
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                    <article class="post post-1">
                        <div class="entry-header">
                            <div class="entry-image">
                                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                    <?php the_post_thumbnail('post-full'); ?>
                                <?php endif; ?>
                            </div>
                            <h1 class="entry-title">
                                <?php the_title(); ?>
                            </h1>

                        </div>
                        <div class="entry-content clearfix">
                            <?php the_content(); // Dynamic Content ?>
                        </div>


                        <div class="share">
                            <?php if(function_exists('qtranxf_getLanguage')) { ?>
                            <?php if (qtranxf_getLanguage()=='ca'): ?>
                            <h3>Comparteix</h3>
                            <?php endif; ?>
                            <?php if (qtranxf_getLanguage()=='es'): ?>
                            <h3>Comparte</h3>
                            <?php endif; ?>
                            <?php if (qtranxf_getLanguage()=='en'): ?>
                            <h3>Share</h3>
                            <?php endif; ?>
                            <?php } ?>
                            <ul class="">
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="facebook-color" target="_blank">Facebook</a></li>
                                <li><a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title_attribute(); ?>" class="twitter-color" target="_blank">Twitter</a></li>
                                <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="googleplus-color" target="_blank">Google+</a></li>
                            </ul>
                        </div>


                        <?php ja_prev_next_post_nav(); ?>


                    </article>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </main>


                <?php get_sidebar( 'recursos_single' ); // (sidebar-recursos_single.php) ?>


            </div>
        </div>
    </div>


    <!-- Popup with Mailchimp subscribe form -->
    <div class="popup" role="alert">
        <div class="popup-container">
            
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <?php the_field('form_code'); ?>
        <?php endwhile; ?>
        <?php endif; ?>
            
        <a href="#0" class="popup-close img-replace">Close</a>
        </div> <!-- cd-popup-container -->
    </div> <!-- cd-popup -->
    <!-- /Popup with Mailchimp subscribe form -->


<?php get_footer(); ?>