<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47544981-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-47544981-1');
    </script>
    <!-- /Global Site Tag (gtag.js) - Google Analytics -->
    
    
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="Sokvist" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="p:domain_verify" content="af5b8510e1fbd6a1e946648b8c23457f"/>

    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link rel="canonical" href="http://www.sokvist.com//">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">

    <?php wp_head(); ?>
    <!-- Modernizer Script for old Browsers -->		
    <script async src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/modernizr.js"></script>
    
    <!-- Hotjar Tracking Code for www.sokvist.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1561195,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>


</head>
<body <?php body_class(); ?>>
    
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<div class="eupopup eupopup-top"></div>

<div class="animsition" id="theme">
    
	<header role="banner">
		<div class="logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>" data-animsition-out-class="fade-out" data-animsition-out-duration="200">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sokvist-logo-sun-v2.1.svg" title="Sokvist, Web & Marketing" alt="Sokvist" width="70" height="70" />
            </a>
		</div><!-- end logo -->
        
        
        <div class="language">
            <?php qtranxf_generateLanguageSelectCode('text') ?>
        </div><!-- /language switcher -->

		
		<nav class="main-menu" 	role="navigation">
			<?php html5blank_nav(); ?>
		</nav><!-- end navigation menu -->
        
        
        
        <!-- Mailchimp subscribe form -->
        <div class="subscribe-form mc_embed_signup">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <p>Vull subscriure'm</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <p>Subscribe me</p>
            <?php endif; ?>
            <?php } ?>
            <form action="//sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=e98b7563d8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate role="form">
                <div id="mc_embed_signup_scroll">

                <div class="mc-field-group">
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                </div>
                <div class="mc-field-group">
                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='en'): ?>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                    <?php endif; ?>
                    <?php } ?>
                </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response"></div>
                    <div class="response" id="mce-success-response"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div class="real-people" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                <div class="clear">
                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='en'): ?>
                    <input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
                    <?php endif; ?>
                    <?php } ?>
                </div>
                </div>
            </form>
        </div>
        <!-- /Mailchimp subscribe form -->
        <!-- Subscribe button -->
        <?php if(function_exists('qtranxf_getLanguage')) { ?>
        <?php if (qtranxf_getLanguage()=='ca'): ?>
        <a href="#" title="Vull subscriure'm" class="subscribe-btn" data-cta-target=".js-sidebar" data-disable-scroll=true>Vull subscriure'm</a>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='en'): ?>
        <a href="#" title="Subscribe me" class="subscribe-btn" data-cta-target=".js-sidebar" data-disable-scroll=true>Subscribe me</a>
        <?php endif; ?>
        <?php } ?>
        <!-- /Subscribe button -->
        
        
		<div class="footer clearfix" role="contentinfo">
			<ul class="social clearfix">
                <li class="twitter"><a href="https://twitter.com/SokvistWeb" title="Sokvist Twitter" target="_blank">
                    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#icon-social-twitter"></use></svg>@SokvistWeb</a></li>
				<li class="facebook"><a href="https://www.facebook.com/sokvistWeb" title="Sokvist Facebook" target="_blank">
                    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#icon-social-facebook"></use></svg>Facebook</a></li>
                <li class="instagram"><a href="https://www.instagram.com/sokvist/" title="Sokvist Instagram" target="_blank">
                    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#icon-social-instagram"></use></svg>Instagram</a></li>
				<!--<li class="googleplus"><a href="https://plus.google.com/105790529308900770762" rel="publisher" target="_blank">
				    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#icon-social-googleplus"></use></svg>Google+</a></li>-->
			</ul><!-- end social -->

			<div class="contact">
				<p><a href="tel:0034620273955">T. +34 620 273 955</a></p>
				<p><a href="mailto:judit@sokvist.com">judit@sokvist.com</a></p>
			</div><!-- end contact -->
			
			<!--<a class="log-btn" href="#" title="Client's login">
		        <svg><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#icon-log-in"></use></svg>Login
            </a>-->
		</div ><!-- end footer -->
	</header><!-- end header -->
	
    
    <div class="theme-switch-wrapper">
        <label class="theme-switch" for="checkbox">
            <input type="checkbox" id="checkbox" />
            <div class="slider round"></div>
        </label>
        <?php if(function_exists('qtranxf_getLanguage')) { ?>
        <?php if (qtranxf_getLanguage()=='ca'): ?>
        <em>Mode fosc</em>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='en'): ?>
        <em>Dark Mode</em>
        <?php endif; ?>
        <?php } ?>
    </div>