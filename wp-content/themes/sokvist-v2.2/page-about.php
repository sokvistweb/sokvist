<?php /* Template Name: Page About */ get_header(); ?>


    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><span><?php the_title(); ?></span></h1>
        </div>
    </section>
    

    <section class="main clearfix team">
        <div class="tabs-container tabs-container-gridder">
            <div class="tabs animated-slide">
                <ul class="tab-links gridder">
                    <li class="grid tab-grid active">
                        <figure class="effect-sarah">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/team-1.jpg" class="media square" alt="Judit" width="520" height="520" />
                            <figcaption>
                                <h2>Judit</h2>
                                
                                <?php query_posts('post_type=page&name=judit'); while (have_posts ()): the_post(); ?>
                                <p><?php the_excerpt(); ?></p>
                                <?php endwhile; ?>
                                
                                <a href="#tab1" class="a-tab">View more</a>
                            </figcaption>		
                        </figure>
                    </li><!--
                    --><li class="grid tab-grid">
                        <figure class="effect-sarah">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/team-2.jpg" class="media square" alt="Sergi" width="520" height="520" />
                            <figcaption>
                                <h2>Sergi</h2>
                                
                                <?php query_posts('post_type=page&name=sergi'); while (have_posts ()): the_post(); ?>
                                <p><?php the_excerpt(); ?></p>
                                <?php endwhile; ?>
                                
                                <a href="#tab2" class="a-tab">View more</a>
                            </figcaption>
                        </figure>
                    </li><!--
                    --><li class="grid tab-grid">
                        <figure class="effect-sarah">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/team-3.jpg" class="media square" alt="David" width="480" height="480" />
                            <figcaption>
                                <h2>David</h2>

                                <?php query_posts('post_type=page&name=david'); while (have_posts ()): the_post(); ?>
                                <p><?php the_excerpt(); ?></p>
                                <?php endwhile; ?>
                                
                                <a href="#tab3" class="a-tab">View more</a>
                            </figcaption>
                        </figure>
                    </li>
                </ul>


                <div class="tab-content tab-content-gridder">
                    <div id="tab1" class="gridder-content tab active">
                        <div class="gridder-show">
                            <div class="gridder-padding">
                                <div class="row cd-project-info">
                                    <div class="row-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/judit.jpg" alt="Judit" width="530" height="530" />
                                    </div>
                                    <?php query_posts('post_type=page&name=judit'); while (have_posts ()): the_post(); ?>
                                    <div class="row-copy">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                            </div><!-- /gridder-padding -->
                        </div><!-- /gridder-show -->
                    </div>

                    <div id="tab2" class="gridder-content tab">
                        <div class="gridder-show">
                            <div class="gridder-padding">
                                <div class="row cd-project-info">
                                    <div class="row-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sergi.jpg" alt="Sergi" width="530" height="530" />
                                    </div>
                                    <?php query_posts('post_type=page&name=sergi'); while (have_posts ()): the_post(); ?>
                                    <div class="row-copy">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                            </div><!-- /gridder-padding -->
                        </div><!-- /gridder-show -->
                    </div>

                    <div id="tab3" class="gridder-content tab">
                        <div class="gridder-show">
                            <div class="gridder-padding">
                                <div class="row cd-project-info">
                                    <div class="row-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/david.jpg" alt="David" width="530" height="530" />
                                    </div>
                                    <?php query_posts('post_type=page&name=david'); while (have_posts ()): the_post(); ?>
                                    <div class="row-copy">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                            </div><!-- /gridder-padding -->
                        </div><!-- /gridder-show -->
                    </div>
                </div><!-- /tab-content -->
            </div><!-- /tabs -->
        </div><!-- /tabs-container -->
    </section>


<?php get_footer(); ?>
