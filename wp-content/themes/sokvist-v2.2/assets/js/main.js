/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

jQuery(document).ready(function($){
    
    
    
    $('.animsition').animsition({
        inClass: 'fade-in',
        outClass: 'fade-out',
        inDuration: 200,
        outDuration: 400
    });
    
    
    
    // Mobile menu toggling 
    /* ====================================================================== */
    $("#menu_icon").click(function(){
        $("header").toggleClass("show_menu");
        $("#menu_icon").toggleClass("close_menu");
        return false;
    });
    
    
    // Subscribe sidebar toggling 
    /* ====================================================================== */
    $(".subscribe-btn").click(function(){
        $(".subscribe-sidebar").addClass("opened");
        return false;
    });
    
    $(".modal-close-btn").click(function(){
        $(".subscribe-sidebar").removeClass("opened");
        return false;
    });
    
    
    // Theme color switcher
    // https://codepen.io/ananyaneogi/pen/zXZyMP
    /* ====================================================================== */
    const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');
    const currentTheme = localStorage.getItem('theme');

    if (currentTheme) {
        document.documentElement.setAttribute('data-theme', currentTheme);

        if (currentTheme === 'dark') {
            toggleSwitch.checked = true;
        }
    }

    function switchTheme(e) {
        if (e.target.checked) {
            document.documentElement.setAttribute('data-theme', 'dark');
            localStorage.setItem('theme', 'dark');
        }
        else {        document.documentElement.setAttribute('data-theme', 'light');
              localStorage.setItem('theme', 'light');
        }    
    }

    toggleSwitch.addEventListener('change', switchTheme, false);
    
    
    
    // Call Gridder - v1.4.1
    // http://www.oriongunning.com/
    /* ====================================================================== */
    $(".gridder").gridderExpander({
        scrollOffset: 150,
        scrollTo: "panel", // "panel" or "listitem"
        animationSpeed: 500,
        animationEasing: "easeInOutExpo"/*,
        onStart: function () {
            console.log("Gridder Inititialized");
        },
        onExpanded: function (object) {
            console.log("Gridder Expanded");
            $(".carousel").carousel();
        },
        onChanged: function (object) {
            console.log("Gridder Changed");
        },
        onClosed: function () {
            console.log("Gridder Closed");
        }*/
    });
    
    
    
    // Lazy load
    $('.lazy').Lazy({
        // your configuration goes here 
        //scrollDirection: 'vertical',
        effect: 'fadeIn',
        delay: 1000
        //effectTime: 900
    });
    
    
    
    // About page
    // https://codyhouse.co/gem/expandable-project-presentation/
    // check if background-images have been loaded and show list items
    /* ====================================================================== */
	/*$('.cd-single-project').bgLoaded({
	  	afterLoaded : function(){
	   		showCaption($('.projects-container li').eq(0));
	  	}
	});

	//open project
	$('.cd-single-project').on('click', function(){
		var selectedProject = $(this),
			toggle = !selectedProject.hasClass('is-full-width');
		if(toggle) toggleProject($(this), $('.projects-container'), toggle);
	});

	//close project
	$('.projects-container .cd-close').on('click', function(){
		toggleProject($('.is-full-width'), $('.projects-container'), false);
	});

	//scroll to project info
	$('.projects-container .cd-scroll').on('click', function(){
		$('.projects-container').animate({'scrollTop':$(window).height()}, 500); 
	});

	//update title and .cd-scroll opacity while scrolling
	$('.projects-container').on('scroll', function(){
		window.requestAnimationFrame(changeOpacity);
	});

	function toggleProject(project, container, bool) {
		if(bool) {
			//expand project
			container.addClass('project-is-open');
			project.addClass('is-full-width').siblings('li').removeClass('is-loaded');
		} else {
			//check media query
			var mq = window.getComputedStyle(document.querySelector('.projects-container'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, ""),
				delay = ( mq == 'mobile' ) ? 100 : 0;

			container.removeClass('project-is-open');
			//fade out project
			project.animate({opacity: 0}, 200, function(){
				project.removeClass('is-loaded');
				$('.projects-container').find('.cd-scroll').attr('style', '');
				setTimeout(function(){
					project.attr('style', '').removeClass('is-full-width').find('.cd-title').attr('style', '');
				}, delay);
				setTimeout(function(){
					showCaption($('.projects-container li').eq(0));
				}, 300);
			});		
		}
	}

	function changeOpacity(){
		var newOpacity = 1- ($('.projects-container').scrollTop())/300;
		$('.projects-container .cd-scroll').css('opacity', newOpacity);
		$('.is-full-width .cd-title').css('opacity', newOpacity);
		//Bug fixed - Chrome background-attachment:fixed rendering issue
		$('.is-full-width').hide().show(0);
	}

	function showCaption(project) {
		if(project.length > 0 ) {
			setTimeout(function(){
				project.addClass('is-loaded');
				showCaption(project.next());
			}, 150);
		}
	}*/
    
    
    // Marketing & Services pages
    // Tabs http://inspirationalpixels.com/tutorials/creating-tabs-with-html-css-and-jquery
    /* ================================================================================= */
    $('.tabs.animated-slide .tab-links a').on('click', function(e) {
		var currentAttrValue = $(this).attr('href');

		// Show/Hide Tabs
		// Fade
        //jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();
        // Slide 1
        //jQuery('.tabs ' + currentAttrValue).siblings().slideUp(400);
        //jQuery('.tabs ' + currentAttrValue).delay(400).slideDown(400);
        // Slide 2
        $('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);
        

		// Change/remove current tab to active
		$(this).closest('li').addClass('active').siblings().removeClass('active');

		e.preventDefault();
	});
    
    
});



/*
* BG Loaded
* Copyright (c) 2014 Jonathan Catmull
* Licensed under the MIT license.
*/
// About page
// https://codyhouse.co/gem/expandable-project-presentation/
// check if background-images have been loaded and show list items
/* ====================================================================== */
 (function($){
 	$.fn.bgLoaded = function(custom) {
	 	var self = this;

		// Default plugin settings
		var defaults = {
			afterLoaded : function(){
				this.addClass('bg-loaded');
			}
		};

		// Merge default and user settings
		var settings = $.extend({}, defaults, custom);

		// Loop through element
		self.each(function(){
			var $this = $(this),
				bgImgs = window.getComputedStyle($this.get(0), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "").split(', ');
			$this.data('loaded-count',0);
			$.each( bgImgs, function(key, value){
				var img = value.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
				$('<img/>').attr('src', img).load(function() {
					$(this).remove(); // prevent memory leaks
					$this.data('loaded-count',$this.data('loaded-count')+1);
					if ($this.data('loaded-count') >= bgImgs.length) {
						settings.afterLoaded.call($this);
					}
				});
			});

		});
	};




     // Subscribe form modal
    // http://codyhouse.co/?p=160
    // Terms (http://codyhouse.co/terms/)
    /* ====================================================================== */
     
     //open popup
    $('.popup-trigger').on('click', function(event){
        event.preventDefault();
        $('.popup').addClass('is-visible');
    });

    //close popup
    $('.popup').on('click', function(event){
        if( $(event.target).is('.popup-close') || $(event.target).is('.popup') ) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    //close popup when clicking the esc keyboard button
    $(document).keyup(function(event){
        if(event.which=='27'){
            $('.popup').removeClass('is-visible');
        }
    });
     
     
     
     // Smooth scrolling
    $('a.more').click(function() {
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top - 120
		}, {
			duration: 900,
			easing: 'swing'
		});
		return false;
	});
     


})(jQuery);