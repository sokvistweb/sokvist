<?php get_header(); ?>


    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><span><?php _e( 'Page not found', 'html5blank' ); ?>.</span> <a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a></h1>
        </div>
    </section>
    
    
    <div class="content-body">
        <div class="container">
            <div class="row-main">
                <article id="post-404">


                </article>
            </div>
        </div>
    </div>


<?php get_footer(); ?>
