<?php /* Template Name: Page Easy web */ get_header(); ?>

    
    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><?php the_title(); ?></h1>
        </div>
    </section>


    <?php if(function_exists('qtranxf_getLanguage')) { ?>
    <?php if (qtranxf_getLanguage()=='ca'): ?>
    <!-- ********** Català ********** -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/cosmic-vinyaters.jpg" class="media" alt="Easy web responsive - Sokvist"/>
            
            <p class="price"><strong>Per <span>500€</span> + IVA</strong></p>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Característiques</a></li>
                    <li><a href="#tab2">Què necessites</a></li>
                    <li><a href="#tab3">No està inclòs</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <ul>
                            <li>Estructuralment, ha de ser <a href="#img-gridder" class="more">com una d'aquestes</a>.</li>
                            <li>Te l'entreguem <strong>indexada a Google</strong> i sobradament apta per a passar un test b&agrave;sic de <strong>SEO</strong> (velocitat de c&agrave;rrega, <em>metadescription</em> SEO, <em>responsive</em>).</li>
                            <li><strong>Termini m&agrave;xim d'1 setmana</strong> a partir de tenir tot el material.</li>
                        </ul>
                    </div>

                    <div id="tab2" class="tab">
                        <ul>
                            <li>Com a mínim 4 o 5 fotos de qualitat, alta resolució, mínim 1200px. Si no en tens, te les podem fer nosaltres - <a href="mailto:judit@sokvist.com?subject=Voldria%20preu%20per%20a%20sessi%C3%B3%20de%20fotos%20o%20v%C3%ADdeo.">consulta preu</a>.</li>
                            <li>Textos d'aproximadament 5 línies per a cada secció. Si això d'escriure et fa mandra, la nostra <em>copywriter</em> et pot ajudar - <a href="mailto:judit@sokvist.com?subject=Consulta%20preu%20redacci%C3%B3%2Fcorrecci%C3%B3%20textos">demana ajuda</a>.</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <ul>
                            <li>El gestor de continguts perquè l'autogestionis. Si tens clar que faràs canvis habitualment - <a href="mailto:judit@sokvist.com">Te n'integrem un</a>.</li>
                            <li>Galeria d'imatges. Quadrícula de miniatures + slider.</li>
                            <li>Més d'un idioma. - <a href="mailto:judit@sokvist.com?subject=Voldria%20la%20web%20en%20varis%20idiomes">Consulta preu</a>.</li>
                            <li>Si tens planificada una estratègia de continguts per atraure clients i/o fer marca, necessitaràs un blog. - <a href="mailto:judit@sokvist.com?subject=Vull%20que%20la%20meva%20web%20tingui%20un%20blog.">Consulta preu</a>.</li>
                            <li>Aplicacions externes, per exemple: caixeta de subscripció a llista de Mailchimp, botons de pagament amb Paypal, motor de reserves, <em>widget</em> de meteorologia, webcam, etc.</li>
                            <li>Hosting i registre de domini. Estem molt satisfets amb el nostre proveïdor de hosting i et podem proporcionar un bon servei d'allotjament i ciberseguretat. <a href="mailto:judit@sokvist.com">Demana-ho</a>.</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">

            <a href="mailto:judit@sokvist.com?subject=Vull%20la%20web%20de%20500%E2%82%AC%2Biva" title="Envia'ns un email" class="trigger">Vull una web</a>
            
            <ul class="list img-list img-gridder" id="img-gridder">
                <li class="half">
                    <a href="http://www.expoprimats.com/" title="Web de l'exposició Primats, la nostra petjada al seu món" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-expoprimats.jpg" class="media" alt="Web Expoprimats - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://cosmic.cat/" title="Web de Còsmic Vinyaters" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-cosmic-vinyaters.jpg" class="media" alt="Web Còsmic Vinyaters - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://hotelelmoli.com/" title="Web de l'Hotel El Molí" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-hotel-el-moli.jpg" class="media" alt="Web Hotel El Molí - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://coworkinglescala.cat/" title="Web de Coworking l'Escala" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-coworkinglescala.jpg" class="media" alt="Web Coworking l'Escala - Sokvist"/>
                    </a>
                </li>
            </ul>
            
            <p><strong>¿Dubtes? Te’ls aclarim</strong></p>
            <p>Mòbil: <a href="tel:0034620273955">620 273 955</a><br>
            Fixe: <a href="tel:0034972793942">972 793 942</a><br>
            Email: <a href="mailto:judit@sokvist.com">judit@sokvist.com</a><br>
            Skype: <a href="skype:judit.figueiras?call">judit.figueiras</a></p>
            
        </div>
    </section><!-- end main -->


    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='es'): ?>
    <!-- ********** Español ********** -->

    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/cosmic-vinyaters.jpg" class="media" alt="Easy web responsive - Sokvist"/>
            
            <p class="price"><strong>Por <span>500€</span> + IVA</strong></p>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Características</a></li>
                    <li><a href="#tab2">Qué necesitas</a></li>
                    <li><a href="#tab3">No está incluido</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <ul>
                            <li>Estructuralmente, debe ser <a href="#img-gridder" class="more">como una de estas</a>.</li>
                            <li>Te la entregamos <strong>indexada en Google</strong> y sobradamente apta para pasar un test básico de <strong>SEO</strong> (velocidad de carga, <em>metadescription</em> SEO, <em>responsive</em>).</li>
                            <li><strong>Plazo máximo de 1 semana</strong> desde el momento de tener todo el material.</li>
                        </ul>
                    </div>

                    <div id="tab2" class="tab">
                        <ul>
                            <li>Al menos 4 o 5 fotos de calidad, alta resolución, mínimo 1200px. Si tienes, te las podemos hacer nosotros - <a href="mailto:judit@sokvist.com?subject=Quisiera%20precio%20pera%20sesi%C3%B3n%20de%20fotos%20o%20v%C3%ADdeo.">consulta precio</a>.</li>
                            <li>Textos de aproximadamente 5 líneas para cada sección. Si esto de escribir te da pereza, nuestra <em>copywriter</em> te puede ayudar - <a href="mailto:judit@sokvist.com?subject=Consulta%20precio%20para%20redacci%C3%B3n%20y%20correcci%C3%B3n%20de%20textos">pide ayuda</a>.</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <ul>
                            <li>El gestor de contenidos para que la autogestiones. Si tienes claro que vas a hacer cambios habitualmente - <a href="mailto:judit@sokvist.com">Te integramos uno</a>.</li>
                            <li>Galería de imágenes. Cuadrícula de miniaturas + slider.</li>
                            <li>Más de un idioma. - <a href="mailto:judit@sokvist.com?subject=Quiero%20la%20web%20en%20varios%20idiomas">Consulta precio</a>.</li>
                            <li>Blog. Si tienes planificada una estrategia de contenidos para atraer clientes y/o hacer marca, necesitarás un blog. - <a href="mailto:judit@sokvist.com?subject=Quiero%20que%20mi%20web%20tenga%20un%20blog.">Consulta precio</a>.</li>
                            <li>Aplicaciones externas, por ejemplo: cajita de suscripción a lista de MailChimp, botones de pago con Paypal, motor de reservas, widget de meteorología, webcam, etc.</li>
                            <li>Hosting y registro de dominio. Estamos muy satisfechos con nuestro proveedor de hosting y te podemos proporcionar un buen servicio de alojamiento y ciberseguridad. <a href="mailto:judit@sokvist.com">Pídelo</a>.</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">

            <a href="mailto:judit@sokvist.com?subject=Quiero%20la%20web%20de%20500%E2%82%AC%2Biva" title="Envíanos un email" class="trigger">Quiero una web</a>
            
            <ul class="list img-list img-gridder" id="img-gridder">
                <li class="half">
                    <a href="http://www.expoprimats.com/" title="Web de l'exposició Primats, la nostra petjada al seu món" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-expoprimats.jpg" class="media" alt="Web Expoprimats - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://cosmic.cat/" title="Web de Còsmic Vinyaters" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-cosmic-vinyaters.jpg" class="media" alt="Web Còsmic Vinyaters - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://hotelelmoli.com/" title="Web de l'Hotel El Molí" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-hotel-el-moli.jpg" class="media" alt="Web Hotel El Molí - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://coworkinglescala.cat/" title="Web de Coworking l'Escala" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-coworkinglescala.jpg" class="media" alt="Web Coworking l'Escala - Sokvist"/>
                    </a>
                </li>
            </ul>
            
            <p><strong>¿Dudas? Te las aclaramos:</strong></p>
            <p>Movil: <a href="tel:0034620273955">620 273 955</a><br>
            Fijo: <a href="tel:0034972793942">972 793 942</a><br>
            Email: <a href="mailto:judit@sokvist.com">judit@sokvist.com</a><br>
            Skype: <a href="skype:judit.figueiras?call">judit.figueiras</a></p>
            
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='en'): ?>
    <!-- ********** English ********** -->

    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/cosmic-vinyaters.jpg" class="media" alt="Easy web responsive - Sokvist"/>
            
            <p class="price"><strong>€ <span>500</span> + VAT</strong></p>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Features</a></li>
                    <li><a href="#tab2">What you need</a></li>
                    <li><a href="#tab3">Not included</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <ul>
                            <li>Structurally, it must be like <a href="#img-gridder" class="more">one of these</a>.</li>
                            <li>We deliver it <strong>Google indexed</strong> and ready to pass a basic <strong>SEO</strong> test (loading speed, SEO metadescription, responsive).</li>
                            <li>Deadline of 1 week maximum after we receive all the assets.</li>
                        </ul>
                    </div>

                    <div id="tab2" class="tab">
                        <ul>
                            <li>At least 4 or 5 high resolution images of minimum 1200 pixels wide. If you don’t have any, we can take them for you - <a href="mailto:judit@sokvist.com?subject=Please%20send%20me%20price%20for%20photo%20and%20video%20shooting.">ask for price</a>.</li>
                            <li>Copywriting of approximately 5 lines for each section. If you don't feel like writing, our copywriter can help you - <a href="mailto:judit@sokvist.com?subject=Check%20price%20for%20copywriting%20service">ask for help</a>.</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <ul>
                            <li>A content manager for self-management. If you are clear that you are going to make changes usually - <a href="mailto:judit@sokvist.com">we will integrate one</a>.</li>
                            <li>Image gallery. Thumbnails grid + slider.</li>
                            <li>More than one language - <a href="mailto:judit@sokvist.com?subject=I%20need%20the%20web%20in%20several%20laguages">Ask for price</a>.</li>
                            <li>Blog. If you have a content strategy planned to attract customers and create brand image, you’ll need a blog - <a href="mailto:judit@sokvist.com?subject=I%20need%20a%20blog%20in%20my%20web">Ask for price</a>.</li>
                            <li>External apps, for example: MailChimp subscription box, Paypal payment buttons, booking engine, weather widget, webcam, etc.</li>
                            <li>•	Hosting and domain registration. We can provide you with a good and secure hosting service - <a href="mailto:judit@sokvist.com">Ask for it</a>.</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">

            <a href="mailto:judit@sokvist.com?subject=Vull%20la%20web%20de%20500%E2%82%AC%2Biva" title="Envia'ns un email" class="trigger">I want a web</a>
            
            <ul class="list img-list img-gridder" id="img-gridder">
                <li class="half">
                    <a href="http://www.expoprimats.com/" title="Web de l'exposició Primats, la nostra petjada al seu món" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-expoprimats.jpg" class="media" alt="Web Expoprimats - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://cosmic.cat/" title="Web de Còsmic Vinyaters" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-cosmic-vinyaters.jpg" class="media" alt="Web Còsmic Vinyaters - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://hotelelmoli.com/" title="Web de l'Hotel El Molí" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-hotel-el-moli.jpg" class="media" alt="Web Hotel El Molí - Sokvist"/>
                    </a>
                </li>
                <li class="half">
                    <a href="http://coworkinglescala.cat/" title="Web de Coworking l'Escala" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/th-coworkinglescala.jpg" class="media" alt="Web Coworking l'Escala - Sokvist"/>
                    </a>
                </li>
            </ul>
            
            <p><strong>Questions? May we help you?:</strong></p>
            <p>Mobile: <a href="tel:0034620273955">620 273 955</a><br>
            Phone: <a href="tel:0034972793942">972 793 942</a><br>
            Email: <a href="mailto:judit@sokvist.com">judit@sokvist.com</a><br>
            Skype: <a href="skype:judit.figueiras?call">judit.figueiras</a></p>
            
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php } ?>


<?php get_footer(); ?>
