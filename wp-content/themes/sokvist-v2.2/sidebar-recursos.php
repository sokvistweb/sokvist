
<aside class="col-aside">
    
    <div class="widget widget-subscribe">
        
        <?php if(function_exists('qtranxf_getLanguage')) { ?>
        <?php if (qtranxf_getLanguage()=='ca'): ?>
        
        <h3 class="widget-title">Conceptes bàsics de SEO per a WordPress</h3>
        <p>Subscriu-te i descarrega't <a href="https://sokvist.com/recurs/conceptes-basics-de-seo-per-a-wordpress/" title="Conceptes bàsics de SEO per a WordPress">la guia</a></p>
        <a class="popup-trigger" href="#" title="Descarrega la guia">Descarrega-la</a>
        
        <!-- Popup with Mailchimp subscribe form -->
        <div class="popup" role="alert">
            <div class="popup-container">
                <div class="widget widget-subscribe">
                    <h3 class="widget-title">Guia SEO bàsic per a WordPress</h3>
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup" class="subscribe-widgetbar">
                    <form action="//sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=6d75e3a4fb" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                            </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response"></div>
                            <div class="response" id="mce-success-response"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Subscriu-te" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                        </div>
                    </form>
                    </div>
                    <!--End mc_embed_signup-->
                </div>
                <a href="#0" class="popup-close img-replace">Close</a>
            </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
        <!-- /Popup with Mailchimp subscribe form -->
        
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='es'): ?>
        
        <h3 class="widget-title">Conceptos básicos de SEO para WordPress</h3>
        <p>Suscríbete y descárgate <a href="https://sokvist.com/es/recurs/conceptos-basicos-de-seo-para-wordpress/" title="Conceptos básicos de SEO para WordPress">la guía</a></p>
        <a class="popup-trigger" href="#" title="Descárgate la guia">Descárgala</a>
        
        <!-- Popup with Mailchimp subscribe form -->
        <div class="popup" role="alert">
            <div class="popup-container">
                <div class="widget widget-subscribe">
                    <h3 class="widget-title">Guia SEO básico para WordPress</h3>
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup" class="subscribe-widgetbar">
                    <form action="//sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=7a20aa5020" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                            </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response"></div>
                            <div class="response" id="mce-success-response"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Suscríbete" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                        </div>
                    </form>
                    </div>
                    <!--End mc_embed_signup-->
                </div>
                <a href="#0" class="popup-close img-replace">Close</a>
            </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
        <!-- /Popup with Mailchimp subscribe form -->
        
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='en'): ?>
        
        <h3 class="widget-title">SEO basics for WordPress</h3>
        <p>Subscribe and download <a href="https://sokvist.com/es/recurs/conceptos-basicos-de-seo-para-wordpress/" title="Conceptos básicos de SEO para WordPress">the guide</a></p>
        <a class="popup-trigger" href="#" title="Descárgate la guia">Download it</a>
        
        <!-- Popup with Mailchimp subscribe form -->
        <div class="popup" role="alert">
            <div class="popup-container">
                <div class="widget widget-subscribe">
                    <h3 class="widget-title">Guia SEO básico para WordPress</h3>
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup" class="subscribe-widgetbar">
                    <form action="//sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=7a20aa5020" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                            </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response"></div>
                            <div class="response" id="mce-success-response"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Suscríbete" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                        </div>
                    </form>
                    </div>
                    <!--End mc_embed_signup-->
                </div>
                <a href="#0" class="popup-close img-replace">Close</a>
            </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
        <!-- /Popup with Mailchimp subscribe form -->
        
        <?php endif; ?>
        <?php } ?>
        
    </div><!-- /.widget-subscribe -->
    
    
    <div class="sidebar-widget">
        
        
        <div class="widget widget_categories">
            
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h3 class="widget-title">Categories</h3>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h3 class="widget-title">Categorías</h3>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h3 class="widget-title">Categories</h3>
            <?php endif; ?>
            <?php } ?>
            
            <?php
            $taxonomy = 'recursos-categories';
            $terms = get_terms($taxonomy); // Get all terms of a taxonomy

            if ( $terms && !is_wp_error( $terms ) ) :
            ?>
            <ul>
                <?php foreach ( $terms as $term ) { ?>
                <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo qtrans_useCurrentLanguageIfNotFoundShowAvailable( $term->name ); ?></a></li>
                <?php } ?>
            </ul>
            <?php endif;?>
            
        </div>
        
        
        <div class="widget widget-recent-posts widget_recent_entries">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h3 class="widget-title">Recursos</h3>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h3 class="widget-title">Recursos</h3>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h3 class="widget-title">Resources</h3>
            <?php endif; ?>
            <?php } ?>
            <?php
            $queryObject = new WP_Query( 'post_type=recurs&posts_per_page=10' );
            // The Loop!
            if ($queryObject->have_posts()) {
                ?>
                <ul>
                <?php
                while ($queryObject->have_posts()) {
                    $queryObject->the_post();
                    ?>

                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php
                }
                ?>
                </ul>
                <?php
            }
            ?>
        </div>
        
        <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-recursos')) ?>
        
	</div>
    
    
</aside>


