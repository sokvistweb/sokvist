<?php get_header(); ?>


    <section class="parallax-section">
        <div id="intro" class="intro">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h1><span>Sokvist</span>, Web i Marketing</h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h1><span>Sokvist</span>, Diseño y Marketing</h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h1><span>Sokvist</span>, Web &amp; Marketing</h1>
            <?php endif; ?>
            <?php } ?>
            <p><?php bloginfo('description'); ?></p>
        </div>
    </section>


    <section class="main clearfix" role="main">
        <ul class="gridder">
            <li class="grid gridder-list" data-griddercontent="#gridder-content-1">
                <figure class="effect-marley">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-dissenyweb-05.jpg" class="media" alt="Disseny Web" width="520" height="520" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Disseny <span>Web</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>Diseño <span>Web</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>Web <span>Design</span></h2>
                        <?php endif; ?>
                        <?php } ?>
                        <a href="#">View more</a>
                    </figcaption>		
                </figure>
            </li><!-- Remove space between inline-block elements http://codepen.io/chriscoyier/pen/hmlqF
            --><li class="grid gridder-list" data-griddercontent="#gridder-content-2">
                <figure class="effect-honey">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-dissenygrafic-05.jpg" class="media" alt="Disseny Grafic" width="520" height="520" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Disseny <span>Gràfic</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>Diseño <span>Gráfico</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>Graphic <span>Design</span></h2>
                        <?php endif; ?>
                        <?php } ?>
                        <a href="#">View more</a>
                    </figcaption>
                </figure>
            </li><!--
            --><li class="grid gridder-list" data-griddercontent="#gridder-content-3">
                <figure class="effect-sarah">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-marketing-01.jpg" class="media square" alt="Marketing Digital" width="480" height="480" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Marketing &amp; <span>SEO</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>Marketing &amp; <span>SEO</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>Marketing &amp; <span>SEO</span></h2>
                        <?php endif; ?>
                        <?php } ?>
                        
                        <?php query_posts('post_type=page&name=digital-marketing'); while (have_posts ()): the_post(); ?>
                        <?php the_excerpt(); ?>
                        <?php endwhile; ?>
                        <a href="#">View more</a>
                    </figcaption>
                </figure>
            </li><!--
            --><li class="grid">
                <figure class="effect-roxy">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-ecommerce.jpg" class="media square" alt="About" width="520" height="520" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>e<span>Commerce</span></h2>
                        <p>Shopify o WooCommerce</p>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>e<span>Commerce</span></h2>
                        <p>Shopify o WooCommerce</p>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>e<span>Commerce</span></h2>
                        <p>Shopify or WooCommerce</p>
                        <?php endif; ?>
                        <?php } ?>
                        <a href="/ecommerce/" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>		
                </figure>
            </li><!--
            --><li class="grid">
                <figure class="effect-bubba bubba1">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-treballs-04.jpg" class="media" alt="Work" width="520" height="520" />
                    <?php query_posts('post_type=page&name=portfolio'); while (have_posts ()): the_post(); ?>
                    <figcaption>
                        <h2><span><?php the_title(); ?></span></h2>
                        <p><?php the_excerpt(); ?></p>
                        <a href="portfolio" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>
                    <?php endwhile; ?>
                </figure>
            </li><!--
            --><li class="grid">
                <figure class="effect-bubba bubba2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-recursos-04.jpg" class="media" alt="Recursos" width="520" height="520" />
                    <?php query_posts('post_type=page&name=services'); while (have_posts ()): the_post(); ?>
                    <figcaption>
                        <h2><span><?php the_title(); ?></span></h2>
                        <p><?php the_excerpt(); ?></p>
                        <a href="services" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>
                    <?php endwhile; ?>
                </figure>
            </li><!--
            --><li class="grid">
                <?php if (have_posts()) : ?>
                <?php query_posts(array( 'post_type' => 'work', 'cat' => 4, 'posts_per_page' => 1 )); ?>
                <?php while (have_posts()) : the_post(); ?>
                <figure class="effect-ruby">
                    <?php $image = get_field('imatge_destacada');
                    if( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="media square" width="520" height="520" />
                    <?php endif; ?>
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Projecte <span>Destacat</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>Proyecto <span>Destacado</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>Featured <span>Project</span></h2>
                        <?php endif; ?>
                        <?php } ?>
                        <p><?php the_title(); ?></p>
                        <a href="portfolio" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>			
                </figure>
                <?php endwhile; ?>
                <?php endif; ?>
            </li><!--
            --><li class="grid">
                <?php if (have_posts()) : ?>
                <?php query_posts(array( 'post_type' => 'work', 'cat' => 3, 'posts_per_page' => 1 )); ?>
                <?php while (have_posts()) : the_post(); ?>
                <figure class="effect-ruby">
                    <?php $image = get_field('imatge_destacada');
                    if( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="media square" width="520" height="520" />
                    <?php endif; ?>
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Projecte <span>Destacat</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>Proyecto <span>Destacado</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>Featured <span>Project</span></h2>
                        <?php endif; ?>
                        <?php } ?>
                        <p><?php the_title(); ?></p>
                        <a href="portfolio" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>			
                </figure>
                <?php endwhile; ?>
                <?php endif; ?>
            </li><!--
            --><li class="grid">
                <figure class="effect-julia julia2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/portfoli/oleumflumen.jpg" class="media square" alt="About" width="520" height="520" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2><span>Demana'ns pressupost</span></h2>
                        <a href="mailto:judit@sokvist.com?subject=M’interessen els vostres serveis&body=Contacteu-me si-us-plau.  El meu telefon es el ........." title="Envia'ns un e-mail">Envia'ns un e-mail</a>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2><span>Pídenos un presupuesto</span></h2>
                        <a href="mailto:judit@sokvist.com?subject=Me interesan vuestros servicios&body=Contactadme por favor. Mi telefono es el ........." title="Envíanos un e-mail">Envíanos un e-mail</a>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2><span>Ask for a quote</span></h2>
                        <a href="mailto:judit@sokvist.com?subject=I am interested in your services&body=Please get in contact with me. My phone number is ........." title="Send us an email">Email us</a>
                        <?php endif; ?>
                        <?php } ?>
                    </figcaption>			
                </figure>
            </li>
        </ul>
	</section><!-- end main -->
    
    
    <section class="main clearfix">
        <div id="gridder-content-1" class="gridder-content">
            <div class="row row-vcenter">
                <div class="row-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-webdesign-2.jpg" alt="Web Design" width="680" height="452" />
                </div>
                <?php query_posts('post_type=page&name=web-design'); while (have_posts ()): the_post(); ?>
                <div class="row-copy">
                    <?php the_content(); ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div id="gridder-content-2" class="gridder-content">
            <div class="row row-vcenter">
                <div class="row-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-graphic.jpg" alt="SEO" width="680" height="452" />
                </div>
                <?php query_posts('post_type=page&name=graphic-design'); while (have_posts ()): the_post(); ?>
                <div class="row-copy">
                    <?php the_content(); ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div id="gridder-content-3" class="gridder-content">
            <div class="row row-vcenter">
                <?php query_posts('post_type=page&name=digital-marketing'); while (have_posts ()): the_post(); ?>
                <div class="row-copy-full">
                    <?php the_content(); ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>


<?php get_footer(); ?>
