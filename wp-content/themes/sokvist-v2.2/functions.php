<?php
/**
 * Author: Sokvist | @SokvistWeb
 * URL: http://sokvist.com
 * Custom functions, support, custom post types and more. 
 * Based on Todd Motto's HTML5 Blank Theme (html5blank.com)
 */

/*------------------------------------*\
    Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support'))
{

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('work', 520, 520, true); // Custom Thumbnail Size call using the_post_thumbnail('work')
    add_image_size('post', 800, 240, true); // Custom Thumbnail Size call using the_post_thumbnail('post')
    add_image_size('post-full', 900, '', true); // Custom Thumbnail Size call using the_post_thumbnail('post-full')

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
    Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav() {
    wp_nav_menu(
    array(
        'theme_location'  => 'header-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="nav" class="nav navbar-nav">%3$s</ul>',
        'depth'           => 0,
        'walker'          => ''
        )
    );
}

// Footer navigation
function footer_nav() {
    wp_nav_menu(
    array(
        'theme_location'  => 'footer-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul>%3$s</ul>',
        'depth'           => 0,
        'walker'          => ''
        )
    );
}



// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'footer-menu' => __('Footer Menu', 'html5blank'), // Footer Navigation
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}



// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area Blog
    register_sidebar(array(
        'name' => __('Widget Area Blog', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-blog',
        'before_widget' => '<div id="%1$s" class="%2$s widget">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area Cursos
    register_sidebar(array(
        'name' => __('Widget Area Cursos', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-cursos',
        'before_widget' => '<div id="%1$s" class="%2$s widget">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
    
    // Define Sidebar Widget Area Recursos
    register_sidebar(array(
        'name' => __('Widget Area Recursos', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-recursos',
        'before_widget' => '<div id="%1$s" class="%2$s widget">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

}


// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}



// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}


// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}


// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return ' ...';
}


// Remove Admin bar
function remove_admin_bar()
{
    return false;
}


// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}


// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}


// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}


// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}


// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-wrap">
    <?php endif; ?>
    <div class="author-avatar pull-left">
    <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
    </div>
<?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
    <br />
<?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
        <?php printf(__('<cite class="pull-left">%s</cite>'), get_comment_author_link()) ?>
        <?php
            printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
        ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
    <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
<?php }

/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
//add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
//add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
//add_filter('post_thumbnail_html', 'remove_width_attribute', 10 ); // Remove width and height dynamic attributes to post images
//add_filter('image_send_to_editor', 'remove_width_attribute', 10 ); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*--------------------------------------
	Custom Post Types
--------------------------------------*/

// Create 1 Custom Post type, called packs-ofertes
function create_post_type_html5() {
    
    // Custom Post type, Cursos
    register_taxonomy_for_object_type('category', 'curs'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'curs');
    register_post_type('curs', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Cursos', 'html5blank'), // Rename these to suit
            'singular_name' => __('Cursos Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Cursos Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Cursos Custom Post', 'html5blank'),
            'new_item' => __('New Cursos Custom Post', 'html5blank'),
            'view' => __('View Cursos Custom Post', 'html5blank'),
            'view_item' => __('View Cursos Custom Post', 'html5blank'),
            'search_items' => __('Search Cursos Custom Post', 'html5blank'),
            'not_found' => __('No Cursos Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No Cursos Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author',
            'trackbacks',
            'comments',
            'revisions'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
    
    
    // Custom Post type, Work
    register_taxonomy_for_object_type('category', 'work'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'work');
    register_post_type('work', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Work', 'html5blank'), // Rename these to suit
            'singular_name' => __('Work Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Work Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Work Custom Post', 'html5blank'),
            'new_item' => __('New Work Custom Post', 'html5blank'),
            'view' => __('View Work Custom Post', 'html5blank'),
            'view_item' => __('View Work Custom Post', 'html5blank'),
            'search_items' => __('Search Work Custom Post', 'html5blank'),
            'not_found' => __('No Work Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No Work Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'category'
        ), // Add Category and Post Tags support
        'show_in_rest' => true
    ));
    
    
    // Custom Post type, Cursos
    register_taxonomy_for_object_type('category', 'recurs'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'recurs');
    register_post_type('recurs', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Recursos', 'html5blank'), // Rename these to suit
            'singular_name' => __('Recursos Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Recursos Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Recursos Custom Post', 'html5blank'),
            'new_item' => __('New Recursos Custom Post', 'html5blank'),
            'view' => __('View Recursos Custom Post', 'html5blank'),
            'view_item' => __('View Recursos Custom Post', 'html5blank'),
            'search_items' => __('Search Recursos Custom Post', 'html5blank'),
            'not_found' => __('No Recursos Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No Recursos Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author',
            'trackbacks',
            'comments',
            'revisions'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag'//,
            //'category'
        ) // Add Category and Post Tags support
    ));
    
}


// Add categories to custom post Recursos
// https://stackoverflow.com/questions/33214580/restrict-category-for-custom-post-type-in-wordpress
add_action( 'init', 'recursos_cat' );

function recursos_cat() {
    register_taxonomy(
        'recursos-categories',
        'recurs',
        array(
            'label' => __( 'Categories Recursos' ),
            'hierarchical' => true,
        )
    );
}

// Remove p tags from category description
remove_filter('term_description','wpautop');


/*------------------------------------*\
    Sokvist V2 Custom Functions
\*------------------------------------*/
// remove emoji icons (WP 4.2)
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );



// How to move jQuery (and Jquery Migrate) script to the Footer
// http://www.joanmiquelviade.com/how-to-move-jquery-script-to-the-footer/
function reassign_jQuery() {
     wp_deregister_script( 'jquery' );
     wp_deregister_script( 'jquery-core' ); // do not forget this
     wp_deregister_script( 'jquery-migrate' ); // do not forget this

     wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', array(), '1.12.4', TRUE );
     wp_enqueue_script('jquery'); 
} 

// Now you can load all of your jQuery dependent scripts, probably you'll prefer to add a priority actions checking 
if ( ! is_admin() ) 
     add_action('init', 'reassign_jQuery'); 



/*
 * Remove the `wp-block-library.css` file from `wp_head()`
 * https://wpcrux.com/blog/remove-gutenberg-enqueued-css/
 * @author Rahul Arora
 * @since  12182018
 * @uses   wp_dequeue_style
 */
add_action( 'wp_enqueue_scripts', function() {
    wp_dequeue_style( 'wp-block-library' );
} );


// Add a body class based on custom field https://krogsgard.com/2012/wordpress-body-class-post-meta/
// check for a certain meta key on the current post and add a body class if meta value exist
/*function krogs_custom_field_body_class( $classes ) {
	if ( get_post_meta( get_the_ID(), 'adding_class_to_body', true ) ) {
		$classes[] = 'dark-header';
	}
    if ( is_404() ) {
		$classes[] = 'dark-header';
	}
	
	// return the $classes array
	return $classes;
}
add_filter('body_class','krogs_custom_field_body_class');*/


/*function prefix_conditional_body_class( $classes ) {
    if( is_404() )
        $classes[] = 'dark-header';

    return $classes;
}
add_filter( 'body_class', 'prefix_conditional_body_class' );*/



// Page Slug Body Class
// http://www.wpbeginner.com/wp-themes/how-to-add-page-slug-in-body-class-of-your-wordpress-themes/
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


// Add excerpt to page
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() 
{
     add_post_type_support( 'page', 'excerpt' );
}



// Add data attribute to menu
function add_menu_atts( $atts, $item, $args ) {
    $atts['class'] = 'animsition-link';
    $atts['data-animsition-out-class'] = 'fade-out';
    $atts['data-animsition-out-duration'] = '500';
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_menu_atts', 10, 3 );



// Blog Pagination
// http://www.wpbeginner.com/wp-themes/how-to-add-numeric-pagination-in-your-wordpress-theme/
function wp_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link('&laquo;') );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="current"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="current"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="current"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link('&raquo;') );

	echo '</ul>' . "\n";

}


// Media Pagination (custom post type)
// http://www.wpbeginner.com/wp-themes/how-to-add-numeric-pagination-in-your-wordpress-theme/
// http://callmenick.com/post/custom-wordpress-loop-with-pagination
function wp_numeric_custom_posts_nav() {

	if( is_singular('media') ) // http://wordpress.stackexchange.com/questions/78368/is-singular-wont-call-my-functions
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link('&laquo;') );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="current"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="current"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="current"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link('&raquo;') );

	echo '</ul>' . "\n";

}


// Remove Contact Form 7 javascript
/*add_action( 'wp_print_scripts', 'deregister_cf7_javascript', 100 ); // Change 100 to the id of the contact form page
function deregister_cf7_javascript() {
    if ( !is_page(100) ) {
        wp_deregister_script( 'contact-form-7' );
    }
}*/

// Remove Contact Form 7 stylesheet
/*add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 ); // Change 100 to the id of the contact form page
function deregister_cf7_styles() {
    if ( !is_page(100) ) {
        wp_deregister_style( 'contact-form-7' );
    }
}*/


// Stop loading the JavaScript and CSS stylesheet on all pages
// https://contactform7.com/loading-javascript-and-stylesheet-only-when-it-is-necessary/
/*add_filter( 'wpcf7_load_js', '__return_false' );*/
add_filter( 'wpcf7_load_css', '__return_false' );


// Add next/previous post links
// http://www.jaredatchison.com/code/add-next-previous-post-links-genesis/
function ja_prev_next_post_nav() {
	if ( is_singular(  array( 'post', 'recurs' ) ) ) {
		echo '<div class="next-prev ">';
			previous_post_link( '<div class="prev-post">%link</div>', '%title' );
			next_post_link( '<div class="next-post pull-right">%link</div>', '%title' );
		echo '</div>';
	}
}
add_action( 'genesis_before_comments', 'ja_prev_next_post_nav' );


// Exclude Category from Widget
// https://wordpress.org/support/topic/excluding-category-from-widget
function exclude_widget_categories($args){
$exclude = "3,4"; // The IDs of the excluding categories
$args["exclude"] = $exclude;
return $args;
}
add_filter("widget_categories_args","exclude_widget_categories");


// Exclude certain pages from WordPress search results
// https://www.johnparris.com/exclude-certain-pages-from-wordpress-search-results/
function jp_search_filter( $query ) {
  if ( $query->is_search && $query->is_main_query() ) {
    $query->set( 'post__not_in', array( 14,16,21,24,28 ) ); 
  }
}
add_action( 'pre_get_posts', 'jp_search_filter' );


// Custom login page
// https://codex.wordpress.org/Customizing_the_Login_Form
function my_login_logo() { ?>
    <style type="text/css">
        body.login {
            background: rgba(39, 24, 47, 0.05);
        }
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sokvist-logo-sun-v2.1.svg) !important;
            background-size: 200px 120px !important;
            width: 200px !important;
            height: 120px !important;
            margin: 0 auto 10px !important;
            padding-bottom: 0 !important;
        }
        body.login div#login form#loginform p.submit input#wp-submit {
            background: #98D2EB;
            border-color: #98D2EB;
            border-radius: 0;
            text-shadow: none;
            box-shadow: none;
        }
        body.login div#login form#loginform p.submit input#wp-submit:hover,
        body.login div#login form#loginform p.submit input#wp-submit:focus {
            background: #FADF63;
            border-color: #FADF63;
        }
        body.login div#login p#nav a,
        body.login div#login p#backtoblog a {
            color: #111;
        }
        body.login div#login p#nav a:hover,
        body.login div#login p#backtoblog a:hover {
            color: #98D2EB;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Sokvist Web & SEO';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


