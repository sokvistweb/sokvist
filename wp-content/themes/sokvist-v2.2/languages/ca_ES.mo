��    +      t  ;   �      �  
   �     �     �     �  	   �  :   �     4     <     \     e     z     �     �  #   �     �     �  
   �               )     6     J  !   f  *   �     �  D   �  
                  ,     3     R     _     z     �     �     �     �     �     �        $     �  3      	     -	     :	     X	     a	  :   m	     �	  ,   �	     �	     �	     �	     
      !
     B
     a
  +   h
     �
  &   �
     �
     �
     �
  *     ;   ,  J   h     �  F   �          !     .     D  +   K     w     �     �     �     �  $   �       (        @     R  #   d     &         +               !          "              #      '                 $                         )         
                       %                 	          (       *                              % Comments %1$s at %2$s %s Search Results for  (Edit) 1 Comment <span class="fn">%s</span> <span class="says">says:</span> Add New Add New HTML5 Blank Custom Post Archives Author Archives for  Categories for Categorised in:  Comments are closed here. Description for this widget-area... Edit Edit HTML5 Blank Custom Post Extra Menu HTML5 Blank Custom Post Header Menu Latest Posts Leave your thoughts New HTML5 Blank Custom Post No HTML5 Blank Custom Posts found No HTML5 Blank Custom Posts found in Trash Page not found Post is password protected. Enter the password to view any comments. Powered by Published by Return home? Search Search HTML5 Blank Custom Post Sidebar Menu Sorry, nothing to display. Tag Archive:  Tags:  This post was written by  To search, type and hit enter. View Article View HTML5 Blank Custom Post Widget Area 1 Widget Area 2 Your comment is awaiting moderation. Project-Id-Version: HTML5 Blank WordPress Theme
POT-Creation-Date: 2012-10-18 00:53+0100
PO-Revision-Date: 
Last-Translator: Fabián Núñez <fanuneza@gmail.com>
Language-Team: Ioan Virag | www.ioanvirag.com <ioan@ioanvirag.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 % Comentaris %1$s en %2$s %s resultats de la cerca per  (Editar) 1 Comentari <cite class="fn">%s</cite> <span class="says">dice:</span> Añadir nuevo Añadir entrada personalizada de HTML5 Blank Arxius Archivos de autor de Categories per:  Categoritzat a:  Los comentarios están cerrados. Descripción para esta zona... Editar Editar entrada personalizada de HTML5 Blank Menú extra Entradas personalizadas de HTML5 Blank Menú de la cabecera Derreres entrades Deixa un comentari Nueva entrada personalizada de HTML5 Blank No se han encontrado entradas personalizadas de HTML5 Blank No se han encontrado entradas personalizadas de HTML5 Blank en la papelera Página no encontrada Entrada protegida. Introduzca la contraseña para ver los comentarios. Desarrollado con Publicat per ¿Regresar al Inicio? Buscar Buscar entrada personalizada de HTML5 Blank Menú de la barra lateral Disculpa, nada para mostrar. Arxiu d'etiquetes:  Etiquetes:  Entrada escrita per Per buscar, escriu i pressiona Enter Veure l'article Ver entrada personalizada de HTML5 Blank Zona 1 de widgets Zona 2 de widgets El seu comentari espera moderació. 