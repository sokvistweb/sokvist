<?php /* Template Name: Page Industry */ get_header('industry'); ?>


<main>
    <section class="hero text-center">
        <div class="container-sm">
            <div class="hero-inner">
                <h1 class="hero-title h2-mobile mt-0 is-revealing"><?php the_title(); ?></h1>
                <h2 class="hero-tagline is-revealing"><?php the_field( 'tagline' ); ?></h2>

                <p class="cta-intro is-revealing"><?php the_field( 'text_intro' ); ?></p>
                
                <p class="cta-intro is-revealing"><?php the_field( 'text_contactans' ); ?></p>

                <div class="hero-button newsletter-form field-grouped field-alone is-revealing">
                    <button class="button button-primary button-block button-shadow js-tingle-modal-1"><?php the_field( 'boto_contactans' ); ?></button>
                </div>

                <p class="cta-intro is-revealing"><?php the_field( 'lead_magnet' ); ?></p>


                <div class="hero-browser">
                    <div class="hero-browser-inner is-revealing">
                        <img src="<?php echo get_template_directory_uri(); ?>/industry/assets/images/digital-marketing.jpg" alt="Sokvist -Consultoria digital per a departaments de vendes" width="768" height="433">
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="features section text-center">
        <div class="container">
            <?php if ( have_rows( 'seccio_2' ) ) : ?>
            <?php while ( have_rows( 'seccio_2' ) ) : the_row(); ?>
            <div class="features-inner section-inner has-bottom-divider">
                <h2 class="features-title is-revealing"><?php the_sub_field( 'titol_1' ); ?></h2>
                <div class="features-wrap">
                    <?php if( get_sub_field( 'subtitol_1' ) || get_sub_field( 'text_1' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M54.8 67.3C66.1 67.3 72 38.7 72 27.5c0-11.3-8.5-15.7-19.8-15.7S13.8 34.7 13.8 46s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M44.5 19.5v40.9h-9.1V19.5h9.1z" fill="#786a2f"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_1' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_1' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_2' ) || get_sub_field( 'text_2' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M54.8 67.3C66.1 67.3 72 38.7 72 27.5c0-11.3-8.5-15.7-19.8-15.7S13.8 34.7 13.8 46s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M53.5 52.4v8.3h-27v-1.1c2.5-3 5.3-6.9 8.5-11.5s5.2-7.9 6-9.9c.8-2 1.3-3.8 1.3-5.7 0-1.6-.5-3-1.6-4.1-1.1-1.2-2.3-1.7-3.9-1.7-3 0-6.3 1.7-9.8 5.1v-8.6c4-2.6 7.8-3.9 11.5-3.9 4 0 7.2 1 9.5 3.1s3.5 4.9 3.5 8.5c0 4.9-3.7 12.1-11.1 21.5h13.1z" fill="#786a2f"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_2' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_2' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="features-wrap">
                    <?php if( get_sub_field( 'subtitol_3' ) || get_sub_field( 'text_3' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M54.8 67.3C66.1 67.3 72 38.7 72 27.5c0-11.3-8.5-15.7-19.8-15.7S13.8 34.7 13.8 46s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M36.4 42.8v-7.2c1.7 0 3.1-.5 4.1-1.4 1-1 1.5-2.1 1.5-3.4 0-1.4-.5-2.5-1.6-3.4-1.1-.9-2.5-1.3-4.2-1.3-2.3 0-4.8.7-7.5 2v-7.2c2.7-1.2 5.8-1.8 9.3-1.8 3.9 0 7 .9 9.5 2.8 2.4 1.9 3.6 4.3 3.6 7.2 0 4.1-2 7.2-6.1 9.5 4.8 1.9 7.2 5.4 7.2 10.5 0 3.6-1.3 6.5-4 8.7-2.6 2.2-6.2 3.3-10.6 3.3-3.6 0-7-.8-10-2.3v-7.9c3.1 2.2 6 3.3 8.6 3.3 1.9 0 3.4-.5 4.6-1.5 1.2-1 1.8-2.3 1.8-3.9s-.6-2.9-1.7-4c-1-1.3-2.5-1.9-4.5-2z" fill="#786a2f"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_3' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_3' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_4' ) || get_sub_field( 'text_4' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M54.8 67.3C66.1 67.3 72 38.7 72 27.5c0-11.3-8.5-15.7-19.8-15.7S13.8 34.7 13.8 46s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M51.1 19.3v24.8h2.8v6.5h-2.8v10.1H42V50.6H26v-6.5l19.2-24.8h5.9zM33.3 44.1h8.8V32.9l-8.8 11.2z" fill="#786a2f"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_4' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_4' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="features-wrap">
                    <?php if( get_sub_field( 'subtitol_5' ) || get_sub_field( 'text_5' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M54.8 67.3C66.1 67.3 72 38.7 72 27.5c0-11.3-8.5-15.7-19.8-15.7S13.8 34.7 13.8 46s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M51 19.3v8.2H37.9v7.6c4.4-.3 7.9.8 10.5 3.3s4 5.6 4 9.3c0 3.9-1.3 7.1-4 9.5-2.7 2.4-6.3 3.5-10.8 3.5-4 0-7.3-.7-9.9-2.2v-7.4c3.1 1.8 6 2.6 8.7 2.6 1.9 0 3.4-.5 4.6-1.6 1.2-1.1 1.8-2.4 1.8-4.1 0-1.8-.7-3.3-2.2-4.3-1.5-1.1-3.5-1.6-6.2-1.6-1.4 0-3 .2-4.7.5V19.3H51z" fill="#786a2f"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_5' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_5' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_6' ) || get_sub_field( 'text_6' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M54.8 67.3C66.1 67.3 72 38.7 72 27.5c0-11.3-8.5-15.7-19.8-15.7S13.8 34.7 13.8 46s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M49.2 19l-9.5 15.5c.9-.2 1.8-.4 2.7-.4 3.1 0 5.7 1.2 7.9 3.5 2.2 2.4 3.3 5.5 3.3 9.3 0 4-1.3 7.3-3.9 10s-5.8 4-9.7 4c-4 0-7.3-1.3-9.8-4-2.5-2.7-3.8-6.2-3.8-10.5 0-4.7 1.9-10 5.6-16L39.1 19h10.1zm-9 34.3c1.4 0 2.5-.5 3.4-1.6.8-1.1 1.3-2.6 1.3-4.4 0-1.8-.4-3.3-1.3-4.4-.9-1.1-2-1.7-3.4-1.7s-2.5.6-3.3 1.7-1.2 2.6-1.2 4.6c0 1.9.4 3.3 1.2 4.4.7.9 1.8 1.4 3.3 1.4z" fill="#786a2f"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_6' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_6' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>


    <section class="features hero-mobile section text-center">
        <div class="container">
        <?php if ( have_rows( 'seccio_3' ) ) : ?>
        <?php while ( have_rows( 'seccio_3' ) ) : the_row(); ?>
            <div class="features-inner section-inner has-bottom-divider">

                <h2 class="features-title is-revealing"><?php the_sub_field( 'titol_1' ); ?></h2>

                <div class="hero-browser">
                    <div class="hero-browser-inner is-revealing">
                        <img src="<?php echo get_template_directory_uri(); ?>/industry/assets/images/digitalitzar.jpg" alt="Sokvist - Procés digitalitzat de vendes" width="768" height="482">
                    </div>
                </div>

                <div class="features-wrap">
                    <?php if( get_sub_field( 'subtitol_1' ) || get_sub_field( 'text_1' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M52.4 67c11.1 0 16.9-28 16.9-39 0-11.1-8.4-15.4-19.5-15.4S12.3 35.2 12.3 46.2C12.3 57.3 41.4 67 52.4 67z" fill="#fdf5d1"/><path d="M49.8 28.5v24.6m-8.7-14.5v14.5m-8.7-24.6v24.6m-8.7-14.5v14.5m34.8-34.8v34.8" fill="none" stroke="#786a2f" stroke-width="5" stroke-linecap="square"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_1' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_1' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_2' ) || get_sub_field( 'text_2' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M51.6 66.5c11.2 0 17.1-28.4 17.1-39.6S60.2 11.3 49 11.3s-38 22.8-38 34c-.1 11.3 29.3 21.2 40.6 21.2z" fill="#fdf5d1"/><path d="M26.3 50.2l29-29" fill="none" stroke="#c7b04e" stroke-width="5" stroke-linecap="square"/><path d="M26.3 38.9l12.4-12.4m-14.4 4.1l4.1-4.1m1.1 32l21.8-21.8M51 48.4l9.7-9.7" fill="none" stroke="#fadf63" stroke-width="5" stroke-linecap="square"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_2' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_2' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_3' ) || get_sub_field( 'text_3' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <div class="feature-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path d="M53.1 67.5c11.3 0 17.2-28.6 17.2-39.8 0-11.3-8.5-15.7-19.8-15.7S12.1 34.9 12.1 46.2s29.7 21.3 41 21.3z" fill="#fdf5d1"/><path d="M41.3 37c-2.3 0-4.2 1.8-4.2 4.1s1.8 4.1 4.2 4.1 4.2-1.8 4.2-4.1-2-4.1-4.2-4.1zm12.4 4.1c0-6.8-5.7-12.3-12.4-12.3s-12.4 5.5-12.4 12.3c0 4.5 2.5 8.5 6.3 10.7l2.1-3.6c-2.5-1.5-4.2-4.1-4.2-7.1 0-4.5 3.7-8.2 8.4-8.2 4.5 0 8.4 3.7 8.4 8.2 0 3.1-1.7 5.7-4.2 7.1l2.1 3.6c3.5-2.1 5.9-6.2 5.9-10.7zM41.3 20.6c-11.4 0-20.8 9.2-20.8 20.5 0 7.6 4.2 14.1 10.3 17.8l2.1-3.6c-5-2.8-8.4-8.1-8.4-14.1 0-9.1 7.5-16.5 16.6-16.5s16.6 7.4 16.6 16.5c0 6-3.3 11.4-8.4 14.1l2.1 3.6c6.3-3.6 10.3-10.2 10.3-17.8.4-11.3-9-20.5-20.4-20.5z" fill="#c7b04e"/></svg>
                            </div>
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_3' ); ?></h3>
                            <p class="text-xs"><?php the_sub_field( 'text_3' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </section>


    <section class="features section who-you-are text-center">
        <div class="container">
            <?php if ( have_rows( 'seccio_4' ) ) : ?>
            <?php while ( have_rows( 'seccio_4' ) ) : the_row(); ?>
            <div class="features-inner section-inner has-bottom-divider">
                <h2 class="features-title is-revealing"><?php the_sub_field( 'titol_1' ); ?></h2>

                <div class="features-wrap">
                    <?php if( get_sub_field( 'text_1' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm"><?php the_sub_field( 'text_1' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'text_2' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm"><?php the_sub_field( 'text_2' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="features-wrap">
                    <?php if( get_sub_field( 'text_3' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm"><?php the_sub_field( 'text_3' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'text_4' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm"><?php the_sub_field( 'text_4' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                
                <div class="features-wrap">
                    <?php if( get_sub_field( 'text_5' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm"><?php the_sub_field( 'text_5' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'text_6' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <p class="text-sm"><?php the_sub_field( 'text_6' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </section>


    <section class="who-we-are text-center">
        <div class="container-sm">
        <?php if ( have_rows( 'seccio_5' ) ) : ?>
        <?php while ( have_rows( 'seccio_5' ) ) : the_row(); ?>
            <div class="hero-inner has-bottom-divider">
                <h2 class="features-title is-revealing"><?php the_sub_field( 'titol_1' ); ?></h2>
                <?php if( get_sub_field( 'text_1' ) ): ?>
                <p class="cta-intro who-intro is-revealing"><?php the_sub_field( 'text_1' ); ?></p>
                <?php endif; ?>
                <?php if( get_sub_field( 'text_2' ) ): ?>
                <p class="cta-intro who-intro is-revealing"><?php the_sub_field( 'text_2' ); ?></p>
                <?php endif; ?>
                <?php if( get_sub_field( 'text_3' ) ): ?>
                <p class="cta-intro who-intro is-revealing"><?php the_sub_field( 'text_3' ); ?></p>
                <?php endif; ?>
                <?php if( get_sub_field( 'text_4' ) ): ?>
                <p class="cta-intro who-intro is-revealing"><?php the_sub_field( 'text_4' ); ?></p>
                <?php endif; ?>
                <?php if( get_sub_field( 'text_5' ) ): ?>
                <p class="cta-intro who-intro is-revealing"><?php the_sub_field( 'text_5' ); ?></p>
                <?php endif; ?>
                <?php if( get_sub_field( 'text_6' ) ): ?>
                <p class="cta-intro who-intro is-revealing"><?php the_sub_field( 'text_6' ); ?></p>
                <?php endif; ?>
                
                <div class="cta-intro is-revealing avatar">
                    <img src="<?php echo get_template_directory_uri(); ?>/industry/assets/images/judit.jpg" alt="<?php the_sub_field( 'avatar_nom' ); ?>" width="120" height="120">
                    <h3><?php the_sub_field( 'avatar_nom' ); ?></h3>
                    <p><?php the_sub_field( 'avatar_tagline' ); ?></p>
                </div>
                
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
            
        </div>
        
        
    </section>
    
    
    <section class="features digital-tools section text-center">
        <div class="container">
            <?php if ( have_rows( 'seccio_6' ) ) : ?>
            <?php while ( have_rows( 'seccio_6' ) ) : the_row(); ?>
            <div class="features-inner section-inner has-bottom-divider">
                <h2 class="features-title is-revealing"><?php the_sub_field( 'titol_1' ); ?></h2>

                <div class="features-wrap">
                    <?php if( get_sub_field( 'subtitol_1' ) || get_sub_field( 'text_1' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_1' ); ?></h3>
                            <p class="text-sm"><?php the_sub_field( 'text_1' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_2' ) || get_sub_field( 'text_2' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_2' ); ?></h3>
                            <p class="text-sm"><?php the_sub_field( 'text_2' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="features-wrap">
                    <?php if( get_sub_field( 'subtitol_3' ) || get_sub_field( 'text_3' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_3' ); ?></h3>
                            <p class="text-sm"><?php the_sub_field( 'text_3' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_4' ) || get_sub_field( 'text_4' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_4' ); ?></h3>
                            <p class="text-sm"><?php the_sub_field( 'text_4' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                
                <div class="features-wrap">
                    <?php if( get_sub_field( 'subtitol_5' ) || get_sub_field( 'text_5' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_5' ); ?></h3>
                            <p class="text-sm"><?php the_sub_field( 'text_5' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_6' ) || get_sub_field( 'text_6' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_6' ); ?></h3>
                            <p class="text-sm"><?php the_sub_field( 'text_6' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                
                <div class="features-wrap">
                    <?php if( get_sub_field( 'subtitol_7' ) || get_sub_field( 'text_7' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_7' ); ?></h3>
                            <p class="text-sm"><?php the_sub_field( 'text_7' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field( 'subtitol_8' ) || get_sub_field( 'text_8' ) ): ?>
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <h3 class="feature-title"><?php the_sub_field( 'subtitol_8' ); ?></h3>
                            <p class="text-sm"><?php the_sub_field( 'text_8' ); ?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </section>
    
    
    <section class="hero what-we-do text-center">
        <div class="container-sm">
            <?php if ( have_rows( 'seccio_7' ) ) : ?>
            <?php while ( have_rows( 'seccio_7' ) ) : the_row(); ?>
            <div class="hero-inner">
                <h3 class="hero-tagline is-revealing"><?php the_sub_field( 'titol_1' ); ?></h3>
                
                <?php if( get_sub_field( 'text_1' ) ): ?>
                <p class="cta-intro is-revealing"><?php the_sub_field( 'text_1' ); ?></p>
                <?php endif; ?>
                
                <?php if( get_sub_field( 'text_2' ) ): ?>
                <p class="cta-intro is-revealing"><?php the_sub_field( 'text_2' ); ?></p>
                <?php endif; ?>
                
                <?php if( get_sub_field( 'text_3' ) ): ?>
                <p class="cta-intro is-revealing"><?php the_sub_field( 'text_3' ); ?></p>
                <?php endif; ?>
                
                <?php if( get_sub_field( 'text_4' ) ): ?>
                <p class="cta-intro is-revealing"><?php the_sub_field( 'text_4' ); ?></p>
                <?php endif; ?>

                <div class="features-wrap features-donate">
                    <div class="feature is-revealing">
                        <div class="feature-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 400 400" xml:space="preserve"><circle cx="207.2" cy="191" r="15.6" fill="#fadf63"/><circle cx="160.4" cy="191" r="15.6" fill="#fadf63"/><path d="M252.6 13.1C184.4 13.1 21 167 21 243s179.1 143.2 247.3 143.2S372 193.9 372 118.6c0-75.9-51.3-105.5-119.4-105.5zM308.4 222c0 19.3-16.6 34.8-37.1 34.8h-1.8V288s-38.4-26.1-41.7-28.6c-3.4-2.5-3.4-2.5-10.4-2.5H143c-20.5 0-37.1-15.5-37.1-34.8v-61.7c0-19.3 16.6-34.9 37.1-34.9h128.2c20.5 0 37.1 15.6 37.1 34.9V222z" fill="#fadf63"/><circle cx="253.9" cy="191" r="15.6" fill="#fadf63"/></svg>
                        </div>
                    </div>

                    <div class="feature is-revealing">
                        <div class="feature-inner feature-donate">
                            <ul>
                                <?php if( get_sub_field( 'curs_1' ) ): ?>
                                <li><?php the_sub_field( 'curs_1' ); ?></li>
                                <?php endif; ?>
                                <?php if( get_sub_field( 'curs_2' ) ): ?>
                                <li><?php the_sub_field( 'curs_2' ); ?></li>
                                <?php endif; ?>
                                <?php if( get_sub_field( 'curs_3' ) ): ?>
                                <li><?php the_sub_field( 'curs_3' ); ?></li>
                                <?php endif; ?>
                                <?php if( get_sub_field( 'curs_4' ) ): ?>
                                <li><?php the_sub_field( 'curs_4' ); ?></li>
                                <?php endif; ?>
                                <?php if( get_sub_field( 'curs_5' ) ): ?>
                                <li><?php the_sub_field( 'curs_5' ); ?></li>
                                <?php endif; ?>
                                <?php if( get_sub_field( 'curs_6' ) ): ?>
                                <li><?php the_sub_field( 'curs_6' ); ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </section>


     <section class="features section contactform">
        <div class="container">
            <?php if ( have_rows( 'seccio_8' ) ) : ?>
            <?php while ( have_rows( 'seccio_8' ) ) : the_row(); ?>
            <div class="features-inner section-inner has-bottom-divider">

                <div class="contact-form newsletter-header text-center is-revealing">

                    <div class="features-wrap features-donate">
                        <div class="feature is-revealing">
                            <div class="feature-inner feature-donate">
                                <h2 class="section-title"><?php the_sub_field( 'titol_1' ); ?></h2>
                                <?php if( get_sub_field( 'text' ) ): ?>
                                <p class="section-p"><?php the_sub_field( 'text' ); ?></p>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="feature is-revealing">
                            <div class="feature-inner feature-donate">

                                <div class="tingle-demo tingle-demo-tiny tingle">
                                
                                    <?php the_sub_field( 'formulari' ); ?>
                                
                                </div>

                            </div>
                        </div>
                    </div>
               </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </section>


    <section class="newsletter section">
        <div class="container-sm">
            <?php if ( have_rows( 'seccio_9' ) ) : ?>
            <?php while ( have_rows( 'seccio_9' ) ) : the_row(); ?>
            <div class="newsletter-inner section-inner has-bottom-divider">

                <div class="newsletter-header text-center is-revealing">

                    <h2><?php the_sub_field( 'titol_1' ); ?></h2>

                    <img src="<?php echo get_template_directory_uri(); ?>/industry/assets/images/guia-com-incorporar-un-xatbot.jpg" alt="Sokvist - Vista prèvia del document pdf" width="200" height="283">

                    <p class="section-paragraph"><?php the_sub_field( 'text' ); ?></p>
                </div>

                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=324f6df81d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div class="footer-form newsletter-form field field-grouped is-revealing">
                        <div class="control control-expanded">
                            <input type="email" value="" name="EMAIL" class="input email" id="mce-EMAIL" placeholder="El teu email" required>
                        </div>

                        <div class="control">
                            <input type="submit" value="Descarrega't la guia" name="subscribe" id="mc-embedded-subscribe" class="button button-primary button-block button-shadow">
                        </div>
                    </div>
                </form>

                <p class="text-center text-small is-revealing"><small>Tenim cura de les teves dades, tal com s’explica en la nostra <a href="/politica-de-privacitat" title="Consulta els nostres termes legals">Política de privacitat</a>.</small></p>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </section>

</main>



<?php get_footer('industry'); ?>