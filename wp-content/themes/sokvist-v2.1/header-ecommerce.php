<!DOCTYPE html>
<html lang="ca">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47544981-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-47544981-1');
    </script>
    <!-- /Global Site Tag (gtag.js) - Google Analytics -->
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W5B9CTJ');</script>
    <!-- End Google Tag Manager -->
    
    
    <meta charset="utf-8">
    <title>Sokvist | E-commerce</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=": E-commerce, mobile commerce i transformació digital quan als teus clients els importa la teva imatge.">
    <meta name="keywords" content="e-commerce, mobile, marketing, SEO" />
    <meta name="author" content="Sokvist" />

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/ecommerce/assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,900,900i" rel="stylesheet">
    
    
    <!-- Facebook Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://sokvist.com/ecommerce/">
    <meta property="og:title" content="Sokvist · E-commerce">
    <meta property="og:image" content="assets/images/og-image-ecommerce.jpg">
    <meta property="og:description" content="E-commerce, mobile commerce i transformació digital quan als teus clients els importa la teva imatge.">
    <meta property="og:site_name" content="Sokvist · E-commerce">
    <meta property="og:locale" content="ca">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    
    <!-- Twitter Card -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@SokvistWeb">
    <meta name="twitter:creator" content="@SokvistWeb">
    <meta name="twitter:url" content="https://sokvist.com/ecommerce/">
    <meta name="twitter:title" content="Sokvist · E-commerce">
    <meta name="twitter:description" content="E-commerce, mobile commerce i transformació digital quan als teus clients els importa la teva imatge.">
    <meta name="twitter:image" content="assets/images/og-image-ecommerce.jpg">
    
    <?php wp_head(); ?>

</head>


<body class="is-boxed has-animations">
    <div class="body-wrap boxed-container">
        <header class="site-header">
            <div class="container">
                <div class="site-header-inner">
                    <div class="brand header-brand">
                        <a class="m-0" href="#">
                            <svg id="logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 180" width="120" height="36"><g id="skv-text"><path class="st0" d="M90.4 53.3c-.9 1.5-1.9 2.6-2.9 3.3-1 .7-2.3 1.1-3.9 1.1-1.4 0-2.9-.4-4.6-1.3-1.6-.9-3.5-1.9-5.5-3-2.1-1.1-4.4-2.1-7.1-3-2.6-.9-5.7-1.3-9-1.3-5.8 0-10.2 1.2-13 3.7-2.9 2.5-4.3 5.9-4.3 10.1 0 2.7.9 4.9 2.6 6.7 1.7 1.8 4 3.3 6.8 4.6 2.8 1.3 6 2.5 9.6 3.5 3.6 1.1 7.3 2.3 11 3.6 3.7 1.3 7.4 2.9 11 4.7 3.6 1.8 6.8 4.1 9.6 6.8 2.8 2.8 5.1 6.1 6.8 10.1 1.7 4 2.6 8.7 2.6 14.3 0 6.2-1.1 12-3.2 17.4-2.2 5.4-5.3 10.1-9.4 14.1-4.1 4-9.1 7.2-15.1 9.5s-12.8 3.5-20.4 3.5c-4.2 0-8.4-.4-12.8-1.3-4.3-.9-8.5-2.1-12.6-3.6-4.1-1.6-7.9-3.4-11.4-5.6-3.6-2.1-6.7-4.5-9.3-7.2l9.2-14.6c.7-1.1 1.6-2 2.9-2.7 1.2-.7 2.5-1 4-1 1.8 0 3.7.6 5.6 1.8 1.9 1.2 4 2.5 6.4 3.9 2.4 1.4 5.1 2.7 8.2 3.9 3.1 1.2 6.7 1.8 10.9 1.8 5.7 0 10-1.2 13.2-3.7 3.1-2.5 4.7-6.4 4.7-11.8 0-3.1-.9-5.7-2.6-7.6-1.7-2-4-3.6-6.8-4.9-2.8-1.3-6-2.4-9.5-3.4-3.6-1-7.2-2.1-11-3.3-3.7-1.2-7.4-2.7-11-4.4-3.6-1.8-6.7-4.1-9.5-6.9-2.8-2.9-5.1-6.4-6.8-10.7-1.7-4.3-2.6-9.5-2.6-15.8 0-5 1-10 3-14.7 2-4.8 5-9.1 8.9-12.8 3.9-3.7 8.8-6.7 14.5-9 5.7-2.2 12.3-3.4 19.6-3.4 4.1 0 8.1.3 12 1 3.9.6 7.6 1.6 11.1 2.9 3.5 1.3 6.8 2.8 9.8 4.5 3 1.8 5.8 3.7 8.2 5.9l-7.9 14.3zM239.9 22.3v77.3h4.2c1.8 0 3.1-.2 4.1-.7 1-.5 1.9-1.4 2.7-2.7L269 69.3c1-1.6 2.3-2.8 3.7-3.5 1.4-.7 3.3-1.1 5.5-1.1h26.2l-24.7 34c-1.2 1.6-2.4 3-3.8 4.2-1.4 1.2-2.9 2.3-4.5 3.3 2.8 2.1 5.2 4.9 7.2 8.3l26.9 45.7h-25.8c-2.2 0-4-.4-5.6-1.1-1.6-.7-2.8-1.9-3.8-3.7l-18.1-33.8c-.9-1.5-1.8-2.5-2.7-3-.9-.5-2.3-.7-4.1-.7h-5.5v42.3h-28.6v-138h28.6z"/><path class="st0" d="M364.9 160.3h-26l-37.1-95.6h23.8c2 0 3.7.5 5.1 1.4 1.4 1 2.3 2.1 2.8 3.5l13.3 42.9c1 3.5 2 6.9 3 10.2 1 3.3 1.8 6.6 2.4 10 .7-3.3 1.5-6.6 2.5-10 1-3.3 2-6.7 3.1-10.2l13.8-42.9c.5-1.4 1.4-2.6 2.8-3.5 1.3-1 2.9-1.4 4.8-1.4h22.7l-37 95.6zM441.2 38.5c0 2.3-.5 4.4-1.4 6.4-.9 2-2.2 3.7-3.8 5.3-1.6 1.5-3.5 2.7-5.6 3.6-2.1.9-4.4 1.3-6.8 1.3-2.3 0-4.5-.4-6.5-1.3s-3.8-2.1-5.4-3.6c-1.6-1.5-2.8-3.3-3.7-5.3-.9-2-1.3-4.1-1.3-6.4 0-2.3.4-4.5 1.3-6.5s2.1-3.8 3.7-5.3 3.4-2.7 5.4-3.6c2-.9 4.2-1.3 6.5-1.3 2.4 0 4.7.4 6.8 1.3 2.1.9 4 2.1 5.6 3.6 1.6 1.5 2.9 3.3 3.8 5.3.9 2 1.4 4.2 1.4 6.5zm-3.4 26.2v95.6h-28.6V64.7h28.6zM513.8 85.6c-.7 1.2-1.5 2-2.3 2.5-.8.5-1.9.7-3.2.7-1.4 0-2.7-.3-4.1-.9-1.4-.6-2.8-1.2-4.4-1.9-1.6-.7-3.3-1.3-5.3-1.9-2-.6-4.2-.9-6.7-.9-3.6 0-6.4.7-8.3 2.1-1.9 1.4-2.9 3.3-2.9 5.8 0 1.8.6 3.3 1.9 4.4 1.3 1.2 2.9 2.2 5 3.1 2.1.9 4.5 1.7 7.1 2.5 2.6.8 5.3 1.7 8.1 2.7 2.8 1 5.5 2.1 8.1 3.5 2.6 1.3 5 2.9 7.1 4.9 2.1 1.9 3.8 4.3 5 7.1 1.3 2.8 1.9 6.1 1.9 10 0 4.7-.9 9.1-2.6 13.1-1.7 4-4.3 7.4-7.7 10.3-3.4 2.9-7.6 5.1-12.6 6.7-5 1.6-10.8 2.4-17.3 2.4-3.3 0-6.5-.3-9.8-.9-3.3-.6-6.4-1.4-9.4-2.5-3-1-5.9-2.3-8.4-3.7-2.6-1.4-4.8-3-6.6-4.7l6.6-10.5c.8-1.2 1.8-2.2 2.9-2.9 1.1-.7 2.5-1.1 4.3-1.1 1.7 0 3.1.4 4.5 1.1s2.7 1.5 4.3 2.4c1.5.9 3.3 1.7 5.4 2.4 2.1.7 4.7 1.1 7.8 1.1 2.2 0 4.1-.2 5.6-.7 1.5-.5 2.8-1.1 3.7-1.9.9-.8 1.6-1.7 2-2.7.4-1 .6-2 .6-3 0-1.9-.6-3.5-1.9-4.7-1.3-1.2-3-2.3-5.1-3.2-2.1-.9-4.5-1.7-7.1-2.5-2.7-.8-5.4-1.6-8.2-2.6-2.8-1-5.5-2.2-8.2-3.5-2.7-1.4-5.1-3.1-7.1-5.3-2.1-2.1-3.8-4.7-5.1-7.8-1.3-3.1-1.9-6.8-1.9-11.2 0-4.1.8-7.9 2.3-11.6 1.6-3.7 3.9-6.9 7.1-9.6 3.1-2.8 7.1-5 11.8-6.6 4.7-1.6 10.3-2.4 16.6-2.4 3.4 0 6.8.3 10 .9 3.2.6 6.3 1.5 9.1 2.6s5.4 2.4 7.7 3.9 4.4 3.1 6.2 4.9l-6.5 10.1zM564.5 161.8c-4.5 0-8.6-.7-12-2-3.5-1.3-6.4-3.2-8.8-5.6-2.4-2.4-4.2-5.4-5.4-8.8-1.2-3.4-1.8-7.3-1.8-11.5V84.4h-8.1c-1.5 0-2.7-.5-3.8-1.4-1-1-1.6-2.3-1.6-4.2V67.7l15.2-2.9 5.6-23.3c.7-2.9 2.8-4.4 6.3-4.4H565V65h23.2v19.5H565V132c0 2.2.5 4 1.6 5.5 1.1 1.4 2.6 2.2 4.7 2.2 1 0 1.9-.1 2.6-.3.7-.2 1.3-.5 1.8-.7.5-.3 1-.5 1.5-.7.5-.2 1-.3 1.6-.3.9 0 1.6.2 2.1.6.5.4 1.1 1 1.6 1.9l8.7 13.5c-3.7 2.8-7.8 4.8-12.4 6.2-4.6 1.2-9.4 1.9-14.3 1.9z"/></g><g id="skv-eye"><path class="st0" d="M202.2 110.5c-2.4-11.7-5.9-21.2-14.2-30-7.1-7.5-17.5-11.4-27.4-13.5-8.9-1.9-17.8-2-26.4 1.5-9.8 3.9-16.3 12.8-22.1 21.2-12.4 17.8-12.9 44.2 4.2 59.4 7.7 6.8 17.1 12.7 27.7 12.6 8.4 0 17.4.3 25.7-1.4 22.1-4.7 37.1-27.6 32.5-49.8zm-44.5 23l.1.3c-.8-.6-3.2.1-4.4.1-2.8.1-5.9.5-8.6-.3-12.1-3.8-15.3-17.5-8.3-27.4 4-5.6 8.9-9 16-8.1 6.3.9 12.4 3.9 15.3 9.8 5 9.8 1.7 23.1-10.1 25.6zM139.5 53.9c.2 1.5-.9 2.4-2.9 3.3-2 .9-4.1 1-4.4 0-2.5-8.2-5.3-11.5-5.3-11.5-.7-1.7.9-2.4 2.9-3.3 2-.9 4.8-.8 5.2.1 3.3 7.1 4.5 11.4 4.5 11.4M121.3 63.6c.8.8.1 2.7-1.5 4.2-1.6 1.6-3.6 2.3-4.3 1.4-4.6-6.1-5.5-6.1-9.6-9.8-1.6-1.3-.5-3.6 1.1-5.2s5.2-2.5 5.7-1.5c3.8 6.5 8.6 10.9 8.6 10.9M185.9 65.7c-.7.9-2.7.6-4.5-.7-1.8-1.3-2.7-3.1-2.1-4 0 0 3-6.4 7.8-12.1.7-.9 2.7-.9 4.5.4 1.8 1.3 3.8 4.4 2.9 5.1-5 4.6-8.6 11.3-8.6 11.3zM163.8 53.3c0 1.1-1.9 1.9-4.2 1.8-2.2-.2-3.7-1.2-3.9-2.4-.6-4.4-1.1-5.2.1-14.8.2-1.4 2.8-2.2 5.1-2 2.2.2 4.3 1.6 4.3 2.8-.2 6.7-1.6 9-1.4 14.6z"/></g></svg>
                        </a>
                    </div>
                </div>
            </div>
        </header>