<?php /* Template Name: Page Consultoria Maketing */ get_header(); ?>

    
    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><?php the_title(); ?></h1>
        </div>
    </section>


    <?php if(function_exists('qtranxf_getLanguage')) { ?>
    <?php if (qtranxf_getLanguage()=='ca'): ?>
    <!-- ********** Català ********** -->


    <section class="main clearfix consulting">
        <div class="tabs-container">
            
            <h2>Demana una consultoria gratuïta sobre marketing online o branding</h2>
            
            <p>Vindrem a conèixer el teu negoci i farem un anàlisi per a detectar quines accions et cal fer per fer-te més visible, arribar al teu client ideal, i seduir-lo una vegada i una altra.</p>
            
            <p>Sol·licita-ho aquí</p>
            
            <div class="widget widget-subscribe">
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup" class="subscribe-widgetbar">
                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=98086caf5a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                        </div>
                        <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                        </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response"></div>
                        <div class="response" id="mce-success-response"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <!--End mc_embed_signup-->
            </div>
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/visibilitat.jpg" class="media" alt="Estratègies per guanyar visibilitat a internet"/>
            
        </div>
    </section><!-- end main -->


    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='es'): ?>
    <!-- ********** Español ********** -->


    <section class="main clearfix consulting">
        <div class="tabs-container">
            
            <h2>Pide una consultoría gratuita sobre marketing online o branding</h2>
            
            <p>Vendremos a conocer tu negocio y haremos un análisis para detectar qué acciones necesitas para hacerte más visible, llegar a tu cliente ideal, y seducirlo una y otra vez.</p>
            
            <p>Solicítala aquí</p>
            
            <div class="widget widget-subscribe">
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup" class="subscribe-widgetbar">
                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=b269acb4c9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                        </div>
                        <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                        </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response"></div>
                        <div class="response" id="mce-success-response"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <!--End mc_embed_signup-->
            </div>
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/visibilitat.jpg" class="media" alt="Estratègies per guanyar visibilitat a internet"/>
            
        </div>
    </section><!-- end main -->


    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='en'): ?>
    <!-- ********** English ********** -->


    <section class="main clearfix consulting">
        <div class="tabs-container">
            
            <h2>Ask for a free online marketing or branding consultancy</h2>
            
            <p>We will come over to learn about your company and we will do an analysis to detect the appropriate actions to be taken so you become more visible, you reach your ideal client and seduce it again and again.</p>
            
            <p>Apply here</p>
            
            <div class="widget widget-subscribe">
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup" class="subscribe-widgetbar">
                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=b269acb4c9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                        </div>
                        <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                        </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response"></div>
                        <div class="response" id="mce-success-response"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <!--End mc_embed_signup-->
            </div>
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/visibilitat.jpg" class="media" alt="Estratègies per guanyar visibilitat a internet"/>
            
        </div>
    </section><!-- end main -->


    <?php endif; ?>
    <?php } ?>


<?php get_footer(); ?>
