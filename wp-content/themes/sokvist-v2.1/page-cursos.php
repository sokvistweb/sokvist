<?php /* Template Name: Page Cursos */ get_header(); ?>



    <section class="parallax-section blog">
        <div id="intro" class="intro">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h1><span>Cursos</span></h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h1><span>Cursos</span></h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h1><span>Courses</span></h1>
            <?php endif; ?>
            <?php } ?>
        </div>
    </section>
    
    
    <div class="content-body">
			<div class="container">
				<div class="row-main">
					<main class="col-main">
                        <?php if (have_posts()) : ?>
                        <?php query_posts(array( 'post_type' => 'curs', 'posts_per_page' => 10, 'paged' => $paged )); ?>
                        <?php while (have_posts()) : the_post(); ?>
						<article class="post post-1">
							<div class="entry-header">
                                <div class="entry-image">
                                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                        <?php the_post_thumbnail('post'); ?>
                                    </a>
                                    <?php endif; ?>
                                </div>
								<h2 class="entry-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200"><?php the_title(); ?></a>
								</h2>
								<div class="entry-meta">
									<span class="post-category"><?php _e( '', 'html5blank' ); the_category(', '); // Separated by commas ?></span>
			
									<span class="post-date"><time datetime="<?php the_time('Y-m-d'); ?> <?php the_time('H:i'); ?>"><?php the_date(); ?></time></span>
			
									<span class="post-author"><?php _e( '', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span>
			
									<span class="comments-link"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span>
								</div>
							</div>
							<div class="entry-content clearfix">
								<?php the_excerpt(); ?>
								<div class="read-more cl-effect-14">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="more-link animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">
                                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                                        Seguir llegint
                                        <?php endif; ?>
                                        <?php if (qtranxf_getLanguage()=='es'): ?>
                                        Sigue leyendo
                                        <?php endif; ?>
                                        <?php if (qtranxf_getLanguage()=='en'): ?>
                                        Continue reading
                                        <?php endif; ?>
                                        <?php } ?>
                                        <span class="meta-nav">→</span>
                                    </a>
								</div>
							</div>
						</article>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        
                        <nav class="post-pagination">
                            <?php wp_numeric_posts_nav(); ?>
                        </nav>
					</main>
					
                    
                    <?php get_sidebar( 'cursos' ); // (sidebar-cursos.php) ?>
                    
                    
				</div>
			</div>
		</div>



<?php get_footer(); ?>
