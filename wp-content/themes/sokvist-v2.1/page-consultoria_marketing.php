<?php /* Template Name: Page Consultoria Maketing */ get_header(); ?>

    
    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><?php the_title(); ?></h1>
        </div>
    </section>


    <section class="main clearfix consulting">
        <div class="tabs-container">
            
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <?php the_content(); ?>
            
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <!-- ********** Català ********** -->
            <div class="widget widget-subscribe">
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup" class="subscribe-widgetbar">
                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=98086caf5a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                        </div>
                        <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                        </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response"></div>
                        <div class="response" id="mce-success-response"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <!--End mc_embed_signup-->
            </div>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <!-- ********** Español ********** -->
            <div class="widget widget-subscribe">
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup" class="subscribe-widgetbar">
                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=b269acb4c9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                        </div>
                        <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                        </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response"></div>
                        <div class="response" id="mce-success-response"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <!--End mc_embed_signup-->
            </div>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <!-- ********** English ********** -->
            <div class="widget widget-subscribe">
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup" class="subscribe-widgetbar">
                <form action="https://sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=b269acb4c9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                        </div>
                        <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                        </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response"></div>
                        <div class="response" id="mce-success-response"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_6d75e3a4fb" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <!--End mc_embed_signup-->
            </div>
            <?php endif; ?>
            <?php } ?>
            
            
            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                <?php the_post_thumbnail(); ?>
            <?php endif; ?>
            
            <?php endwhile; ?>
            <?php else: ?>

                <h2>Nothing to display</h2>

            <?php endif; ?>
            
        </div>
    </section><!-- end main -->


<?php get_footer(); ?>
