
<aside class="col-aside">
    
    
    <div class="sidebar-widget">
        
        
        <div class="widget widget_categories">
            
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h3 class="widget-title">Categories</h3>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h3 class="widget-title">Categorías</h3>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h3 class="widget-title">Categories</h3>
            <?php endif; ?>
            <?php } ?>
            
            <?php
            $taxonomy = 'recursos-categories';
            $terms = get_terms($taxonomy); // Get all terms of a taxonomy

            if ( $terms && !is_wp_error( $terms ) ) :
            ?>
            <ul>
                <?php foreach ( $terms as $term ) { ?>
                <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo qtrans_useCurrentLanguageIfNotFoundShowAvailable( $term->name ); ?></a></li>
                <?php } ?>
            </ul>
            <?php endif;?>
            
        </div>
        
        
        <div class="widget widget-recent-posts widget_recent_entries">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h3 class="widget-title">Recursos</h3>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h3 class="widget-title">Recursos</h3>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h3 class="widget-title">Resources</h3>
            <?php endif; ?>
            <?php } ?>
            <?php
            $queryObject = new WP_Query( 'post_type=recurs&posts_per_page=10' );
            // The Loop!
            if ($queryObject->have_posts()) {
                ?>
                <ul>
                <?php
                while ($queryObject->have_posts()) {
                    $queryObject->the_post();
                    ?>

                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php
                }
                ?>
                </ul>
                <?php
            }
            ?>
        </div>
        
        <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-recursos')) ?>
        
	</div>
    
    
</aside>


