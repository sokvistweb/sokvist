    
<!-- Mailchimp subscribe form -->
    <div id="mc_embed_signup" class="subscribe-sidebar">
        <span class="modal-close-btn"><svg><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#icon-ios-close-outline"></use></svg></span>
        <div class="sidebar-block">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <p>Vull subscriure'm</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <p>Suscríbeme</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <p>Subscribe me</p>
            <?php endif; ?>
            <?php } ?>
            <form action="//sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=e98b7563d8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">

                <div class="mc-field-group">
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                </div>
                <div class="mc-field-group">
                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='es'): ?>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='en'): ?>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                    <?php endif; ?>
                    <?php } ?>
                </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response"></div>
                    <div class="response" id="mce-success-response"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div class="real-people" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                <div class="clear">
                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='es'): ?>
                    <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='en'): ?>
                    <input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
                    <?php endif; ?>
                    <?php } ?>
                </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /Mailchimp subscribe form -->
    

    <footer>
        <div id="menu_icon">
            <svg class="svg-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#icon-navicon"></use></svg>
            <svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#icon-ios-close-empty"></use></svg>
        </div>
    
        <div class="logo-mobile">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>" data-animsition-out-class="fade-out" data-animsition-out-duration="200">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sokvist-logo-sun-v2.1.svg" title="Sokvist, Web & SEO" alt="Sokvist" width="50" height="50"/>
            </a>
        </div>
    </footer>
    
</div>


    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery-1.11.0.min.js" type="text/javascript"><\/script>')</script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/plugins.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/main.min.js"></script>


</body>
</html>

