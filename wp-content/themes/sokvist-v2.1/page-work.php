<?php /* Template Name: Page Work */ get_header(); ?>


    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><span><?php the_title(); ?></span></h1>
        </div>
    </section>
    

	<section class="main work-gridder clearfix" role="main">
        <ul class="gridder">
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'work', 'posts_per_page' => 33 )); ?>
            <?php $counter = -1; while (have_posts()) : the_post(); $counter++ ?>
            <li class="work grid gridder-list" data-griddercontent="#gridder-content-<?php echo $counter; ?>">
                <figure class="effect-bubba bubba3">
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('work'); ?>
                    <?php endif; ?>
                    <figcaption>
                        <h2><span><?php the_title(); ?></span></h2>
                        <p><?php the_field('project_type'); ?></p>
                        <a href="#">View more</a>
                    </figcaption>			
                </figure>
                <!-- No closing </li> to remove space between inline-block elements http://codepen.io/chriscoyier/pen/hmlqF -->
            <?php endwhile; ?>
            <?php endif; ?>
            
        </ul>
	</section><!-- end main -->
    
    
    <section class="main clearfix">
        <?php if (have_posts()) : ?>
        <?php query_posts(array( 'post_type' => 'work', 'posts_per_page' => 33 )); ?>
        <?php $counter = -1; while (have_posts()) : the_post(); $counter++ ?>
        <div id="gridder-content-<?php echo $counter; ?>" class="gridder-content">
            <div class="gridder-copy">
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div>
            <div class="gridder-imgs">
                <?php $image = get_field('imatge_responsive');
                if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>
            <div class="gridder-imgs">
                <?php $image = get_field('imatge_adicional');
                if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>
            <div class="gridder-imgs">
                <?php $image = get_field('imatge_adicional_2');
                if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </section>


<?php get_footer(); ?>
