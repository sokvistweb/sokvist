<?php /* Template Name: Page Marketing */ get_header(); ?>

    
    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><span><?php the_title(); ?></span></h1>
        </div>
    </section>

    <?php if(function_exists('qtranxf_getLanguage')) { ?>
    <?php if (qtranxf_getLanguage()=='ca'): ?>
    <!-- ********** Català ********** -->

    <section class="main clearfix">
        <ul class="">
            <li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-1.jpg" class="media" alt="img11"/>
                    <figcaption>
                        <h2>El <span>90%</span></h2>
                        <p>dels consumidors consulten a internet abans de comprar.</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- Remove space between inline-block elements http://codepen.io/chriscoyier/pen/hmlqF 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-2.jpg" class="media" alt="img04"/>
                    <figcaption>
                        <h2><span>Saps</span> que</h2>
                        <p>cada cop és més important atendre canals digitals per a arribar als teus clients, però no saps com posar-t’hi</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-3.jpg" class="media" alt="img13"/>
                    <figcaption>
                        <h2>Tens <span>clar</span></h2>
                        <p>que internet és vital per a la continuïtat del teu negoci, però no domines les eines digitals.</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-4.jpg" class="media" alt="img15"/>
                    <figcaption>
                        <h2>El teu <span>client</span></h2>
                        <p>és molt volàtil i és difícil que repeteixi?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-5.jpg" class="media" alt="img02"/>
                    <figcaption>
                        <h2>Massa <span>sovint</span></h2>
                        <p>t’arriben clients que no són el perfil que t’agradaria?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-6.jpg" class="media" alt="img02"/>
                    <figcaption>
                        <h2><span>Gastes</span> massa</h2>
                        <p>en comissions a tercers que et fan publicitat i promoció, i no tens clar com et retorna la inversió?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-7.jpg" class="media" alt="img21"/>
                    <figcaption>
                        <h2><span>Vols</span></h2>
                        <p>posar-te al dia en temes d’internet però no tens temps?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-8.jpg" class="media" alt=""/>
                    <figcaption>
                        <h2>Ets <span>un gran</span></h2>
                        <p>professional i un expert en altres coses i prefereixes concentrar-te en l’ofici que t’agrada?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li>
        </ul>
	</section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container tabs-container-marketing">
            <h2>T'ajudem a vendre millor utilitzant les eines digitals</h2>
            <h3>Així aconseguiràs el teu objectiu</h3>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Genera confiança</a></li>
                    <li><a href="#tab2">Fes-te visible</a></li>
                    <li><a href="#tab3">Fidelitza els teus clients</a></li>
                    <li><a href="#tab4">Estalvia en publicitat</a></li>
                    <li><a href="#tab5">Cuida el disseny</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <p>Utilitza l’estratègia de <i>Blogging</i> per a seduir el teu client potencial, demostrar-li el teu domini en la matèria i fer-li entendre tot el que li pots oferir.</p>
                        <p>Et caldrà un blog optimitzat. Publicar 1 article setmanal optimitzat amb contingut atractiu per al teu client objectiu.</p>
                        <p>Tenir en compte les paraules clau en cada article, perquè Google et trobi.</p>
                        <p>Col·laborar amb professionals complementaris.</p>
                        <p>Utilitzar <i>influencers</i>.</p>
                    </div>

                    <div id="tab2" class="tab">
                        <p>L’estratègia de Marketing de Continguts a les Xarxes Socials t’ajudarà a:</p>
                        <ul>
                            <li>arribar al tipus de client que busques</li>
                            <li>cridar la seva atenció amb continguts visuals, vídeos, sortejos, promocions</li>
                            <li>humanitzar la teva marca amb continguts emocionals i empàtics</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <p>Amb l’Email Marketing fet amb molta cura, aconsegueix que el teu client et tingui present en el moment adequat.</p>
                    </div>
                    
                    <div id="tab4" class="tab">
                        <p>Aprofita la publicitat patrocinada de Google, Facebook, Instagram, LinkedIn, etc, però si tens dominades les altres estratègies no et caldrà invertir gaire diners en SEM.</p>
                    </div>
                    
                    <div id="tab5" class="tab">
                        <p>La teva imatge gràfica en els continguts digitals, <i>packagings</i>, rètols, lones, <i>flyers</i>... tot ha de ser consistent i alineat amb el teu missatge.</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container">
            
            <h2>Eines</h2>
            
            <ul class="list img-list">
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">El blog de la web</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">El blog d’altres entitats o de persones amb influència en el sector</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Portals i plataformes de difusió</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Xarxes Socials: Facebook, Instagram, Twitter, LinkedIn, Google+, Pinterest, Youtube...</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Email Marketing amb softwares oberts de gestió de <i>newsletters</i></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Anuncis patrocinats: Adwords, Facebook Ads, Twitter Ads</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Semàntica SEO en totes les publicacions</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Softwares d’estudi de paraules clau</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Google Analytics</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Google Search Console</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Softwares per a corregir/millorar el SEO-on-site</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub"><i>Copywriting</i>, és dir, escriure amb intenció</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Disseny gràfic i disseny web</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Vídeo i fotografia professional</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container">
            <h2>Quant pagues ara per publicitat que no saps realment quin resultat et dóna?</h2>
            <p>En tot moment cal analitzar els resultats de les publicacions i campanyes per a veure si tenen èxit, si envien tràfic a la web, si augmenten les vendes...</p>
            <p>Pots fer-ho tot tu o t’ajudem? Intercanviem impressions?</p>
            <a href="mailto:judit@sokvist.com?subject=Vull concertar una cita&body=Contacteu-me si-us-plau.  El meu telefon es el ........." title="Envia'ns un email" class="trigger">Concertem una cita</a>
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='es'): ?>
    <!-- ********** Español ********** -->

    <section class="main clearfix">
        <ul class="">
            <li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-1.jpg" class="media" alt="img11"/>
                    <figcaption>
                        <h2>El <span>90%</span></h2>
                        <p>de los consumidores consultan en internet antes de comprar.</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- Remove space between inline-block elements http://codepen.io/chriscoyier/pen/hmlqF 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-2.jpg" class="media" alt="img04"/>
                    <figcaption>
                        <h2><span>Sabes</span> que</h2>
                        <p>cada vez es más importante atender canales digitales para llegar a tus clientes, pero no sabes cómo ponerte en ello.</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-3.jpg" class="media" alt="img13"/>
                    <figcaption>
                        <h2>Tienes <span>claro</span></h2>
                        <p>que internet es vital para la continuidad de tu negocio, pero no dominas las herramientas digitales.</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-4.jpg" class="media" alt="img15"/>
                    <figcaption>
                        <h2>¿Tu <span>cliente</span></h2>
                        <p>es muy volátil y es difícil que repita?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-5.jpg" class="media" alt="img02"/>
                    <figcaption>
                        <h2>¿Demasiado <span>a menudo</span></h2>
                        <p>te llegan clientes que no son el perfil que te gustaría?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-6.jpg" class="media" alt="img02"/>
                    <figcaption>
                        <h2><span>Destinas</span> mas</h2>
                        <p>dinero del que quisieras a pagar comisiones para publicidad y promoción, pero no tienes claro cómo te retorna la inversión?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-7.jpg" class="media" alt="img21"/>
                    <figcaption>
                        <h2><span>¿Quieres</span></h2>
                        <p>ponerte al día en temas de internet pero no tienes tiempo?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-8.jpg" class="media" alt=""/>
                    <figcaption>
                        <h2>¿Eres <span>un gran</span></h2>
                        <p>profesional y un experto en otras cosas y prefieres concentrarte en el oficio que te gusta?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li>
        </ul>
	</section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container tabs-container-marketing">
            <h2>Te ayudamos a vender mejor utilizando las herramientas digitales</h2>
            <h3>Así conseguirás tu objetivo</h3>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Genera confianza</a></li>
                    <li><a href="#tab2">Hazte visible</a></li>
                    <li><a href="#tab3">Fideliza tus clientes</a></li>
                    <li><a href="#tab4">Ahorra en publicidad</a></li>
                    <li><a href="#tab5">Cuida el diseño</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <p>Utiliza la estrategia de <i>Blogging</i> para seducir tu cliente potencial, demostrarle tu dominio en la materia y hacerle entender todo lo que le puedes ofrecer.</p>
                        <p>Te hará falta un blog optimizado. Publicar 1 artículo semanal optimizado con contenido atractivo para tu cliente objetivo.</p>
                        <p>Tener en cuenta las palabras clave en cada artículo, para que Google te encuentre.</p>
                        <p>Colaborar con profesionales complementarios.</p>
                        <p>Utilizar <i>influencers</i>.</p>
                    </div>

                    <div id="tab2" class="tab">
                        <p>La estrategia de Marketing de Contenidos en las Redes Sociales te ayudará a:</p>
                        <ul>
                            <li>Llegar al tipo de cliente que buscas</li>
                            <li>Llamar su atención con contenidos visuales, videos, sorteos, promociones</li>
                            <li>Humanizar tu marca con contenidos emocionales y empáticos</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <p>Con el Email Marketing hecho con mucho esmero, consigue que tu cliente te tenga presente en el momento adecuado.</p>
                    </div>
                    
                    <div id="tab4" class="tab">
                        <p>Aprovecha la publicidad patrocinada de Google, Facebook, Instagram, LinkedIn, etc., pero si tienes dominadas las otras estrategias no te hará falta invertir mucho dinero en SEM.</p>
                    </div>
                    
                    <div id="tab5" class="tab">
                        <p>Tu imagen gráfica en los contenidos digitales, <i>packagings</i>, rótulos, lonas, <i>flyers</i>... todo tiene que ser consistente y alineado con tu mensaje.</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container">
            
            <h2>Herramientas</h2>
            
            <ul class="list img-list">
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">El blog de la web</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">El blog de otras entidades o de personas con influencia en el sector</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Portales y plataformas de difusión</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Redes Sociales: Facebook, Instagram, Twitter, LinkedIn, Google+, Pinterest, Youtube...</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Email Marketing con softwares abiertos de gestión de <i>newsletters</i></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Anuncios patrocinados: Adwords, Facebook Ads, Twitter Ads</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Semántica SEO en todas las publicaciones</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Softwares de estudio de palabras clave</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Google Analytics</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Google Search Console</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Softwares para corregir/mejorar el SEO-on-site</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub"><i>Copywriting</i>, es decir, escribir con intención</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Diseño gráfico y diseño web</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Video y fotografía profesional</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container">
            <h2>¿Cuánto pagas actualmente por publicidad que no sabes realmente qué resultado te da?</h2>
            <p>En todo momento hay que analizar los resultados de las publicaciones y campañas para ver si tienen éxito, si envían tráfico en la web, si aumentan las ventas...</p>
            <p>¿Puedes hacerlo todo tú o te ayudamos? ¿Intercambiamos impresiones?</p>
            <a href="mailto:judit@sokvist.com?subject=Quiero concertar una cita&body=Contactadme por favor. Mi telefono es el ........." title="Mándanos un email" class="trigger">Concertar una cita</a>
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='en'): ?>
    <!-- ********** English ********** -->

    <section class="main clearfix">
        <ul class="">
            <li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-1.jpg" class="media" alt="img11"/>
                    <figcaption>
                        <h2><span>90%</span> of</h2>
                        <p>consumers search the internet before buying.</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- Remove space between inline-block elements http://codepen.io/chriscoyier/pen/hmlqF 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-2.jpg" class="media" alt="img04"/>
                    <figcaption>
                        <h2><span>You</span> know</h2>
                        <p>it is becoming important to attend digital channels to reach your customers, but you don't know how to get into it.</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-3.jpg" class="media" alt="img13"/>
                    <figcaption>
                        <h2>Your <span>are</span></h2>
                        <p>aware that the internet is vital to the continuity of your business, but you don’t master the digital tools.</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-4.jpg" class="media" alt="img15"/>
                    <figcaption>
                        <h2>Is <span>your</span></h2>
                        <p>customer very volatile and will unlikely repeat?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-5.jpg" class="media" alt="img02"/>
                    <figcaption>
                        <h2>Too <span>often</span></h2>
                        <p>do customers come to you but they are not the profile you would like?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-6.jpg" class="media" alt="img02"/>
                    <figcaption>
                        <h2><span>Do</span> you</h2>
                        <p>allocate more money than you'd like for ads and promotions, but aren’t sure about the return on inversion?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-7.jpg" class="media" alt="img21"/>
                    <figcaption>
                        <h2>You <span>want</span></h2>
                        <p>to catch up on internet topics but you don’t have the time?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li><!-- 
            --><li class="grid grid-25 gridder-list">
                <figure class="effect-bubba bubba4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/bg-marketing-8.jpg" class="media" alt=""/>
                    <figcaption>
                        <h2>Are <span>you</span></h2>
                        <p>a great professional and an expert in other things and prefer to focus on the job you like?</p>
                        <span class="mask"></span>
                    </figcaption>			
                </figure>
            </li>
        </ul>
	</section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container tabs-container-marketing">
            <h2>We help you to sell better by using digital tools</h2>
            <h3>So will achieve your goal</h3>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Generates trust</a></li>
                    <li><a href="#tab2">Become visible</a></li>
                    <li><a href="#tab3">Create loyal customers</a></li>
                    <li><a href="#tab4">Save on publicity</a></li>
                    <li><a href="#tab5">Take care of design</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <p>Use the Blogging strategy to seduce your prospective client, demonstrate your mastery of the subject and make him understand everything you can offer.</p>
                        <p>You will need an optimized blog. Publish weekly 1 optimized article with engaging content for your target client.</p>
                        <p>Take into account the keywords in each article, so Google can find you.</p>
                        <p>Collaborate with complementary professionals.</p>
                        <p>Use influencers.</p>
                    </div>

                    <div id="tab2" class="tab">
                        <p>The Content Marketing Strategy in Social Networks will help you to:</p>
                        <ul>
                            <li>Reach the type of customer you are looking for</li>
                            <li>Call their attention with visual content, videos, giveaways, promotions</li>
                            <li>Humanize your brand with emotional and empathic content</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <p>With Email Marketing done carefully, your customer will have you in mind at the right moment.</p>
                    </div>
                    
                    <div id="tab4" class="tab">
                        <p>Take advantage of sponsored advertising from Google, Facebook, Instagram, LinkedIn, etc., but if you have mastered other strategies you won’t need to invest much money in SEM.</p>
                    </div>
                    
                    <div id="tab5" class="tab">
                        <p>Your graphic image on digital content, packaging, signs, canvasses, flyers... everything has to be consistent and aligned with your message.</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container">
            
            <h2>Tools</h2>
            
            <ul class="list img-list">
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">The blog of your web</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">The blog of other entities or people with impact in the sector</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Broadcast portals and platforms</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Social Networks: Facebook, Instagram, Twitter, LinkedIn, Google+, Pinterest, Youtube...</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Email Marketing with open newsletter management software</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Sponsored ads: Adwords, Facebook Ads, Twitter Ads</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Semantic SEO in all posts</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Keyword study software</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Google Analytics</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Google Search Console</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">•	Software to correct/improve SEO-on-site</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Copywriting, that is, writing with intention</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Graphic design and web design</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Video and professional photography</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section><!-- end main -->


    <section class="main clearfix">
        <div class="tabs-container">
            <h2>How much do you currently pay for advertising that you actually don’t know what the outcome is?</h2>
            <p>At all times you have to analyse the results of the publications and campaigns to see if they are successful, if they send traffic to the web, if they increase sales...</p>
            <p>Can you do it all? Or, do we help you? Let’s talk about it?</p>
            <a href="mailto:judit@sokvist.com?subject=I want to make an appointment&body=Please contact me, my phone number is ........." title="Send us an email" class="trigger">Make an appointment</a>
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php } ?>


<?php get_footer(); ?>
