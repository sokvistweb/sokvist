<?php /* Template Name: Page Services */ get_header(); ?>


    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><span><?php the_title(); ?></span></h1>
        </div>
    </section>
    

    <section class="main clearfix">
        <div class="tabs-container tabs-container-gridder">
            <div class="tabs animated-slide">
                <ul class="tab-links gridder">
                    <li class="grid tab-grid active">
                        <figure class="effect-marley">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-dissenyweb-06.jpg" class="media" alt="Disseny Web" width="520" height="520" />
                            <figcaption>
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                <h2>Disseny <span>Web</span></h2>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                <h2>Diseño <span>Web</span></h2>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                <h2>Web <span>Design</span></h2>
                                <?php endif; ?>
                                <?php } ?>
                                <a href="#tab1" class="a-tab">View more</a>
                            </figcaption>		
                        </figure>
                    </li><!--
                    --><li class="grid tab-grid">
                        <figure class="effect-honey">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-dissenygrafic-06.jpg" class="media" alt="Disseny Grafic" width="520" height="520" />
                            <figcaption>
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                <h2>Disseny <span>Gràfic</span></h2>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                <h2>Diseño <span>Gráfico</span></h2>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                <h2>Graphic <span>Design</span></h2>
                                <?php endif; ?>
                                <?php } ?>
                                <a href="#tab2" class="a-tab">View more</a>
                            </figcaption>
                        </figure>
                    </li><!--
                    --><li class="grid tab-grid">
                        <figure class="effect-sarah">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-marketing-01.jpg" class="media square" alt="Marketing Digital" width="480" height="480" />
                            <figcaption>
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                <h2>Marketing &amp; <span>SEO</span></h2>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                <h2>Marketing &amp; <span>SEO</span></h2>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                <h2>Marketing &amp; <span>SEO</span></h2>
                                <?php endif; ?>
                                <?php } ?>

                                <?php query_posts('post_type=page&name=digital-marketing'); while (have_posts ()): the_post(); ?>
                                <?php the_excerpt(); ?>
                                <?php endwhile; ?>
                                <a href="#tab3" class="a-tab">View more</a>
                            </figcaption>
                        </figure>
                    </li>
                </ul>


                <div class="tab-content tab-content-gridder">
                    <div id="tab1" class="gridder-content tab active">
                        <div class="gridder-show">
                            <div class="gridder-padding">
                                <div class="row row-vcenter">
                                    <div class="row-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-webdesign-2.jpg" alt="Web Design" width="680" height="452" />
                                    </div>
                                    <?php query_posts('post_type=page&name=web-design'); while (have_posts ()): the_post(); ?>
                                    <div class="row-copy">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                            </div><!-- /gridder-padding -->
                        </div><!-- /gridder-show -->
                    </div>

                    <div id="tab2" class="gridder-content tab">
                        <div class="gridder-show">
                            <div class="gridder-padding">
                                <div class="row row-vcenter">
                                    <div class="row-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-graphic.jpg" alt="SEO" width="680" height="452" />
                                    </div>
                                    <?php query_posts('post_type=page&name=graphic-design'); while (have_posts ()): the_post(); ?>
                                    <div class="row-copy">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                            </div><!-- /gridder-padding -->
                        </div><!-- /gridder-show -->
                    </div>

                    <div id="tab3" class="gridder-content tab">
                        <div class="gridder-show">
                            <div class="gridder-padding">
                                <div class="row row-vcenter">
                                    <?php query_posts('post_type=page&name=digital-marketing'); while (have_posts ()): the_post(); ?>
                                    <div class="row-copy-full">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                            </div><!-- /gridder-padding -->
                        </div><!-- /gridder-show -->
                    </div>
                </div><!-- /tab-content -->
            </div><!-- /tabs -->
        </div><!-- /tabs-container -->
    </section>
    

<?php get_footer(); ?>
