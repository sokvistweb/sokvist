<?php get_header(); ?>


    <section class="parallax-section blog">
        <div id="intro" class="intro">
            <h1><span><?php _e( 'Page not found', 'html5blank' ); ?></span></h1>
        </div>
    </section>
    
    
    <div class="content-body">
        <div class="container">
            <div class="row-main">
                <article id="post-404">

                    <h2>
                        <a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
                    </h2>

                    <div class="sea">
                        <div class="circle-wrapper">
                            <div class="bubble"></div>
                            <div class="submarine-wrapper">
                                <div class="submarine-body">
                                    <div class="window"></div>
                                    <div class="engine"></div>
                                    <div class="light"></div>
                                </div>
                                <div class="helix"></div>
                                <div class="hat">
                                  <div class="leds-wrapper">
                                      <div class="periscope"></div>
                                      <div class="leds"></div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </article>
            </div>
        </div>
    </div>


<?php get_footer(); ?>
