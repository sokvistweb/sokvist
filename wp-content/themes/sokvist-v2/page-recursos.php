<?php /* Template Name: Page Recursos */ get_header(); ?>



    <section class="parallax-section blog">
        <div id="intro" class="intro">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h1><span>Recursos</span></h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h1><span>Recursos</span></h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h1><span>resources</span></h1>
            <?php endif; ?>
            <?php } ?>
        </div>
    </section>
    
    
    <div class="content-body">
			<div class="container">
				<div class="row-main">
					<main class="col-main">
                        <?php if (have_posts()) : ?>
                        <?php query_posts(array( 'post_type' => 'recurs', 'posts_per_page' => 10, 'paged' => $paged )); ?>
                        <?php while (have_posts()) : the_post(); ?>
						<article class="post post-1">
							<div class="entry-header">
                                <div class="entry-image">
                                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                        <?php the_post_thumbnail('post'); ?>
                                    </a>
                                    <?php endif; ?>
                                </div>
								<h2 class="entry-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200"><?php the_title(); ?></a>
								</h2>

							</div>
							<div class="entry-content clearfix">
								<?php the_excerpt(); ?>
								<div class="read-more cl-effect-14">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="more-link animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">
                                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                                        Seguir llegint
                                        <?php endif; ?>
                                        <?php if (qtranxf_getLanguage()=='es'): ?>
                                        Sigue leyendo
                                        <?php endif; ?>
                                        <?php if (qtranxf_getLanguage()=='en'): ?>
                                        Continue reading
                                        <?php endif; ?>
                                        <?php } ?>
                                        <span class="meta-nav">→</span>
                                    </a>
								</div>
							</div>
						</article>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        
                        <nav class="post-pagination">
                            <?php wp_numeric_posts_nav(); ?>
                        </nav>
					</main>
					
                    
                    <?php get_sidebar( 'recursos' ); // (sidebar-recursos.php) ?>
                    
                    
				</div>
			</div>
		</div>



<?php get_footer(); ?>
