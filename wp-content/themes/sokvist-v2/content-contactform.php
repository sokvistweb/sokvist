
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST'){

    if (!isset($_POST['Nombre']) || !isset($_POST['Email'])){

        $errors .= 'Por favor completar todos los campos requeridos.';
    }

    if ($_POST['Nombre'] != "") {
        $_POST['Nombre'] = filter_var($_POST['Nombre'], FILTER_SANITIZE_STRING);
        if ($_POST['Nombre'] == "") {
            $errors .= 'Nombre is not valid.<br/>';
        }
    }

    if ($_POST['Email'] != "") {
        $email = filter_var($_POST['Email'], FILTER_SANITIZE_EMAIL);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors .= 'Email is not valid.<br/>';
        }
    }

    if ($_POST['Message'] != "") {
        $_POST['Message'] = filter_var($_POST['Message'], FILTER_SANITIZE_STRING);
        if ($_POST['Message'] == "") {
            $errors .= 'Message is not valid.<br/>';
        }
    }
    

    if(!isset($errors)) {
        $to = 'sergi@sokvist.com';
        $from = $_POST['Email'];
        $subject = 'Formulario de contacto - Ibasiam';
        $content = "
        Nombre: " . $_POST['Nombre'] . "
        Email: " . $_POST['Email'] . "
        Mensaje: " . $_POST['Message'];

        if(mail($to, $subject, $content, 'From:' . $from)){
            echo '<p class="alert alert-success">Gracias por contactar con nosotros, su mensaje ha sido enviado.</p>';
        } else {
            echo '<p class="alert alert-danger">Hubo un error al enviar su mensaje</p>';
        }
    } else {
        echo '<p class="alert alert-danger">' . $errors . '</p>';
    }
}

?>



                
<form id="form" class="form" name="form" method="post" enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
    <div class="form-content">
        <div id="section1">
            <div class="field"><div class="edit-options"><div class="edit"></div><div class="delete"></div></div><label for="Nombre">Nombre (requerido)</label><input type="text" id="Nombre" name="Nombre" required></div>
            
            <div class="field"><label for="Email">E-mail (requerido)</label><input type="email" id="Email" name="Email" required></div>
        </div>

        <div id="section2">
            <div class="field">
                <p>Mensaje</p>
                <textarea id="Message" name="Message"></textarea>
            </div>

            <div class="field"><input type="submit" id="SubmitForm"></div>
        </div>
    </div>
</form>
