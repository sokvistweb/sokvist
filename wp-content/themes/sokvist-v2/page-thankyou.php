<?php /* Template Name: Page Thankyou */ get_header(); ?>

    
    <section class="parallax-section">
        <div id="intro" class="intro">
            <h1><?php the_title(); ?></h1>
        </div>
    </section>


    <?php if(function_exists('qtranxf_getLanguage')) { ?>
    <?php if (qtranxf_getLanguage()=='ca'): ?>
    <!-- ********** Català ********** -->


    <section class="main clearfix consulting">
        <div class="tabs-container">
            
            <p>En un termini de 48 hores et contactarem per quedar. Parlarem de quines accions fas de promoció, quins resultats obtens i què t’agradaria canviar.</p>
            
            <p>Mentre trobem el moment de reunir-nos, a <a href="https://sokvist.com/">la nostra web</a> pots informar-te millor de <a href="https://sokvist.com/qui-som/">qui som</a>, <a href="https://sokvist.com/marketing/">a què ens dediquem</a> i <a href="https://sokvist.com/treballs/">quins clients tenim</a>.</p>
            
            
        </div>
    </section><!-- end main -->


    <section class="main clearfix">
        
        <ul class="gridder">
            <li class="grid gridder-list" data-griddercontent="#gridder-content-1">
                <figure class="effect-marley">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-dissenyweb-03.jpg" class="media" alt="Disseny Web" width="520" height="520" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Disseny <span>Web</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>Diseño <span>Web</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>Web <span>Design</span></h2>
                        <?php endif; ?>
                        <?php } ?>
                        <a href="#">View more</a>
                    </figcaption>		
                </figure>
            </li><!-- Remove space between inline-block elements http://codepen.io/chriscoyier/pen/hmlqF

            --><li class="grid gridder-list" data-griddercontent="#gridder-content-2">
                <figure class="effect-honey">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-dissenygrafic-03.jpg" class="media" alt="Disseny Grafic" width="520" height="520" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Disseny <span>Gràfic</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>Diseño <span>Gráfico</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>Graphic <span>Design</span></h2>
                        <?php endif; ?>
                        <?php } ?>
                        <a href="#">View more</a>
                    </figcaption>
                </figure>
            </li><!--

            --><li class="grid">
                <figure class="effect-sarah">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-marketing-01.jpg" class="media square" alt="Marketing Digital" width="480" height="480" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Marketing &amp; <span>SEO</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>Marketing &amp; <span>SEO</span></h2>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>Marketing &amp; <span>SEO</span></h2>
                        <?php endif; ?>
                        <?php } ?>
                        
                        <?php query_posts('post_type=page&p=28'); while (have_posts ()): the_post(); ?>
                        <?php the_excerpt(); ?>
                        <?php endwhile; ?>
                        <a href="<?php echo esc_url( home_url( '/marketing' ) ); ?>" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>
                </figure>
            </li><!--

            --><li class="grid">
                <figure class="effect-roxy">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-quisom.png" class="media square" alt="About" width="536" height="536" />
                    <figcaption>
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <h2>Qui <span>som</span></h2>
                        <p>L'equip</p>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <h2>N<span>osotros</span></h2>
                        <p>El equipo</p>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <h2>About <span>Us</span></h2>
                        <p>The Team</p>
                        <?php endif; ?>
                        <?php } ?>
                        <a href="<?php echo esc_url( home_url( '/about' ) ); ?>" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>		
                </figure>
            </li><!--

            --><li class="grid">
                <figure class="effect-bubba bubba1">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-treballs-04.jpg" class="media" alt="Work" width="520" height="520" />
                    <?php query_posts('post_type=page&p=6'); while (have_posts ()): the_post(); ?>
                    <figcaption>
                        <h2><span><?php the_title(); ?></span></h2>
                        <p><?php the_excerpt(); ?></p>
                        <a href="<?php echo esc_url( home_url( '/portfoli' ) ); ?>" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>
                    <?php endwhile; ?>
                </figure>
            </li><!--

            --><li class="grid">
                <figure class="effect-bubba bubba2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-recursos-04.jpg" class="media" alt="Recursos" width="520" height="520" />
                    <?php query_posts('post_type=page&p=10'); while (have_posts ()): the_post(); ?>
                    <figcaption>
                        <h2><span><?php the_title(); ?></span></h2>
                        <p><?php the_excerpt(); ?></p>
                        <a href="<?php echo esc_url( home_url( '/recursos' ) ); ?>" class="animsition-link" data-animsition-out-class="fade-out" data-animsition-out-duration="200">View more</a>
                    </figcaption>
                    <?php endwhile; ?>
                </figure>
            </li>
        </ul>
        
    </section>


    <section class="main clearfix">
        <div id="gridder-content-1" class="gridder-content">
            <div class="row row-vcenter">
                <div class="row-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-webdesign-2.jpg" alt="Web Design" width="680" height="452" />
                </div>
                <?php query_posts('post_type=page&p=21'); while (have_posts ()): the_post(); ?>
                <div class="row-copy">
                    <?php the_content(); ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div id="gridder-content-2" class="gridder-content">
            <div class="row row-vcenter">
                <div class="row-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-graphic.jpg" alt="SEO" width="680" height="452" />
                </div>
                <?php query_posts('post_type=page&name=disseny-grafic'); while (have_posts ()): the_post(); ?>
                <div class="row-copy">
                    <?php the_content(); ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>


    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='es'): ?>
    <!-- ********** Español ********** -->

    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/moscatell-demporda-espolla.jpg" class="media" alt="E-shop web responsive"/>
            
            <h2>E-shop para Instagram y Facebook</h2>
            <h3 class="es-intro"><span>Sokvist</span>, Diseño & Marketing</h3>
            
            <p>Somos súper fans de las compras por internet. Ya casi no pisamos una tienda física desde hace años y sabemos lo que es:</p>
            
            <ul class="list img-list">
                <li class="half">
                    <div class="inner">
                        <div class="li-img tick">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Tener una buena experiencia de compra en internet y convertirnos en clientes-apóstoles de una marca.</p>
                        </div>
                    </div>
                </li>
                <li class="half">
                    <div class="inner">
                        <div class="li-img cross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Entrar en una tienda online y salir sin comprar nada y, lo que es peor, sintiéndonos frustrados.</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>En el 98% de los casos, los usuarios que nos hemos frustrado durante un proceso de compra online, hacemos acciones más allá de dejar de comprar:</p>
            
            <ul class="list img-list">
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">nos damos de baja de la <em>newsletter</em> de ese negocio</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">bloqueamos los anuncios de esa marca</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">lo comentamos con amigos y criticamos esa marca</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>Aparte de que el producto sea interesante o no, lo más importante en una tienda online es ofrecer <strong>una buena experiencia de compra</strong>, que el proceso sea fácil, fluido, limpio y atento. El cliente debe terminar el proceso haciendo <i>click</i> en el botón de pagar con una sonrisa y una sensación positiva en el cuerpo.</p>
            
            <p>Si tienes una estrategia de comunicación en redes sociales, un nicho de mercado ya definido y un máximo de 15 productos, <strong class="orange">hazte una e-Shop especial para Instagram y Facebook</strong>.</p>
            
            <p>Tu cliente está acostumbrado a la navegación <strong>rápida, fresca, intuitiva e interactiva</strong> del entorno de redes sociales. Por tanto, tu e-Shop debe estar a la altura de sus expectativas.</p>
            
            <a class="showcase-img" href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/portfoli/easyshop-showcase-2.jpg" class="media" alt="E-shop web responsive"/></a>
            
            <h3>Asegúrate un buen ratio de conversión</h3>
            
            <p>Si haces un anuncio en Facebook o Instagram para llevar visitantes a tu tienda, la navegación debe ser rápida y eficiente. Has seducido a tu visitante, lo has llevado hasta tu tienda, ahora pónselo muy fácil para <strong>COMPRAR</strong>.</p>
            
            <p>Ese es el objetivo con el que hemos diseñado esta tienda.</p>
            
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <h3 class="price">Una E-shop <a href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank">como esta</a> = <span>1.600€</span> + IVA</h3>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">Características de la E-shop</a></li>
                    <li><a href="#tab2">Necesitarás</a></li>
                    <li><a href="#tab3">Extras</a></li>
                    <li><a href="#tab4">Plazo de entrega</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <ul>
                            <li>Diseño <em>responsive</em> (multidispositivos)</li>
                            <li>Estructura intuïtiva, limpia y de usabilidad óptima</li>
                            <li>Gestor de contenidos fácil para que la puedas autogestionar</li>
                            <li>Plataforma de pago</li>
                            <li>Botones de compartición de las fichas de producto en redes sociales, WhatsApp o email</li>
                            <li>Pack SEO básico incluido. La entregamos en las condiciones indispensables para que pase con nota los test de <strong>SEO: velocidad de carga y usabilidad</strong>.
                                <ul>
                                    <li>Sitemap xml</li>
                                    <li>Meta-descripción en la “<em>home</em>”</li>
                                    <li>Semántica y URL’s “<em>google-friendly</em>”</li>
                                    <li>Archivo robots.txt</li>
                                    <li>Optimización de las imágenes para web</li>
                                    <li>Indexación en buscadores</li>
                                    <li>Alta en Google Analytics</li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div id="tab2" class="tab">
                        <ul>
                            <li>2-3 fotos en alta resolución para los fondos de pantalla. Mínimo 2000 px de ancho.</li>
                            <li>Fotos de producto profesionales.</li>
                            <li>Textos de máximo 2 párrafos para la sección Quiénes somos</li>
                            <li>Especificar las condiciones de venta, políticas de envíos y devoluciones y tener ya contratado un TPV o una cuenta de Paypal.</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <a class="tab3-a" href="mailto:judit@sokvist.com?subject=Extras%20e-Shop%20Instagram">consulta precio</a>
                        <ul>
                            <li>Galería de imágenes. Cuadrícula de miniaturas o slider</li>
                            <li>Más de un idioma. </li>
                            <li>Si tienes planificada una estrategia de contenidos para atraer clientes y hacer marca, necesitarás un blog.</li>
                            <li>Aplicaciones externas, por ejemplo: cajita de suscripción a lista de Mailchimp, widget de opiniones de Facebook, Tripadvisor, etc.</li>
                            <li>Wishlist, ratings, elección de tallas y colores.</li>
                            <li>Hosting y registro de dominio. Estamos muy satisfechos con nuestro proveedor y te podemos proporcionar un buen servicio de alojamiento y ciberseguridad. <a href="mailto:judit@sokvist.com">Pídelo</a>.</li>
                            <li>Mantenimiento web (antispam, protección, actualización de plugins).</li>
                        </ul>
                    </div>
                    
                    <div id="tab4" class="tab">
                        <p><strong>15 días</strong>. Te la entregamos “llave en mano”, totalmente optimizada y funcionando en 15 días después de recibir todo el contenido por tu parte.</p>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            <h2>¿Quieres vender en redes sociales? Ésta es tu e-shop</h2>

            <a href="mailto:judit@sokvist.com?subject=Quiero contratar una e-shop&body=Contactadme por favor. Mi telefono es el ........." title="Envia'ns un email" class="trigger">Contrátala</a>
            
            <p><strong>¿Dudas? Te las aclaramos:</strong></p>
            <p>Movil: <a href="tel:0034620273955">620 273 955</a><br>
            Fijo: <a href="tel:0034972793942">972 793 942</a><br>
            Email: <a href="mailto:judit@sokvist.com">judit@sokvist.com</a><br>
            Skype: <a href="skype:judit.figueiras?call">judit.figueiras</a></p>
            
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php if (qtranxf_getLanguage()=='en'): ?>
    <!-- ********** English ********** -->

    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marketing/moscatell-demporda-espolla.jpg" class="media" alt="E-shop web responsive"/>
            
            <h2>E-shop for Instagram and Facebook</h2>
            <h3 class="es-intro"><span>Sokvist</span>, Design & Marketing</h3>
            
            <p>We are super fans of online shopping. We have hardly stepped on a physical store for years, and we know what it is:</p>
            
            <ul class="list img-list">
                <li class="half">
                    <div class="inner">
                        <div class="li-img tick">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" xml:space="preserve">
                            <path d="M448,71.9c-17.3-13.4-41.5-9.3-54.1,9.1L214,344.2l-99.1-107.3c-14.6-16.6-39.1-17.4-54.7-1.8 c-15.6,15.5-16.4,41.6-1.7,58.1c0,0,120.4,133.6,137.7,147c17.3,13.4,41.5,9.3,54.1-9.1l206.3-301.7 C469.2,110.9,465.3,85.2,448,71.9z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Having a good shopping experience on the internet and become a brand's client-apostles.</p>
                        </div>
                    </div>
                </li>
                <li class="half">
                    <div class="inner">
                        <div class="li-img cross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">Enter an online store and leave without buying anything and, what is worse, feeling frustrated.</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>In 98% of the cases, we users that have been frustrated during an online shopping process, we do actions beyond stop buying:</p>
            
            <ul class="list img-list">
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">we unsubscribe from the newsletter of that business</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">we block ads of that brand</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="li-img redcross">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <path d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5 c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9 c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
                            </svg>
                        </div>
                        <div class="li-text">
                            <p class="li-sub">we discuss it with friends and criticize that brand</p>
                        </div>
                    </div>
                </li>
            </ul>
            
            <p>Apart from whether the product is interesting or not, the most important thing in an online store is to offer <strong>a good shopping experience</strong> and that the process is easy, smooth, clean and attentive. The client must finish the process by clicking on the pay button with a smile and a positive feeling.</p>
            
            <p>If you have a communication strategy in social media, a niche market already defined and a maximum of 15 products, get a special <strong class="orange">e-Shop for Instagram and Facebook</strong>.</p>
            
            <p>Your client is used to <strong>fast, fresh, intuitive and interactive browsing</strong> of the social media environment. Therefore, your e-shop must rise to their expectations.</p>
            
            <a class="showcase-img" href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/portfoli/easyshop-showcase-2.jpg" class="media" alt="E-shop web responsive"/></a>
            
            <h3>Make sure you have a good conversion rate</h3>
            
            <p>If you publish an ad on Facebook or Instagram to bring visitors to your store, navigation should be fast and efficient. You have seduced your visitor; you have taken him/her to your shop: now make it very easy for him/her to <strong>BUY</strong>.</p>
            
            <p>That's the goal for which we have designed this store.</p>
            
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            
            <h3 class="price">An E-shop <a href="http://sokvist.com/easyshop/" title="Easy Shop de Sokvist" target="_blank">like this</a> = <span>€ 1,600</span> + VAT</h3>

            <div class="tabs animated-slide">
                <ul class="tab-links">
                    <li class="active"><a href="#tab1">E-shop features</a></li>
                    <li><a href="#tab2">What you need</a></li>
                    <li><a href="#tab3">Additional features</a></li>
                    <li><a href="#tab4">Delivery time</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <ul>
                            <li>Responsive design</li>
                            <li>Intuitive, clean and usable structure</li>
                            <li>Easy to use content manager so you can self-manage it</li>
                            <li>Payment platform</li>
                            <li>Product listing sharing buttons on social media, WhatsApp or email</li>
                            <li>Basic SEO pack included. We deliver the site ready to pass the <strong>SEO test: loading speed and usability</strong>.
                                <ul>
                                    <li>Xml sitemap</li>
                                    <li>Meta-description on home page</li>
                                    <li>Semantics and URLs google-friendly</li>
                                    <li>Robots.txt file</li>
                                    <li>Web images optimization</li>
                                    <li>Search engine indexing</li>
                                    <li>Google Analytics registration</li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div id="tab2" class="tab">
                        <ul>
                            <li>2-3 high resolution images to be used as a background. Minimum 2000 pixels wide.</li>
                            <li>Professional product photos.</li>
                            <li>Copywriting of 2 paragraphs maximum for the About Us section.</li>
                            <li>Specify terms of sale, shipment and return policies, and login data to POS or PayPal account.</li>
                        </ul>
                    </div>

                    <div id="tab3" class="tab">
                        <a class="tab3-a" href="mailto:judit@sokvist.com?subject=Extras%20e-Shop%20Instagram">price inquiry</a>
                        <ul>
                            <li>Image gallery. Thumbnails grid or slider.</li>
                            <li>Languages.</li>
                            <li>Blog. If you have a content strategy planned to attract customers and create brand image, you’ll need a blog.</li>
                            <li>External apps, for example: Mailchimp subscription form, Facebook reviews widget, Tripadvidor, etc...</li>
                            <li>Wishlist, ratings, size and color picker.</li>
                            <li>Hosting and domain registration. We can provide you with a good and secure hosting service.  <a href="mailto:judit@sokvist.com">Ask for it</a>.</li>
                            <li>Web maintenance (anti spam, protection, updates).</li>
                        </ul>
                    </div>
                    
                    <div id="tab4" class="tab">
                        <p><strong>15 days</strong>. We deliver it "turnkey", fully optimized and running in 15 days after receiving all the content.</p>
                    </div>

                </div>
            </div>
        </div>
    </section><!-- end main -->


    <section class="main clearfix easyshop">
        <div class="tabs-container">
            <h2>Want to sell on social media? This is your e-shop</h2>

            <a href="mailto:judit@sokvist.com?subject=Quiero contratar una e-shop&body=Contactadme por favor. Mi telefono es el ........." title="Envia'ns un email" class="trigger">Order now</a>
            
            <p><strong>Questions? May we help you?:</strong></p>
            <p>Mobile: <a href="tel:0034620273955">620 273 955</a><br>
            Phone: <a href="tel:0034972793942">972 793 942</a><br>
            Email: <a href="mailto:judit@sokvist.com">judit@sokvist.com</a><br>
            Skype: <a href="skype:judit.figueiras?call">judit.figueiras</a></p>
            
        </div>
    </section><!-- end main -->

    <?php endif; ?>
    <?php } ?>


<?php get_footer(); ?>
