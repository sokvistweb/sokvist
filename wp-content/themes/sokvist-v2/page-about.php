<?php /* Template Name: Page About */ get_header(); ?>



    <div class="projects-container">
		<ul>
			<li class="cd-single-project">
				<div class="cd-title">
					<h2>Judit</h2>
				</div> <!-- .cd-title -->
                <?php query_posts('post_type=page&name=judit'); while (have_posts ()): the_post(); ?>
				<div class="cd-project-info">
                    <div class="round-img">
                        <?php if ( has_post_thumbnail()):
                            the_post_thumbnail('work');
                        endif; ?>
                    </div>
					<?php the_content(); ?>
				</div> <!-- .cd-project-info -->
                <?php endwhile; ?>
			</li>

			<li class="cd-single-project">
				<div class="cd-title">
					<h2>Sergi</h2>
				</div> <!-- .cd-title -->
                <?php query_posts('post_type=page&name=sergi'); while (have_posts ()): the_post(); ?>
				<div class="cd-project-info">
                    <div class="round-img">
                        <?php if ( has_post_thumbnail()):
                            the_post_thumbnail('work');
                        endif; ?>
                    </div>
					<?php the_content(); ?>
				</div> <!-- .cd-project-info -->
                <?php endwhile; ?>
			</li>
            
            <li class="cd-single-project">
				<div class="cd-title">
					<h2>David</h2>
				</div> <!-- .cd-title -->
                <?php query_posts('post_type=page&name=david'); while (have_posts ()): the_post(); ?>
				<div class="cd-project-info">
                    <div class="round-img">
                        <?php if ( has_post_thumbnail()):
                            the_post_thumbnail('work');
                        endif; ?>
                    </div>
					<?php the_content(); ?>
				</div> <!-- .cd-project-info -->
                <?php endwhile; ?>
			</li>

			
		</ul>
		<a href="#0" class="cd-close">Close</a>
		<a href="#0" class="cd-scroll">Scroll</a>
	</div> <!-- .project-container -->



<?php get_footer(); ?>
