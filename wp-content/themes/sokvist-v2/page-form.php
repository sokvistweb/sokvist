<?php
/**
 * Template Name: Cuéntanos Contact Form
 */
?>


<?php get_header(); ?>


<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST'){

    if (!isset($_POST['Nombre']) || !isset($_POST['Email'])){

        $errors .= 'Por favor completar todos los campos requeridos.';
    }

    if ($_POST['Nombre'] != "") {
        $_POST['Nombre'] = filter_var($_POST['Nombre'], FILTER_SANITIZE_STRING);
        if ($_POST['Nombre'] == "") {
            $errors .= 'Nombre is not valid.<br/>';
        }
    }

    if ($_POST['Email'] != "") {
        $email = filter_var($_POST['Email'], FILTER_SANITIZE_EMAIL);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors .= 'Email is not valid.<br/>';
        }
    }

    if ($_POST['Web'] != "") {
        $_POST['Web'] = filter_var($_POST['Web'], FILTER_SANITIZE_STRING);
        if ($_POST['Web'] == "") {
            $errors .= 'Web is not valid.<br/>';
        }
    }
    
    if ($_POST['Asunto'] != "") {
        $_POST['Asunto'] = filter_var($_POST['Asunto'], FILTER_SANITIZE_STRING);
        if ($_POST['Asunto'] == "") {
            $errors .= 'Asunto is not valid.<br/>';
        }
    }

    if ($_POST['Message1'] != "") {
        $_POST['Message1'] = filter_var($_POST['Message1'], FILTER_SANITIZE_STRING);
        if ($_POST['Message1'] == "") {
            $errors .= 'Message is not valid.<br/>';
        }
    }
    
    if ($_POST['Message2'] != "") {
        $_POST['Message2'] = filter_var($_POST['Message2'], FILTER_SANITIZE_STRING);
        if ($_POST['Message2'] == "") {
            $errors .= 'Message is not valid.<br/>';
        }
    }
    
    if ($_POST['Message3'] != "") {
        $_POST['Message3'] = filter_var($_POST['Message3'], FILTER_SANITIZE_STRING);
        if ($_POST['Message3'] == "") {
            $errors .= 'Message is not valid.<br/>';
        }
    }
    
    if ($_POST['Message4'] != "") {
        $_POST['Message4'] = filter_var($_POST['Message4'], FILTER_SANITIZE_STRING);
        if ($_POST['Message4'] == "") {
            $errors .= 'Message is not valid.<br/>';
        }
    }

    if(!isset($errors)) {
        $to = 'sergi@sokvist.com';
        $from = $_POST['Email'];
        $subject = 'Formulario de contacto - Ibasiam';
        $content = "
        Nombre: " . $_POST['Nombre'] . "
        Email: " . $_POST['Email'] . "
        Web: " . $_POST['Web'] . "
        Asunto: " . $_POST['Asunto'] . "
        Mensaje (conocer mas): " . $_POST['Message1'] . "
        Mensaje (que problemas): " . $_POST['Message2'] . "
        Mensaje (que precio): " . $_POST['Message3'] . "
        Mensaje (que servicio): " . $_POST['Message4'];

        if(mail($to, $subject, $content, 'From:' . $from)){
            echo '<p class="alert alert-success">Gracias por contactar con nosotros, su mensaje ha sido enviado.</p>';
        } else {
            echo '<p class="alert alert-danger">Hubo un error al enviar su mensaje</p>';
        }
    } else {
        echo '<p class="alert alert-danger">' . $errors . '</p>';
    }
}

?>


<section class="parallax-section blog">
    <div id="intro" class="intro">

    </div>
</section>


<div class="content-body">
			<div class="container">
				<div class="row-main">
					<main class="col-main">
            
            <h1><?php the_title(); ?></h1>
            <article>


                
<form id="form" class="form" name="form" method="post" enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
    <h2>¡Cuéntanos tu proyecto!</h2>
    <div class="form-content">
        <div id="section1">
            <div class="field"><div class="edit-options"><div class="edit"></div><div class="delete"></div></div><label for="Nombre">Su nombre (requerido)</label><input type="text" id="Nombre" name="Nombre" required></div>
            
            <div class="field"><label for="Email">Su e-mail (requerido)</label><input type="email" id="Email" name="Email" required></div>
            
            <div class="field"><label for="Web">Sitio web de la empresa</label><input type="text" id="Web" name="Web"></div>
            
            <div class="field"><label for="Asunto">Asunto</label><input type="text" id="Asunto" name="Asunto"></div>
            
        </div>

        <div id="section2">
            <div class="field">
                <p>Necesitamos conocer más sobre tu empresa o tu producto. Cuéntanos al sector al que pertenece  ¿Que objetivos quieres conseguir?</p>
                <textarea id="Message1" name="Message1"></textarea>
            </div>
            <div class="field">
                <p>¿Que tipo de problemas os estáis encontrando a la hora de conseguir objetivos?</p>
                <textarea id="Message2" name="Message2"></textarea>
            </div>
            <div class="field">
                <p>¿Que precio tiene NO estar alcanzando estos objetivos? Se especifico en términos de pérdida de dinero, tiempo, desgaste... </p>
                <textarea id="Message3" name="Message3"></textarea>
            </div>
            <div class="field">
                <p>¿Que tipo de servicio estas buscando? Necesitas revitalizar tu marca, necesitas un nuevo nombre para tu producto, necesitas lanzar una nueva categoría al mercado, necesitas un plan estratégico. Son muchas la opciones ¡Cuéntanos! Estaremos encantados de escucharte.</p>
                <textarea id="Message4" name="Message4"></textarea>
            </div>
            <div class="field"><input type="submit" id="SubmitForm"></div>
        </div>
    </div>
</form>
                
                
<?php get_template_part( 'content', 'contactform' ); ?>
                
            
        </article>

</main>
<?php get_sidebar( 'cursos' ); // (sidebar-cursos.php) ?>
                    
</div>
			</div>
		</div>
                    
                    

<?php get_footer(); ?>