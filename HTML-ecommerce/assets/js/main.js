/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

(function () {
    
    
    const doc = document.documentElement

    doc.classList.remove('no-js')
    doc.classList.add('js')

    // Reveal animations
    if (document.body.classList.contains('has-animations')) {
        /* global ScrollReveal */
        const sr = window.sr = ScrollReveal()

        sr.reveal('.hero-title, .hero-tagline, .cta-intro, .features-title, .hero-paragraph, .newsletter-header, .newsletter-form, .text-small', {
          duration: 600,
          distance: '40px',
          easing: 'cubic-bezier(0.5, -0.01, 0, 1.005)',
          origin: 'bottom',
          interval: 150
        })

        sr.reveal('.bubble-3, .bubble-4, .hero-browser-inner, .bubble-1, .bubble-2', {
          duration: 600,
          scale: 0.95,
          easing: 'cubic-bezier(0.5, -0.01, 0, 1.005)',
          interval: 150
        })

        sr.reveal('.feature', {
          duration: 400,
          distance: '40px',
          easing: 'cubic-bezier(0.5, -0.01, 0, 1.005)',
          interval: 100,
          origin: 'bottom',
          viewFactor: 0.5
        })
    }
    
    
    
    /**
    * Modal Tiny no footer
    */

    var modalTinyNoFooter = new tingle.modal({
        onClose: function() {
            console.log('close');
        },
        onOpen: function() {
            console.log('open');
        },
        beforeOpen: function() {
            console.log('before open');
        },
        beforeClose: function() {
            console.log('before close');
            return true;
        },
        cssClass: ['class1', 'class2']
    });
    var btn = document.querySelector('.js-tingle-modal-1');
    btn.addEventListener('click', function(){
        modalTinyNoFooter.open();
    });
    modalTinyNoFooter.setContent(document.querySelector('.tingle-demo-tiny').innerHTML);
    
    
    
    /*!
    * https://github.com/iamdustan/smoothscroll
    */
    // scroll into view
    document.querySelector('.js-scroll-into-hello').addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('.newsletter').scrollIntoView({ behavior: 'smooth' });
    });
    
    document.querySelector('.js-scroll-into-hello2').addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('.newsletter').scrollIntoView({ behavior: 'smooth' });
    });
    
    
     
}())